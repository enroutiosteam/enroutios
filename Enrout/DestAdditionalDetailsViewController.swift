//
//  DestAdditionalDetailsViewController.swift
//  Enrout
//
//  Created by Daniel Kwakye on 09/01/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import UIKit
import ContactsUI

class DestAdditionalDetailsViewController: UIViewController {
    
    @IBOutlet weak var recipientName: UITextField!
    @IBOutlet weak var recipientPhone: UITextField!
    @IBOutlet weak var selectContact: UIButton!
    @IBOutlet weak var doneBtn: UIButton!
    
    @IBOutlet weak var pageTitle: UILabel!
    
    @IBOutlet weak var closeBtn: UIButton!
    
    var destination: Destination?
    var contactPicker: CNContactPickerViewController!
    
    var source: DestMultiTableViewController?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        doneBtn.curveCorners()
        
        contactPicker  = CNContactPickerViewController()
        contactPicker.delegate = self
        contactPicker.displayedPropertyKeys =
            [CNContactGivenNameKey
                , CNContactPhoneNumbersKey]
        
        doneBtn.addTarget(self, action: #selector(doneBtnTapped(_:)), for: .touchUpInside)
        selectContact.addTarget(self, action: #selector(addContactBtnTapped), for: .touchUpInside)
        closeBtn.addTarget(self, action: #selector(closeBtnTapped(_:)), for: .touchUpInside)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setValues()
    }
    
    func setValues(){
        pageTitle.text = destination?.place
        recipientName.text = destination?.recipientName
        recipientPhone.text = destination?.recipientNumber
        
        let image = UIImage(named: "add_contact")
        let resizedImage = image?.resizeImage(targetSize: CGSize(width: 30, height: 30))
        selectContact.setImage(resizedImage, for: .normal)
        recipientName.addRightIcon(image: UIImage(named: "edit_icon")!)
        self.recipientPhone.addRightIcon(image: UIImage(named: "edit_icon")!)
    }
    
    @IBAction func addContactBtnTapped(){
        
         self.present(contactPicker, animated: true, completion: nil)
     }
     
     @IBAction func closeBtnTapped(_ sender: Any) {
         self.dismiss(animated: true, completion: nil)
     }
     
     @IBAction func doneBtnTapped(_ sender: Any){
        destination?.recipientName = recipientName.text
        destination?.recipientNumber = recipientPhone.text

         source?.additionalDetailsAdded(destination: destination!)
         self.dismiss(animated: true, completion: nil)
         
     }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DestAdditionalDetailsViewController: CNContactPickerDelegate{
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        // You can fetch selected name and number in the following way

        // user name
        let userName:String = contact.givenName

        // user phone number
        let userPhoneNumbers:[CNLabeledValue<CNPhoneNumber>] = contact.phoneNumbers
        let firstPhoneNumber:CNPhoneNumber = userPhoneNumbers[0].value


        // user phone number string
        let primaryPhoneNumberStr:String = firstPhoneNumber.stringValue

        recipientName.text = userName
        recipientPhone.text = primaryPhoneNumberStr
        print(primaryPhoneNumberStr)

    }
}
