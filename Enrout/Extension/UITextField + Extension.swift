//
//  UITextField + Extension.swift
//  Enrout
//
//  Created by Daniel Expresspay on 28/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Foundation

extension UITextField{
    
    func addRightIcon(image: UIImage, selector: Selector? =  nil) {
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
       // let newImage = image.resizeImage(targetSize: CGSize(width: 20.0, height: 20.0))
        btn.setImage(image, for: .normal)
        if let selector = selector {
            btn.addTarget(self, action: selector, for: .touchUpInside)
        }
        let rv = UIView(frame: CGRect(x: 0, y: 0, width: btn.frame.width + 10, height: btn.frame.height))
        self.rightViewMode = .always
        rv.addSubview(btn)
        self.rightView = rv
    }
}
