//
//  UIFont+Extension.swift
//  Enrout
//
//  Created by Jude Botchwey on 20/11/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Foundation
import UIKit



extension UIFont {
    
    class func enroutFontOne(forSize size: CGFloat) -> UIFont?{
        return UIFont.systemFont(ofSize: size)
    }
    
    
     class func enroutFontTwo(forSize size: CGFloat) -> UIFont?{
          return UIFont.systemFont(ofSize: size)
      }
      
    
}
