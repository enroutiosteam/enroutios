//
//  UIButton + Extension.swift
//  Enrout
//
//  Created by Daniel Expresspay on 17/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

extension UIButton {
    func curveCorners(){
        self.layer.cornerRadius = 10.0
    }
    
    func applyBorder() {
        self.layer.borderColor = #colorLiteral(red: 0, green: 0.8500903249, blue: 0.8637030721, alpha: 1)
        self.layer.borderWidth = 1.0
    }
}

//@IBDesignable
//class LeftAlignedIconButton: UIButton {
//    override func titleRect(forContentRect contentRect: CGRect) -> CGRect {
//        let titleRect = super.titleRect(forContentRect: contentRect)
//        let imageSize = currentImage?.size ?? .zero
//        let availableWidth = contentRect.width - imageEdgeInsets.right - imageSize.width - titleRect.width
//        return titleRect.offsetBy(dx: round(availableWidth / 2), dy: 0)
//    }
//}
//
//@IBDesignable
//class RightAlignedIconButton: UIButton {
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        semanticContentAttribute = .forceRightToLeft
//        contentHorizontalAlignment = .right
//        let availableSpace = bounds.inset(by: contentEdgeInsets)
//        let availableWidth = availableSpace.width - imageEdgeInsets.left - (imageView?.frame.width ?? 0) - (titleLabel?.frame.width ?? 0)
//        titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: availableWidth / 2)
//    }
//}
