//
//  CollectionReference + extension.swift
//  Enrout
//
//  Created by Daniel Kwakye on 21/02/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import FirebaseFirestore

extension Query {
    func whereField(_ field: String, isDateInToday value: Date) -> Query {
        let components = Calendar.current.dateComponents([.year, .month, .day], from: value)
        guard
            let start = Calendar.current.date(from: components),
            let end = Calendar.current.date(byAdding: .day, value: 1, to: start)
        else {
            fatalError("Could not find start date or calculate end date.")
        }
        return whereField(field, isGreaterThan: start).whereField(field, isLessThan: end)
    }
}
