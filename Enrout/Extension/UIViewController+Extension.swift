//
//  UINavigationController+Extension.swift
//  Enrout
//
//  Created by Jude Botchwey on 20/11/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Foundation
import UIKit



extension UIViewController {
    
    func makeNavigationBarTransparent(forNavigationController navigationController:UINavigationController?,withTintColor tintColor:UIColor = UIColor.green, setTextAttr setAttr:Bool = true){
            if let bar = navigationController?.navigationBar {
                bar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
                bar.shadowImage = UIImage()
                bar.tintColor = tintColor
                bar.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.0)
                let yourBackImage = UIImage(named: "back")
                               bar.backIndicatorImage = yourBackImage
                               bar.backIndicatorTransitionMaskImage = yourBackImage
                
                
                if setAttr{
                    let fontDictionary = [ NSAttributedString.Key.foregroundColor:tintColor,
                                           NSAttributedString.Key.font: UIFont.enroutFontOne(forSize: 16)!]
                    bar.titleTextAttributes = fontDictionary
                }
    
                navigationController?.navigationBar.backIndicatorImage = UIImage(named: "back")
                navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "back")
                navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
                navigationController?.navigationBar.isTranslucent = true
                navigationController?.view.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.0)
            }
          
        }
        
        func createDefaultNavigationBar(forNavigationController navigationController:UINavigationController?){
            if let navigationBar = navigationController?.navigationBar {
                navigationBar.isTranslucent = true
                navigationBar.tintColor = UIColor.white
                let fontDictionary = [ NSAttributedString.Key.foregroundColor:UIColor.white,
                                       NSAttributedString.Key.font: UIFont.enroutFontOne(forSize: 16)!]
                navigationBar.titleTextAttributes = fontDictionary
                navigationBar.setBackgroundImage(imageLayerForGradientBackground(withFrame: navigationBar.bounds), for: UIBarMetrics.default)
                navigationBar.shadowImage = UIImage()
                
                let yourBackImage = UIImage(named: "back")
                navigationBar.backIndicatorImage = yourBackImage
                navigationBar.backIndicatorTransitionMaskImage = yourBackImage
            }
        }
    
    
       func navigationBarWithBackgroundColor(_ backgroundColor: UIColor, TintColor tintColor: UIColor, displayBackButtonIfNeeded: Bool, BackImage imageName:String, displayShadowBar: Bool = false) {

           let backButtonImage = UIImage(named: imageName)

           if #available(iOS 13.0, *) {

               let appearance = UINavigationBarAppearance()
               appearance.backgroundColor = backgroundColor

               appearance.titleTextAttributes = [.foregroundColor: tintColor]
               appearance.setBackIndicatorImage(backButtonImage, transitionMaskImage: backButtonImage)
               appearance.shadowImage = displayShadowBar ? UIImage(named:"back") : UIImage()

               let back = UIBarButtonItemAppearance()
               // hide back button text
               back.normal.titleTextAttributes = [.foregroundColor: UIColor.clear]
               appearance.backButtonAppearance = back

               navigationController?.navigationBar.tintColor = tintColor
               navigationController?.navigationBar.standardAppearance = appearance
               navigationController?.navigationBar.compactAppearance = appearance
               navigationController?.navigationBar.scrollEdgeAppearance = appearance

           } else {
               if displayBackButtonIfNeeded {
                   self.navigationController?.navigationBar.backIndicatorImage = backButtonImage
                   self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backButtonImage
                   self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "back", style: UIBarButtonItem.Style.plain, target: nil, action: nil)

               } else {
                   self.navigationItem.setHidesBackButton(true, animated: false)
               }

               self.navigationController?.navigationBar.barTintColor = backgroundColor
               self.navigationController?.navigationBar.tintColor = tintColor
               self.navigationController?.navigationBar.setBackgroundImage(UIImage(named:"back"), for: UIBarMetrics.default)
               self.navigationController?.navigationBar.shadowImage = displayShadowBar ? UIImage(named:"back") : UIImage()
           }
       }
    
    
    
}

extension UIViewController {

    var isModal: Bool {

        let presentingIsModal = presentingViewController != nil
        let presentingIsNavigation = navigationController?.presentingViewController?.presentedViewController == navigationController
        let presentingIsTabBar = tabBarController?.presentingViewController is UITabBarController

        return presentingIsModal || presentingIsNavigation || presentingIsTabBar
    }
}
