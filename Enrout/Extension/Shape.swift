//
//  Shape.swift
//  Enrout
//
//  Created by Daniel Kwakye on 05/02/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import Foundation
import UIKit

class Shape: UIView {
    @IBInspectable var cornerRadius: CGFloat {
           get {
               return layer.cornerRadius
           }
           set {
               layer.cornerRadius = newValue
               layer.shadowRadius = newValue
               layer.masksToBounds = false
           }
       }

       @IBInspectable var shadowOpacity: Float {
           get {
               return layer.shadowOpacity
           }
           set {
               layer.shadowOpacity = newValue
            layer.shadowColor = UIColor.separator.cgColor
           }
       }
    
    @IBInspectable var border: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
            layer.borderColor = UIColor.separator.cgColor
        }
    }

       @IBInspectable var shadowOffset: CGSize {
           get {
               return layer.shadowOffset
           }
           set {
               layer.shadowOffset = newValue
               layer.shadowColor = UIColor.separator.cgColor
               layer.masksToBounds = false
           }
       }
}

