//
//  Enums.swift
//  Enrout
//
//  Created by Daniel Expresspay on 18/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

enum Panels {
    case searchpickpoint, pickpointdetailsingle, searchdestinationpoint, destinationpointdetailssingle, viaMap, senderDetails, pickpointdetailmultiple, destinationpointdetailsmultiple
}

enum MapModes{
    case beganDraggin, settled
}

enum PaymentType{
    case payForWallet, payForTrip, payForMe
}
