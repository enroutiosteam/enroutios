//
//  Constant.swift
//  Enrout
//
//  Created by Jude Botchwey on 20/11/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Foundation



/// Note: - Userdefault keys

let USER_PASSED_INITIAL = "passed_initial"
let USER_LOGGED_IN = "user_logged_in"
let OTP_SUCCESSFUL = "otp_successful"
let RIDER_LAST_KNOWN_LOCATION = "RIDER_LAST_KNOWN_LOCATION"
let RIDER_TRIP_STATUS = "RIDER_TRIP_STATUS"


class UIConstants {
    public static var activeTrip: Trip = Trip()
    public static var activePickUps = [Pickup]()
    public static var activeDestinations = [Destination]()
    public static var lastKnowLocation: PosLatLng = PosLatLng()
}
