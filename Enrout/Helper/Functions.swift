//
//  Functions.swift
//  Enrout
//
//  Created by Jude Botchwey on 20/11/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseAuth
import Presentr
import GoogleMaps
import Pulley

func imageLayerForGradientBackground(withFrame frame:CGRect) -> UIImage {
    return UIImage()
}

func getUser(userId: String, onCompletion: @escaping (_ serverError: Bool, _ applicationuser: ApplicationUser?) -> Void){
    let db = Firestore.firestore()
    
    db.collection(Collections.USERS.rawValue).document(userId).getDocument { (documentSnapshot, error) in
        
        if let error = error{
            onCompletion(false, nil)
            print("Error fetching data \(error.localizedDescription)")
        }
        
        guard let documentSnapshot = documentSnapshot else {
            onCompletion(false, nil)
            return
        }
        
        // user with given id not found
        if !documentSnapshot.exists {
            onCompletion(true, nil)
            return
        }
        
        // found user with given id
        var user = ApplicationUser()
        let data = documentSnapshot.data()
        
        user.fullName = data?["fullName"] as? String
        user.id = data?["id"] as? String
        user.address = data?["address"] as? String
        user.currentBalance = data?["currentBalance"] as? Double ?? 0.0
        user.authType = data?["authType"] as? String
        user.dateAdded = data?["dateAdded"]
        user.email = data?["email"] as? String
        user.gender = data?["gender"] as? String
        user.isOnline = data?["isOnline"] as? Bool
        user.lastLogin = data?["lastLogin"]
        user.outStandingBalance = data?["outStandingBalance"] as? Double ?? 0.0
        user.phoneNumber = data?["phoneNumber"] as? String
        user.photoUrl = data?["photoUrl"] as? String
        user.device = data?["device"] as? String ?? "IOS"
        user.appReviewed = data?["appReviewed"] as? Bool ?? false
        
        onCompletion(true, user)
        
    }
}

func updateUser(user: ApplicationUser, onCompletion: @escaping (_ status: Bool, _ user: ApplicationUser?) -> Void){
    let db = Firestore.firestore()
    var params = [String:Any]()
    
    params["fullName"] = user.fullName
    params["id"] = user.id
    params["address"] = user.address
    params["currentBalance"] = user.currentBalance
    params["authType"] = user.authType
    params["dateAdded"] = user.dateAdded
    params["email"] = user.email
    params["gender"] = user.gender
    params["isOnline"] = user.isOnline
    params["lastLogin"] = user.lastLogin
    params["outStandingBalance"] = user.outStandingBalance
    params["phoneNumber"] = user.phoneNumber
    params["photoUrl"] = user.photoUrl
    params["device"] = user.device
    params["appReviewed"] = user.appReviewed
    db.collection(Collections.USERS.rawValue).document(user.id!).updateData(params, completion: {
        err in
         if let err = err {
                print("Error creating user \(err.localizedDescription)")
                  onCompletion(false, nil)
            return
          }
                    
         onCompletion(true, user)
    })
}

func updateIsOnline(user: ApplicationUser, onCompletion: @escaping (_ status: Bool, _ user: ApplicationUser?) -> Void  ) {
    
    let db = Firestore.firestore()
    var params = [String:Any]()
    params["isOnline"] = user.isOnline
    params["device"] = user.device
    
   db.collection(Collections.USERS.rawValue).document(user.id!).updateData(params, completion: {
        err in
         if let err = err {
                print("Error creating user \(err.localizedDescription)")
                  onCompletion(false, nil)
            return
          }
                    
         onCompletion(true, user)
    })
}

func createUser(user: ApplicationUser, onCompletion: @escaping (_ status: Bool, _ user: ApplicationUser?) -> Void){
       let db = Firestore.firestore()
       var params = [String:Any]()
    
    params["fullName"] = user.fullName
    params["id"] = user.id
    params["address"] = user.address
    params["currentBalance"] = user.currentBalance
    params["authType"] = user.authType
    params["dateAdded"] = user.dateAdded
    params["email"] = user.email
    params["gender"] = user.gender
    params["isOnline"] = user.isOnline
    params["lastLogin"] = user.lastLogin
    params["outStandingBalance"] = user.outStandingBalance
    params["phoneNumber"] = user.phoneNumber
    params["photoUrl"] = user.photoUrl
    params["device"] = user.device
    params["appReviewed"] = user.appReviewed
       
    db.collection(Collections.USERS.rawValue).document(user.id!).setData(params, completion: {
           err in
          if let err = err {
                 print("Error creating user \(err.localizedDescription)")
                   onCompletion(false, nil)
             return
           }
                     
          onCompletion(true, user)
           
       })
   }

func finalizeLoginWithCredentials(viewController: UIViewController, credential: AuthCredential){
    
    // sing user in with credential
     let dialog = AnyProgressDialog.newInstance(view: viewController.view).show()
    Auth.auth().signIn(with: credential) { (authDataResult, error) in
        if let error = error{
           dialog.dismis()
           print(error.localizedDescription)
            Toast.error(view: viewController.view, message: error.localizedDescription)
           return
       }
        
        guard let authUser = authDataResult else {
            dialog.dismis()
            Toast.error(view: viewController.view, message: "please try again")
            return
        }
        
        getUser(userId: authUser.user.uid) { (status, user) in
            if !status {
               dialog.dismis()
               Toast.error(view: viewController.view, message: "Please try again")
               return
            }
            
            // check if user is already register,
            guard let user = user else {
               // if not tell user to register and logout
                dialog.dismis()
                Toast.error(view: viewController.view, message: "Sorry, ure not registed in our system")
                try? Auth.auth().signOut()
                return
            }
            
            Constants.applicationUser = user
            Constants.applicationUser?.email = user.email ?? ""
            Constants.applicationUser?.gender = user.gender ?? ""
            Constants.applicationUser?.photoUrl = user.photoUrl ?? ""
            let encodedUser = try? JSONEncoder().encode(Constants.applicationUser)
            UserDefaults.standard.set(encodedUser, forKey: Constants.APPLICATION_USER)
            
            if var user = Constants.applicationUser {
                print("user id => \(user.id!)")
                user.isOnline = true
                user.device = "IOS"
                user.lastLogin = FieldValue.serverTimestamp()
                user.authType = UserDefaults.standard.string(forKey: Constants.AUTH_TYPE)
                updateUser(user: user) { _, _ in
                    dialog.dismis()
                   Toast.success(view: viewController.view, message: "\(user.fullName!) has logged in successfuly")
                   viewController.performSegue(withIdentifier: "goToMainView", sender: viewController)
                }
            }
            
            // if hes available, get user and take him to dashboard
           
            
        }
        
    }
    
    
    
}



func finalizeRegistrationWithCredentials(viewController: UIViewController, credential: AuthCredential){
    
    let dialog = AnyProgressDialog.newInstance(view: viewController.view).show()
            Auth.auth().signIn(with: credential) { (authDataResult, error) in
                if let error = error{
                    dialog.dismis()
                    print(error.localizedDescription)
                    Toast.error(view: viewController.view, message: "Unable to sign in please try again")
                    return
                }
                
            
                guard let authUser = authDataResult else {
                    dialog.dismis()
                    Toast.error(view: viewController.view, message: "please try again")
                    return
                }
                
                 // check if user is already registered
                getUser(userId: authUser.user.uid) { (status, user) in
                    if !status {
                        dialog.dismis()
                        Toast.error(view: viewController.view, message: "Please try again")
                        return
                    }
                    
                      let pref = UserDefaults.standard
                    
                    if let user = user {
                        // user exists, and user is aready signed in so go to dashboard
                        Constants.applicationUser = user
                        let message = "\(user.fullName!) registered successfully"
                        dialog.dismis()
                        Toast.success(view: viewController.view, message: message)
                        
                        
                        Constants.applicationUser = user
                        Constants.applicationUser?.email = user.email ?? ""
                        Constants.applicationUser?.gender = user.gender ?? ""
                        Constants.applicationUser?.photoUrl = user.photoUrl ?? ""
                        
                        let encodedUser = try? JSONEncoder().encode(Constants.applicationUser)
                        UserDefaults.standard.set(encodedUser, forKey: Constants.APPLICATION_USER)
                        
                        print("encodedUser => \(String(data: encodedUser!, encoding: .utf8))")
                        
                        viewController.performSegue(withIdentifier: "goToMainView", sender: viewController)
                        return
                    }
                    
                    
                    // new user, create user and  move user to dashboard
                    Constants.applicationUser  = ApplicationUser()
                  
                    
                    Constants.applicationUser?.fullName = pref.string(forKey: Constants.USER_DISPLAY_NAME)
                    Constants.applicationUser?.id = authUser.user.uid
                    Constants.applicationUser?.address = ""
                    Constants.applicationUser?.currentBalance = 0.0
                    Constants.applicationUser?.authType = pref.string(forKey: Constants.AUTH_TYPE)
                    
                    Constants.applicationUser?.dateAdded = FieldValue.serverTimestamp()
                    Constants.applicationUser?.email = authUser.user.email ?? ""
                    Constants.applicationUser?.gender = ""
                    Constants.applicationUser?.isOnline = true
                    Constants.applicationUser?.lastLogin = FieldValue.serverTimestamp()
                    Constants.applicationUser?.outStandingBalance = 0.0
                    Constants.applicationUser?.phoneNumber = pref.string(forKey: Constants.PHONE_AUTH_PHONE_NUMBER)
                    Constants.applicationUser?.photoUrl = ""
                
                    createUser(user: Constants.applicationUser!) { (status, appUser) in
                        if !status {
                            dialog.dismis()
                            Toast.success(view: viewController.view, message: "Please try again")
                            return
                        }
                        
                        let encodedUser = try? JSONEncoder().encode(Constants.applicationUser)
                        UserDefaults.standard.set(encodedUser, forKey: Constants.APPLICATION_USER)
                        
                        if var user = Constants.applicationUser {
                            print("user id => \(user.id!)")
                            user.isOnline = true
                            user.device = "IOS"
                            user.lastLogin = FieldValue.serverTimestamp()
                            updateUser(user: user) { _, _ in
                               
                                let message = "\(appUser!.fullName!) registered successfully"
                                                       
                               dialog.dismis()
                               Toast.success(view: viewController.view, message: message)
                              
                               viewController.performSegue(withIdentifier: "goToMainView", sender: viewController)
                                
                            }
                        }
                        
                       
                    }
                }
                
                
                
            }
           
    
}

func getUniqueString() -> String {
    let uid = Auth.auth().currentUser?.uid
    let dateFormatterGet = DateFormatter()
    dateFormatterGet.dateFormat = "yyyyMMddHHmmss"
    return "\(uid!)\(dateFormatterGet.string(from: Date()))"
    
}

func getScreenSize() -> (width: CGFloat, height: CGFloat){
    let screenSize = UIScreen.main.bounds
    let screenWidth = screenSize.width
    let screenHeight = screenSize.height
    return (screenWidth, screenHeight)
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

//Presentr * Pop up dialog
public func presentModalController(presentingController: UIViewController,imageName: String, headerTitle: String, subHeaderTitle: String, doneHidden: Bool, cancelHidden: Bool, withHandler: @escaping (Int) -> ()){
            
//    let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//    let viewController = storyboard.instantiateViewController(withIdentifier: "CustomPopUpController") as! CustomPopUpController
    let viewController = CustomPopUpController.customPopupController
    viewController.configAlert(imageName: imageName, headerTitle: headerTitle, subHeaderTitle: subHeaderTitle, doneHidden: doneHidden, cancelHidden: cancelHidden, withHandler: withHandler)
    presentingController.customPresentViewController(getCustomPresenter(), viewController: viewController, animated: true, completion: nil)
}

func presentProductImageEnlargened(presentingController: UIViewController, image: UIImage, title: String, price: Double){
    
    let viewController = ProductModalViewController.productModalViewController
    viewController.configure(image: image, title: title, price: price)
    presentingController.customPresentViewController(getCustomPresenter(), viewController: viewController, animated: true, completion: nil)
}

func presentItemCategories(presentingController: UIViewController){
    let viewController = ItemCategoriesViewController.itemCategoriesViewController
    viewController.delegate = presentingController as? ItemCategoryDelegate
    presentingController.customPresentViewController(getCustomPresenter(enterAnimation: .coverHorizontalFromLeft), viewController: viewController, animated: true)
}

func getSweetAlertInstance() -> SweetAlertViewController{
    let sweetAlertViewController: SweetAlertViewController = {
        return UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(identifier: "SweetAlertViewController") as! SweetAlertViewController
    }()
    
    return sweetAlertViewController
}

func presentSweetAlert(presentingController: UIViewController, message: String, showCancelBtn: Bool = false, delegate: SweetAlertDelegate? = nil, title: String? = nil) -> SweetAlertViewController? {
    let viewController = getSweetAlertInstance()
    viewController.config(message: message, title: title, showCancelBtn: showCancelBtn, delegate: delegate)
    presentingController.customPresentViewController(getCustomPresenter(enterAnimation: .crossDissolve), viewController: viewController, animated: true)
    
    return viewController
}

func dateToString(date: Date) -> String{
    let formatter = DateFormatter()
    // initially set the format based on your datepicker date / server String
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

    let myString = formatter.string(from: date) // string purpose I add here
    return myString
    
    // convert your string to date
//    let yourDate = formatter.date(from: myString)
//    //then again set the date format whhich type of output you need
//    formatter.dateFormat = "dd-MMM-yyyy"
//    // again convert your date to string
//    let myStringafd = formatter.string(from: yourDate!)
}

func openTrackTripVC(presenter:UIViewController, drawerContentVC: TrackBottomViewController, trackMainViewController:  TrackMapViewController, trip: Trip){
            
           trackMainViewController.trip = trip
           drawerContentVC.trip = trip

           drawerContentVC.mapViewController = trackMainViewController
                
    trackMainViewController.trackBottomVC = drawerContentVC
        
           let pulleyController = PulleyViewController(contentViewController: trackMainViewController, drawerViewController: drawerContentVC)
           
           pulleyController.modalPresentationStyle = .fullScreen
    presenter.present(pulleyController, animated: true, completion: nil)
}

func getCustomPresenter(enterAnimation: TransitionType = .coverVerticalFromTop) -> Presentr{
    
    let width = ModalSize.full
       let height = ModalSize.full
       let center = ModalCenterPosition.customOrigin(origin: CGPoint(x: 0, y: 0))
       let customType = PresentationType.custom(width: width, height: height, center: center)

       let customPresenter = Presentr(presentationType: customType)
       customPresenter.transitionType = enterAnimation
       customPresenter.dismissTransitionType = .crossDissolve
       customPresenter.roundCorners = false
       customPresenter.backgroundColor = .black
       customPresenter.backgroundOpacity = 0.5
       customPresenter.dismissOnSwipe = true
       customPresenter.dismissOnSwipeDirection = .top
    
    return customPresenter
}

func getTripType() -> TripTypeCode {
    if UIConstants.activePickUps.count > 1 {
        return TripTypeCode.MP
    }else if UIConstants.activeDestinations.count > 1 {
        return TripTypeCode.MD
    }else{
          return TripTypeCode.SPSD
    }
  
}

func shortenPickupsDestinations(trip: Trip) -> String{
    var ppStringBuilder = ""
    for pp in trip.pickupNames! {
        var abbrvPickUps: String = pp
        if pp.count > 4{
            abbrvPickUps = String(pp.prefix(4))
        }
        
        ppStringBuilder = "\(ppStringBuilder)\(abbrvPickUps)"+"/"
    }
    
    if ppStringBuilder.isEmpty { return "" }
    
    ppStringBuilder.removeLast()
    let pickupStringResult: String = String(ppStringBuilder)
    
    var destStringBuilder = ""
    
    for dd in trip.destinationNames! {
       var abbrvDest = dd
       if dd.count > 4{
            abbrvDest = String(dd.prefix(4))
        }
        
        destStringBuilder = "\(destStringBuilder)\(abbrvDest)"+"/"
    }
    if destStringBuilder.isEmpty {return "" }
    destStringBuilder.removeLast()
    let destStringResult: String = String(destStringBuilder)
    
    return "\(pickupStringResult) ➡️ \(destStringResult)"
}


func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double, completion: @escaping (_ address: String?) -> Void) {
    var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
    let lat: Double = pdblLatitude
    //21.228124
    let lon: Double = pdblLongitude
    //72.833770
    let ceo: CLGeocoder = CLGeocoder()
    center.latitude = lat
    center.longitude = lon

    let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)


    ceo.reverseGeocodeLocation(loc, completionHandler:
        {(placemarks, error) in
            if (error != nil)
            {
                print("reverse geodcode fail: \(error!.localizedDescription)")
                completion(nil)
                return
            }
            let pm = placemarks! as [CLPlacemark]

            if pm.count > 0 {
                let pm = placemarks![0]
//                print(pm.country)
//                print(pm.locality)
//                print(pm.subLocality)
//                print(pm.thoroughfare)
//                print(pm.postalCode)
//                print(pm.subThoroughfare)
                var addressString : String = ""
                if pm.subLocality != nil {
                    addressString = addressString + pm.subLocality! + ", "
                }
                if pm.thoroughfare != nil {
                    addressString = addressString + pm.thoroughfare! + ", "
                }
                if pm.locality != nil {
                    addressString = addressString + pm.locality! + ", "
                }
                if pm.country != nil {
                    addressString = addressString + pm.country! + ", "
                }
                if pm.postalCode != nil {
                    addressString = addressString + pm.postalCode! + " "
                }

                completion(addressString)
                return
          }
            
           completion(nil)
    })

}















extension UIViewController{
    func makeNavBarTransparent(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }
}
