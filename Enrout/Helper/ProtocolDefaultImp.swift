//
//  ProtocolInheritor.swift
//  Enrout
//
//  Created by Daniel Expresspay on 26/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Foundation

extension OnPlaceCompletedDelegate {
    func onPlaceSelected(placeComplete: PlaceComplete) {
    }
    
    func onTextChanged(query: String) {
    }
    
    func isSearching() {
    }
    
    func doneSearching(foundPlaces: Bool) {
    }
    
}

extension PlaceCompleteSwipedDelegate {
    func onSwipe(placeComplete: PlaceComplete){}
}

extension ItemCategoryDelegate {
    func onSelect(item: ItemCategory){}
    func onCancel(){}
}

extension SweetAlertDelegate {
    func onCancel(){}
    func onConfirm(){}
}

extension FetchTripsDelegate {
    func onGoingTripsFetched(error: Error?,trips: [Trip]?) {}
    func completedTripsFetched(error: Error?,trips: [Trip]?) {}
    func cancelledTripsFetched(error: Error?,trips: [Trip]?) {}
}

extension CancelOrderDelegate {
    func onConfirm(reasons: String){}
}

extension PaymentCompleteDelegate {
    func onPayentComplete(error: Error?){}
}

extension PaymentSuccessDelegate {
    func onDoneTapped() {}
}
