//
//  Protocols.swift
//  Enrout
//
//  Created by Daniel Expresspay on 18/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Pulley

protocol BottomContentViewControllerInteracted {
    func changeDrawerPosition(pulleyPosition: PulleyPosition)
    func switchdrawer(currentPanel: Panels)
    func removeCurrentPannel()
    func selectViaMapMode(source: PointType, isActive: Bool)
}

protocol OnPlaceCompletedDelegate {
    func onPlaceSelected(placeComplete: PlaceComplete)
    func onTextChanged(query: String)
    func isSearching()
    func doneSearching(foundPlaces: Bool)
}

protocol PlaceCompleteSwipedDelegate {
    func onSwipe(placeComplete: PlaceComplete)
}

protocol ItemCategoryDelegate {
    func onSelect(item: ItemCategory)
    func onCancel()
}

protocol SweetAlertDelegate {
    func onCancel()
    func onConfirm()
}

protocol FetchTripsDelegate {
    func onGoingTripsFetched(error: Error?, trips: [Trip]?)
    func completedTripsFetched(error: Error?, trips: [Trip]?)
    func cancelledTripsFetched(error: Error?, trips: [Trip]?)
}

protocol CancelOrderDelegate {
    func onConfirm(reasons: String)
}

protocol PaymentCompleteDelegate {
    func onPayentComplete(error: Error?)
}

protocol PaymentSuccessDelegate {
    func onDoneTapped()
}

