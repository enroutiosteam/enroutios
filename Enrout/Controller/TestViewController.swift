//
//  TestViewController.swift
//  Enrout
//
//  Created by Daniel Expresspay on 29/11/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit
import GooglePlaces
import SocketIO

class TestViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    var placesClient: GMSPlacesClient!
    var mSocket: SocketIOClient!
    //var socketConnection: SockIOConnection!

    override func viewDidLoad() {
        super.viewDidLoad()
        placesClient = GMSPlacesClient.shared()
        //socketConnection = SockIOConnection()
        //mSocket = SockIOConnection.getSocketInstance()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func placeComplete(_ sender: Any) {
        Toast.success(view: self.view, message: "Fetch google places")
        
        let filter = GMSAutocompleteFilter()
        filter.country = "GH"
        
        
        placesClient.autocompleteQuery("kumasi", bounds: nil, boundsMode: .bias, filter: filter) { (predictions, error) in
            
            if let err = error {
                print(err.localizedDescription)
                Toast.error(view: self.view, message: err.localizedDescription)
                return
            }
            
            guard let predictions = predictions else{
                return
            }
            
            for prediction in predictions {
                print("\(prediction.attributedPrimaryText.string) , \(String(describing: prediction.attributedSecondaryText!.string))")
            }
        }
        
    }
    
//    make payment =======
    
    @IBAction func topUpWallet(_ sender: Any) {
        
       // let myMomo = "0541243508"
        let testMomo = "0541111111"
        Wallet.getInstance(view: self.view, viewController: self).addFunds(momoNumber: testMomo, network: .MTN, amount: 5, vodafoneAuthToken: nil) { (paymentStatus, error) in
            
            print("payment Status => \(paymentStatus) ")
            print("Error => \(String(describing: error?.localizedDescription))")
            Toast.success(view: self.view, message: "Transaction Successful")
     
        }
        
    }
    
    var switchConnection = false
    @IBAction func connectToSocket(_ sender: Any) {
        switchConnection = !switchConnection
        
//        if switchConnection {
//            socketConnection.connect()
//        }else{
//            socketConnection.disconect()
//        }
    }
    
    @IBAction func getWalletTransactions(_ sender: Any) {
       let dialog = AnyProgressDialog.newInstance(view: self.view).show()
        Wallet.getInstance(view: self.view, viewController: self).getTransactions { (transactions, error) in
            dialog.dismis()
            if let err = error {
                print(err.localizedDescription)
                return
            }
            
            Toast.success(view: self.view, message: "Success")
            
        }
    }
    
    
    @IBAction func openFAQ(_ sender: Any) {
        openFaq(viewcontroller: self)
    }
    
    
    @IBAction func openContactUs(_ sender: Any) {
        contactUs(viewcontroller: self)
    }
    
    @IBAction func logout(_ sender: Any) {
        logoutUser(viewController: self, completion: {
            
        })
    }
    
    @IBOutlet weak var avatar: UIImageView!
    
    @IBAction func personalDetails(_ sender: Any) {
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
    
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        print("image selected")
        
        guard let selectedImage = info[.originalImage] as? UIImage else{
            return
        }
        
        avatar.image = selectedImage
        dismiss(animated: true, completion: nil)
        
        var user = ApplicationUser()
        user.email = "kofiNyarko@email.com"
        user.phoneNumber = "0541111111"
        user.fullName = "Kofi Nyarko"
        user.gender = "Male"
        
        let imageData = selectedImage.jpegData(compressionQuality: 0.8)!

        updateUserDetails(viewController: self, user: user, imageData: imageData, completed: { _ in })
    }
    
    @IBAction func requestRider(_ sender: Any) {
        let currLatLng = PosLatLng()
        dispatchRequest(viewController: self, userCurrLatLng: currLatLng, completion: {
            status, message in
            print("status => \(String(describing: status)), message => \(String(describing: message))")
        })
        
    }
    
    @IBAction func estimateTripCost(_ sender: Any) {
        estimateCost(viewController: self, completion: {_,_,_,_ in
        })
    }
    
    
    @IBAction func testCase1(_ sender: Any) {
        applyCoupon(code: "WE-333") { error,coupon  in
            if let err = error {
                print("Error => \(err.localizedDescription)")
            }
            
            print("coupon is worth => \(String(describing: coupon?.value))")
        }
    }
    
    @IBAction func testCase2(_ sender: Any) {
//        getTrips(by: .CANCELLED) { (trips, error) in
//            if let err = error{
//                print("Error => \(err.localizedDescription)")
//                return
//            }
//
//            print(String(describing: trips?.count))
//        }
        
        // rate rider
//        rateRider(viewController: self, riderId: "MgU83f3E8ftA2G02Q17L", on: "FA822171-5704-4222-A3E9-37E78CA24CE7", with: 4.5, and: "Awesome") { (error) in
//            if let err = error {
//                print(err.localizedDescription)
//                return
//            }
//        }
        
        // get rider rating
        getRiderRating(riderId: "MgU83f3E8ftA2G02Q17L") { (rating) in
            if let rating = rating {
                print("rider rating => \(rating)")
            }
        }
    }
    
    @IBAction func testCase3(_ sender: Any) {
        //scheduleNotification(notificationType: "First Notification")
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
