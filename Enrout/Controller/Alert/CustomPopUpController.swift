//
//  CustomPopUpController.swift
//  Enrout
//
//  Created by Jude Botchwey on 02/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit

class CustomPopUpController: UIViewController {

    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var modalImageView: UIImageView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var subHeaderLabel: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    var alertHandler : ((Int) -> ())?
    var subHeaderText: String?
    var headerText: String?
    var doneHidden: Bool?
    var cancelHidden: Bool?
    var displayImage: String?
    
    public static var customPopupController: CustomPopUpController = {
        return UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(identifier: "CustomPopUpController") as! CustomPopUpController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        doneButton.curveCorners()
        
        definesPresentationContext = true
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        displayAlertPrompt(imageName: displayImage!, headerTitle: headerText!, subHeaderTitle: subHeaderText!, doneHidden: self.doneHidden!, cancelHidden: self.cancelHidden!, withHandler: alertHandler!)
    }
    
    
    
    func setupView(){
        backgroundView.layer.cornerRadius = 3.0
        doneButton.layer.cornerRadius = 5.0
        cancelButton.layer.cornerRadius = 5.0
    }

    @IBAction func donePressed(_ sender: Any) {
        alertHandler!(1)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func cancelPressed(_ sender: Any) {
        alertHandler!(0)
        self.dismiss(animated: true, completion: nil)
    }
    
    func configAlert(imageName: String = "modal_image", headerTitle: String = "", subHeaderTitle: String = "", doneHidden: Bool = false, cancelHidden: Bool = false, withHandler handler: @escaping (Int) -> ()){
        displayImage = imageName
        headerText = headerTitle
        subHeaderText = subHeaderTitle
        self.doneHidden = doneHidden
        self.cancelHidden = cancelHidden
        self.alertHandler = handler
    }
    
    private func displayAlertPrompt(imageName: String = "modal_image", headerTitle: String = "", subHeaderTitle: String = "", doneHidden: Bool = false, cancelHidden: Bool = false, withHandler handler: @escaping (Int) -> ()){
        self.modalImageView.image = UIImage(named: imageName)
        self.headerLabel.text = headerTitle
        self.subHeaderLabel.text = subHeaderTitle
        self.doneButton.isHidden = doneHidden
        self.cancelButton.isHidden = cancelHidden
        alertHandler = handler
        
    }
    
    
    
}
