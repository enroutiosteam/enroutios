//
//  LogoutAlertViewController.swift
//  Enrout
//
//  Created by Jude Botchwey on 07/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit
import FirebaseAuth

class LogoutAlertViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func noTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    @IBAction func yesTapped(_ sender: Any) {
        //callLogout function here
        logoutUser(viewController: self, completion: {
            self.performSegue(withIdentifier: "goToMainView", sender: self)
        })
       //
    }
    
 
}
