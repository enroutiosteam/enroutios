//
//  SweetAlertViewController.swift
//  Enrout
//
//  Created by Daniel Expresspay on 29/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit

class SweetAlertViewController: UIViewController {

    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var confirmBtn: UIButton!
    
    @IBOutlet weak var alertTitle: UILabel!
    @IBOutlet weak var message: UILabel!
    
    private var mTitle: String?
    private var mMessage: String?
    private var showCancelBtn: Bool = false
    var delegate: SweetAlertDelegate?
    
    public static var sweetAlertViewController: SweetAlertViewController = {
        return UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(identifier: "SweetAlertViewController") as! SweetAlertViewController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        cancelBtn.curveCorners()
        confirmBtn.curveCorners()
        
        cancelBtn.addTarget(self, action: #selector(onCancelBtnTapped), for: .touchUpInside)
        confirmBtn.addTarget(self, action: #selector(onConfirmBtnTapped), for: .touchUpInside)
    
    }
    
    override func viewDidAppear(_ animated: Bool) {
         alertTitle.text = mTitle
           message.text = mMessage
           if showCancelBtn {
               cancelBtn.isHidden = false
           }else{
               cancelBtn.isHidden = true
           }
    }
    
    @IBAction func onCancelBtnTapped(){
        self.dismiss(animated: true, completion: nil)
        delegate?.onCancel()
    }
    
    @IBAction func onConfirmBtnTapped(){
        self.dismiss(animated: true, completion: nil)
        delegate?.onConfirm()
    }
    
    func config(message: String, title:String? = nil, showCancelBtn: Bool = false, delegate: SweetAlertDelegate? = nil){
        self.mTitle = title == nil ? "Notice!" : title
        self.mMessage = message
        self.showCancelBtn = showCancelBtn
        self.delegate = delegate
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
