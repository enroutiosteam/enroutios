//
//  DestinationDetailSingleTripViewController.swift
//  Enrout
//
//  Created by Daniel Expresspay on 18/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit
import ContactsUI

class DestinationDetailSingleTripViewController: UIViewController {

    @IBOutlet weak var listView: UIView!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var placeTitle: UILabel!
    @IBOutlet weak var placeSubtitle: UILabel!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var recipientNameTF: UITextField!
    @IBOutlet weak var recipientPhoneTF: UITextField!
    @IBOutlet weak var pickContactBtn: UIButton!
    @IBOutlet weak var continueBtn: UIButton!
    var bottomViewController: BottomViewController?
     var placeCompleteController: PlaceCompleteTableViewController = PlaceCompleteTableViewController.placeCompleteViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setTargets()
        continueBtn.curveCorners()
        
        recipientNameTF.delegate = self
        recipientPhoneTF.delegate = self
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("confirm active destinations => \(UIConstants.activeDestinations.count)")
        let destination = UIConstants.activeDestinations[0]
        let placeComplete = PlaceComplete()
        placeComplete.name = destination.place
        placeComplete.description = destination.description
        placeComplete.latitude = destination.pos?.geopoint?.latitude
        placeComplete.longitude = destination.pos?.geopoint?.longitude
         placeCompleteController.rowIsSelectable = false
         placeCompleteController.rowIsSwipable = true
        placeCompleteController.addItem(placeComplete: placeComplete)
        placeCompleteController.swipeCompleteSource = self
        placeCompleteController.point = .DESTINATION
        
        listView.addSubview(placeCompleteController.view)
        placeCompleteController.view.frame = listView.bounds
        
        if getTripType() == .MP {
            addBtn.isEnabled = false
        }else{
            addBtn.isEnabled = true
        }
        
//        let image = UIImage(named: "add_contact")
//        let resizedImage = image?.resizeImage(targetSize: CGSize(width: 30, height: 30))
//        pickContactBtn.setImage(resizedImage, for: .normal)
//
    }
    
    func setTargets(){
        closeBtn.addTarget(self, action: #selector(closeBtnTapped(_:)), for: .touchUpInside)
        continueBtn.addTarget(self, action: #selector(continueBtnTapped(_:)), for: .touchUpInside)
        addBtn.addTarget(self, action: #selector(self.addBtnTapped(_:)), for: .touchUpInside)
        pickContactBtn.addTarget(self, action: #selector(pickContactBtnTapped(_:)), for: .touchUpInside)
    }
    
    @IBAction func addBtnTapped(_ sender: Any){
        placeCompleteController.rowIsSelectable = true
        bottomViewController?.onbackPressed(currentPanel: .destinationpointdetailssingle, additionalData: ["type" : "ADD_DESTINATION"])
    }
    
    @IBAction func pickContactBtnTapped(_ sender: Any){
        let contactPicker = CNContactPickerViewController()
        contactPicker.delegate = self
        contactPicker.displayedPropertyKeys =
            [CNContactGivenNameKey
                , CNContactPhoneNumbersKey]
        self.present(contactPicker, animated: true, completion: nil)
    }
    
    
    @IBAction func closeBtnTapped(_ sender: Any?){
        placeCompleteController.rowIsSelectable = true
        placeCompleteController.rowIsSwipable = false
        bottomViewController?.onbackPressed(currentPanel: .destinationpointdetailssingle, additionalData: ["type" : "BACKPRESSED"])
    }
    
    
    @IBAction func continueBtnTapped(_ sender: Any){
        
        if recipientNameTF.text == ""{
            Toast.error(view: self.view, message: "recipient name required")
            return
        }
        
        if recipientPhoneTF.text == "" {
            Toast.error(view: self.view, message: "recipient phone required")
            return
        }
        
       let destination = UIConstants.activeDestinations[0]
        destination.recipientName = recipientNameTF.text
        destination.recipientNumber = recipientPhoneTF.text
        bottomViewController?.switchdrawer(currentPanel: .destinationpointdetailssingle)
        
        recipientNameTF.text = ""
        recipientPhoneTF.text = ""
    }


}

extension DestinationDetailSingleTripViewController: CNContactPickerDelegate{
    func contactPicker(_ picker: CNContactPickerViewController,
                          didSelect contactProperty: CNContactProperty) {

       }

       func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
           // You can fetch selected name and number in the following way

           // user name
           let userName:String = contact.givenName

           // user phone number
           let userPhoneNumbers:[CNLabeledValue<CNPhoneNumber>] = contact.phoneNumbers
           let firstPhoneNumber:CNPhoneNumber = userPhoneNumbers[0].value


           // user phone number string
           let primaryPhoneNumberStr:String = firstPhoneNumber.stringValue

           recipientNameTF.text = userName
           recipientPhoneTF.text = primaryPhoneNumberStr
           print(primaryPhoneNumberStr)

       }

}

extension DestinationDetailSingleTripViewController: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        bottomViewController?.expandDrawer(animated: true)
    }
}

extension DestinationDetailSingleTripViewController: PlaceCompleteSwipedDelegate {
    func onSwipe(placeComplete: PlaceComplete) {
        UIConstants.activeDestinations = []
        closeBtnTapped(nil)
        bottomViewController?.mainViewController?.reRenderIconsOnMap()
    }
}
