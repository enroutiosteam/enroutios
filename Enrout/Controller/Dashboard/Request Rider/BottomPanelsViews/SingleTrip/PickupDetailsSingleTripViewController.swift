//
//  PickupDetailsSingleTripViewController.swift
//  Enrout
//
//  Created by Daniel Expresspay on 18/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit

class PickupDetailsSingleTripViewController: UIViewController {
    
    @IBOutlet weak var listView: UIView!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var placeTitle: UILabel!
    @IBOutlet weak var placeSubtitle: UILabel!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var itemCategoryTF: UITextField!
    @IBOutlet weak var itemInstructionsTF: UITextField!
    @IBOutlet weak var continueBTN: UIButton!
    var bottomViewController: BottomViewController?
    var placeCompleteController: PlaceCompleteTableViewController = PlaceCompleteTableViewController.placeCompleteViewController

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTargets()
        
        continueBTN.curveCorners()
        // Do any additional setup after loading the view.
        
      
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        // let newImage = image.resizeImage(targetSize: CGSize(width: 20.0, height: 20.0))
         btn.setImage(UIImage(named: "menu_icon"), for: .normal)
         btn.addTarget(self, action:  #selector(self.menuItemCategoryTapped(_:)), for: .touchUpInside)
        
         let rv = UIView(frame: CGRect(x: 0, y: 0, width: btn.frame.width + 10, height: btn.frame.height))
         itemCategoryTF.rightViewMode = .always
         rv.addSubview(btn)
         itemCategoryTF.rightView = rv
        
        
        itemInstructionsTF.addRightIcon(image: UIImage(named: "edit_icon")!)
        
        
    }
    
    @IBAction func menuItemCategoryTapped(_ sender: UIButton?){
        presentItemCategories(presentingController: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
         let pickup = UIConstants.activePickUps[0]
              let placeComplete = PlaceComplete()
              placeComplete.name = pickup.place
              placeComplete.description = pickup.description
              placeComplete.latitude = pickup.pos?.geopoint?.latitude
              placeComplete.longitude = pickup.pos?.geopoint?.longitude
               placeCompleteController.rowIsSelectable = false
               placeCompleteController.rowIsSwipable = true
               placeCompleteController.point = .PICKUP
              placeCompleteController.addItem(placeComplete: placeComplete)
               placeCompleteController.swipeCompleteSource = self
              listView.addSubview(placeCompleteController.view)
              placeCompleteController.view.frame = listView.bounds
        
            if getTripType() == .MD {
                addBtn.isEnabled = false
            }else{
                addBtn.isEnabled = true
            }
        
    }
    
    
    
    
    func setupTargets(){
        closeBtn.addTarget(self, action: #selector(self.closePanel(_:)), for: .touchUpInside)
        continueBTN.addTarget(self, action: #selector(moveToNextPanel(_:)), for: .touchUpInside)
        addBtn.addTarget(self, action: #selector(self.addBtnTapped), for: .touchUpInside)
        itemCategoryTF.delegate = self
        itemInstructionsTF.delegate = self
    }
    
    @IBAction func itemCategoryTapped(_ sender: Any){
        print("show item categories")
        presentItemCategories(presentingController: self)
    }
    
    @IBAction func closePanel(_ sender: Any?) {
        
        let alert = UIAlertController(title: "Notice", message: "This action will reset all the selected locations. If you intend to add pickup point, use the add button below instead", preferredStyle: .actionSheet)
        let continueBtn = UIAlertAction(title: "Proceed", style: .destructive, handler: {
            _ in
            
            self.placeCompleteController.rowIsSelectable = true
            self.placeCompleteController.rowIsSwipable = false
            self.bottomViewController?.mainViewController?.refreshSession()
        })
        
        let cancelBtn = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        alert.addAction(continueBtn)
        alert.addAction(cancelBtn)
        self.present(alert, animated: true, completion: nil)
//        if UIConstants.activeDestinations.count > 0 {
//
//        }else{
//            UIConstants.activePickUps = []
//            bottomViewController?.onbackPressed(currentPanel: .pickpointdetailsingle)
//        }
    }
    
    @IBAction func addBtnTapped(){
       placeCompleteController.rowIsSelectable = true
        bottomViewController?.onbackPressed(currentPanel: .pickpointdetailsingle)
    }
    
    @IBAction func moveToNextPanel(_ sender: Any){
        let pickup = UIConstants.activePickUps[0]
        pickup.itemCategory = itemCategoryTF.text
        pickup.instruction = itemInstructionsTF.text
        itemInstructionsTF.endEditing(true)
        bottomViewController?.switchdrawer(currentPanel: .pickpointdetailsingle)
    }

}


extension PickupDetailsSingleTripViewController: PlaceCompleteSwipedDelegate {
    func onSwipe(placeComplete: PlaceComplete) {
        closePanel(nil)
        //bottomViewController?.mainViewController?.refreshSession()
    }
}

extension PickupDetailsSingleTripViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == itemCategoryTF {
            itemCategoryTF.endEditing(true)
            // show categories to choose from
            print("show item categories")
            presentItemCategories(presentingController: self)
            
        }else if textField == itemInstructionsTF {
            bottomViewController?.expandDrawer(animated: true)
        }
    }
}

extension PickupDetailsSingleTripViewController: ItemCategoryDelegate {
    func onSelect(item: ItemCategory) {
        itemCategoryTF.text = item.label
    }
}
