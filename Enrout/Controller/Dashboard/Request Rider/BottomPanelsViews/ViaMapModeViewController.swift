//
//  ViaMapModeViewController.swift
//  Enrout
//
//  Created by Daniel Expresspay on 27/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit

class ViaMapModeViewController: UIViewController {
   
    @IBOutlet weak var titleTv: UILabel!
    @IBOutlet weak var placeBtn: UIButton!
    @IBOutlet weak var loadingShimmerView: UIView!
    var isLoading = false
    var bottomViewController: BottomViewController?
    var placeComplete: PlaceComplete?
    @IBOutlet weak var closeBtn: UIButton!
    
    var titleText: String?
    var pointType: PointType = .PICKUP

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupview()
        placeBtn.addTarget(self, action: #selector(self.acceptPlace(_:)), for: .touchUpInside)
        closeBtn.addTarget(self, action: #selector(self.closeBtnTapped), for: .touchUpInside)
    }
    
    private func setupview(){
        titleTv.text = titleText
        placeBtn.setTitle(placeComplete?.name, for: .normal)
        loadShimmer()
        if isLoading {
            loadingShimmerView.isHidden = false
            placeBtn.isHidden = true
        }else{
            loadingShimmerView.isHidden = true
            placeBtn.isHidden = false
        }
    }
    
    @IBAction private func acceptPlace(_ sender: Any){
        if self.pointType == .PICKUP{
            self.bottomViewController?.showPickUpDetailsForm(currentPanel: .searchpickpoint, placeComplete: placeComplete!)
        }else{
            self.bottomViewController?.showDestinationDetail(currentPanel: .searchdestinationpoint, placeComplete: placeComplete!)
        }
        
    }
    
    private func loadShimmer(){
        loadingShimmerView.addSubview(ShimmerViewController.shimmerViewController.view)
        ShimmerViewController.shimmerViewController.view.frame = loadingShimmerView.bounds
    }
    
    @IBAction private func closeBtnTapped(_ sender: Any){
        // disable viaMapMode
        bottomViewController?.leaveViaMapMode()
    }

    
    public lazy var viaMapPanelController: ViaMapModeViewController = {
        return UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(identifier: "ViaMapModeViewController") as! ViaMapModeViewController
    }()
    
    func config(title: String, place: PlaceComplete){
        titleText = title
        placeComplete = place
    }
    
    private func setLoadingState(){
        isLoading = true
        setupview()
    }
    
    func setSettleState(place: PlaceComplete){
        // get name of place, while showing the loading view
        setLoadingState()
        getAddressFromLatLon(pdblLatitude: place.latitude!, withLongitude: place.longitude!) { (address) in
            place.name = address ?? "Unkown Street"
            self.isLoading = false
            self.placeComplete = place
            self.setupview()
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
