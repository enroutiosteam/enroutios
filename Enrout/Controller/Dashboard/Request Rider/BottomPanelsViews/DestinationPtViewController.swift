//
//  DestinationPtViewController.swift
//  Enrout
//
//  Created by Daniel Expresspay on 18/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit
import CoreLocation

class DestinationPtViewController: UIViewController {

    @IBOutlet weak var placeCompleteView: UIView!
    var shimmerViewController = ShimmerViewController.shimmerViewController
    var placeCompleteViewController = PlaceCompleteTableViewController.placeCompleteViewController
    var currentlyAddedController: UIViewController?
    
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var selectViaMapBtn: UIButton!
    @IBOutlet weak var useCurrentLocationBtn: UIButton!
    var bottomViewController: BottomViewController?
    
    let manager = CLLocationManager()
    var dialog: AnyProgressDialog?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupTargets()
        // Do any additional setup after loading the view.
         let closeBtnTF = UIButton(type: .close)
        closeBtnTF.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        closeBtnTF.addTarget(self, action: #selector(self.clearTextField), for: .touchUpInside)
    
        let rightView = UIView(frame: CGRect(x: 0, y: 0, width: closeBtnTF.frame.width + 10, height: closeBtnTF.frame.height))
        rightView.addSubview(closeBtnTF)
//        closeBtnTF.layoutMargins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
        
        searchTF.rightView = rightView
        searchTF.rightViewMode = .always
        searchTF.delegate = self
        searchTF.tintColor = UIColor.black
        searchTF.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        searchTF.delegate = self
        
        dialog = AnyProgressDialog.newInstance(view: self.view)
       
    }
    
    func setupTargets(){
    closeBtn.addTarget(self, action: #selector(self.closeBtnTapped(_:)), for: .touchUpInside)
    useCurrentLocationBtn.addTarget(self, action: #selector(useMyCurrentLocationTapped(_:)), for: .touchUpInside)
        selectViaMapBtn.addTarget(self, action: #selector(self.selectViaMapBtnTapped(_:)), for: .touchUpInside)
    }
    
    @IBAction func clearTextField(_ sender: UIButton?) {
        // clear text in text field
        searchTF.text = ""
        searchTF.endEditing(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
       placeCompleteViewController.rowIsSwipable = false
       placeCompleteViewController.rowIsSelectable = true
       placeCompleteViewController.point = .DESTINATION
       self.placeCompleteViewController.placeCompleteSource = self
    }
    
    @IBAction func textFieldDidChange(_ textField: UITextField) {
        showShimmer()
        placeCompleteViewController.onTextChanged(query: textField.text!)
       
    }
    
    func showShimmer(){
        shimmerViewController.view.isHidden = false
        swapViewControllerToContainer(viewController: shimmerViewController)
    }
    
    @IBAction private func closeBtnTapped(_ sender: Any){
        self.placeCompleteViewController.rowIsSwipable = true
        self.placeCompleteViewController.rowIsSelectable = false
        bottomViewController?.onbackPressed(currentPanel: .searchdestinationpoint)
    }
    
    @IBAction func selectViaMapBtnTapped(_ sender: Any){
        bottomViewController?.selectViaMapMode(source: .DESTINATION, isActive: true)
    }
    
    @IBAction private func useMyCurrentLocationTapped(_ sender: Any){
        if UIConstants.lastKnowLocation.latitude != nil && UIConstants.lastKnowLocation.longitude != nil{
            afterCurrentLocationFound(lat:UIConstants.lastKnowLocation.latitude!, lng: UIConstants.lastKnowLocation.longitude!)
            
            return
        }
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestAlwaysAuthorization()
        manager.requestLocation()
        
        dialog?.setText(text: "Getting your location")
        dialog?.show()
    }
    
    func showPlaceCompleteList(){
            placeCompleteViewController.rowIsSwipable = false
            placeCompleteViewController.rowIsSelectable = true
           swapViewControllerToContainer(viewController: placeCompleteViewController)
    }
    
    func swapViewControllerToContainer(viewController: UIViewController){
        
        removeViewControllerFromContainer()
        
        placeCompleteView.addSubview(viewController.view)
        viewController.view.frame = placeCompleteView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParent: self)
        currentlyAddedController = viewController
    }
    
    func removeViewControllerFromContainer(){
       currentlyAddedController?.willMove(toParent: nil)
      // Remove Child View From Superview
      currentlyAddedController?.view.removeFromSuperview()
      // Notify Child View Controller
      currentlyAddedController?.removeFromParent()
    }
    
    func afterCurrentLocationFound(lat: Double, lng: Double){
        let dialog = AnyProgressDialog.newInstance(view: self.view).show()
          getAddressFromLatLon(pdblLatitude: lat, withLongitude: lng) { (address) in
              dialog.dismis()
              
              if address == nil {
                  Toast.error(view: self.view, message: "Location not found")
                  return
              }
              
              let resultingPlacecomplete = PlaceComplete()
              resultingPlacecomplete.name = address
              resultingPlacecomplete.longitude = lng
              resultingPlacecomplete.latitude = lat
              
              self.bottomViewController!.showDestinationDetail(currentPanel: Panels.searchdestinationpoint,placeComplete: resultingPlacecomplete)
              
          }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DestinationPtViewController: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        bottomViewController?.expandDrawer(animated: true)
    }
}


extension DestinationPtViewController: OnPlaceCompletedDelegate {
    
    func doneSearching(foundPlaces: Bool) {
        if foundPlaces {
          showPlaceCompleteList()
        }else{
            shimmerViewController.view.isHidden = true
        }
    }
    
    func onPlaceSelected(placeComplete: PlaceComplete) {
        
        print("destination place is selected")
        searchTF.endEditing(true)
        // get the lat lng of selected place
        getLatLngFromPlaceId(viewController: self, place: placeComplete) { (resultingPlacecomplete) in
            self.searchTF.endEditing(true)
            self.bottomViewController!.showDestinationDetail(currentPanel: Panels.searchdestinationpoint,placeComplete: resultingPlacecomplete)
            self.clearTextField(nil)
//            self.showPickUpDetailsForm(placeComplete: resultingPlacecomplete)
        }
        
    }
    
}


extension DestinationPtViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        dialog?.setText(text: "Please wait")
        dialog?.dismis()
        if locations.first != nil {
            print("got location use current location")
                   guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
                      print("locations = \(locValue.latitude) \(locValue.longitude)")
                  
            UIConstants.lastKnowLocation.latitude = locValue.latitude
            UIConstants.lastKnowLocation.longitude = locValue.longitude
            afterCurrentLocationFound(lat: locValue.latitude, lng: locValue.longitude)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        dialog?.setText(text: "Please wait")
        dialog?.dismis()
        Toast.error(view: self.view, message: "\(error.localizedDescription)")
    }
}
