//
//  SenderDetailsViewController.swift
//  Enrout
//
//  Created by Daniel Expresspay on 28/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit
import ContactsUI

class SenderDetailsViewController: UIViewController , CNContactPickerDelegate{
    
    
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var senderName: UITextField!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var pickContactBtn: UIButton!
    @IBOutlet weak var senderPhone: UITextField!
    var bottomViewController: BottomViewController?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let image = UIImage(named: "add_contact")
        let resizedImage = image?.resizeImage(targetSize: CGSize(width: 30, height: 30))
        pickContactBtn.setImage(resizedImage, for: .normal)
        closeBtn.addTarget(self, action: #selector(closeBtnTapped(_:)), for: .touchUpInside)
        continueBtn.addTarget(self, action: #selector(continueBtnTapped(_:)), for: .touchUpInside)
        continueBtn.curveCorners()
        pickContactBtn.addTarget(self, action: #selector(pickContact(_:)), for: .touchUpInside)
        
        senderName.delegate = self
        senderPhone.delegate = self
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        senderName.text = Constants.applicationUser?.fullName
        senderPhone.text = Constants.applicationUser?.phoneNumber
    }
    
  
    @IBAction func closeBtnTapped(_ sender: Any){
        bottomViewController?.onbackPressed(currentPanel: .senderDetails)
    }
    
    @IBAction func pickContact(_ sender: Any) {
        let contactPicker = CNContactPickerViewController()
        contactPicker.delegate = self
        contactPicker.displayedPropertyKeys =
            [CNContactGivenNameKey
                , CNContactPhoneNumbersKey]
        self.present(contactPicker, animated: true, completion: nil)
    }
    
    @IBAction func continueBtnTapped(_ sender: Any){
        print("continue btn tapped")
        if senderName.text == "" || senderPhone.text == "" {
            Toast.error(view: self.view, message: "Sender details required")
            return
        }
 
        if getTripType() == .SPSD{
            UIConstants.activePickUps[0].pickUpName = senderName.text
             UIConstants.activePickUps[0].pickUpPersonPhone = senderPhone.text
        }else{
            UIConstants.activeTrip.senderName = senderName.text
            UIConstants.activeTrip.senderPhoneNumber = senderPhone.text
        }
        
        senderName.endEditing(true)
        senderPhone.endEditing(true)
        bottomViewController?.switchdrawer(currentPanel: .senderDetails)
    }
    
    func contactPicker(_ picker: CNContactPickerViewController,
                       didSelect contactProperty: CNContactProperty) {

    }

    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        // You can fetch selected name and number in the following way

        // user name
        let userName:String = contact.givenName

        // user phone number
        let userPhoneNumbers:[CNLabeledValue<CNPhoneNumber>] = contact.phoneNumbers
        let firstPhoneNumber:CNPhoneNumber = userPhoneNumbers[0].value


        // user phone number string
        let primaryPhoneNumberStr:String = firstPhoneNumber.stringValue

        senderName.text = userName
        senderPhone.text = primaryPhoneNumberStr
        print(primaryPhoneNumberStr)

    }

    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SenderDetailsViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        bottomViewController?.expandDrawer(animated: true)
    }
}
