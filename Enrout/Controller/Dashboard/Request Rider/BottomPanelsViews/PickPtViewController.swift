//
//  PickPtViewController.swift
//  Enrout
//
//  Created by Daniel Expresspay on 13/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit
import Pulley
import CoreLocation

class PickPtViewController: UIViewController, PulleyDrawerViewControllerDelegate, UITextFieldDelegate {
    
    var shimmerViewController = ShimmerViewController.shimmerViewController
    var placeCompleteViewController = PlaceCompleteTableViewController.placeCompleteViewController
    var currentlyAddedController: UIViewController?
    var dialog: AnyProgressDialog?
    
    @IBOutlet weak var closeBtn: UIButton!
    let manager = CLLocationManager()

    @IBOutlet weak var searchPickpointTF: UITextField!
    @IBOutlet weak var selectViaMapBT: UIButton!
    @IBOutlet weak var useCurrentLocationBT: UIButton!
    @IBOutlet weak var selectViaMapSV: UIStackView!
    @IBOutlet weak var useMyCurrentLocationSV: UIStackView!
    @IBOutlet weak var creative: UIImageView!
    var bottomViewController: BottomViewController?
    var showhideLocationButtons = false
    @IBOutlet weak var placeCompleteContainerView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let closeBtnTF = UIButton(type: .close)
        closeBtnTF.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        closeBtnTF.addTarget(self, action: #selector(self.clearTextField), for: .touchUpInside)
    
        let rightView = UIView(frame: CGRect(x: 0, y: 0, width: closeBtnTF.frame.width + 10, height: closeBtnTF.frame.height))
        rightView.addSubview(closeBtnTF)
//        closeBtnTF.layoutMargins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
        //magnifyingglass
        
        let leftBtn = UIButton()
        leftBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        leftBtn.setImage(UIImage(systemName: "magnifyingglass")?.withTintColor(UIColor.black), for: .normal)
        
        let leftView = UIView(frame: CGRect(x: 0, y: 0, width: leftBtn.frame.width + 15, height: leftBtn.frame.height))
        leftView.contentMode = .center
         leftView.addSubview(leftBtn)
        
        searchPickpointTF.leftView = leftView
        searchPickpointTF.leftViewMode = .always
  
        
        searchPickpointTF.rightView = rightView
        searchPickpointTF.rightViewMode = .whileEditing
        
        searchPickpointTF.delegate = self
        searchPickpointTF.tintColor = UIColor.black
        searchPickpointTF.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        
        selectViaMapSV.isHidden = true
        useMyCurrentLocationSV.isHidden = true
        
        // Do any additional setup after loading the view.
        useCurrentLocationBT.addTarget(self, action: #selector(self.useCurrentLocationTapped(_:)), for: .touchUpInside)
        

        selectViaMapBT.addTarget(self, action: #selector(selectViapMapMode(_:)), for: .touchUpInside)
        
        closeBtn.isHidden = true
        closeBtn.addTarget(self, action: #selector(closeBtnTapped), for: .touchUpInside)
        
        dialog = AnyProgressDialog.newInstance(view: self.view)
       
        
    }
    
    @IBAction func closeBtnTapped(){
        bottomViewController?.onbackPressed(currentPanel: .searchpickpoint)
    }
    
    @IBAction func textFieldDidChange(_ textField: UITextField) {
        showShimmer()
        placeCompleteViewController.onTextChanged(query: textField.text!)
        if textField.text == "" {
            resetCreative()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
         self.placeCompleteViewController.placeCompleteSource = self
        self.placeCompleteViewController.point = .PICKUP
        placeCompleteViewController.rowIsSwipable = false
        placeCompleteViewController.rowIsSelectable = true
        
        if UIConstants.activePickUps.count < 1 {
            closeBtn.isHidden = true
        }else{
            closeBtn.isHidden = false
        }
    }
    
    @IBAction func selectViapMapMode(_ sender: Any){
        searchPickpointTF.endEditing(true)
        //Toast.success(view: self.view, message: "Select via map tapped")
        bottomViewController?.selectViaMapMode(source: .PICKUP, isActive: true)
    }
    
    
    func showShimmer(){
        creative.isHidden = true
        swapViewControllerToContainer(viewController: shimmerViewController)
    }
    
    func showPlaceCompleteList(){
        creative.isHidden = true
        placeCompleteViewController.rowIsSwipable = false
        placeCompleteViewController.rowIsSelectable = true
        swapViewControllerToContainer(viewController: placeCompleteViewController)
    }
    
    func resetCreative(){
        removeViewControllerFromContainer()
        creative.isHidden = false
    }
    
    func swapViewControllerToContainer(viewController: UIViewController){
        
        removeViewControllerFromContainer()
        
        placeCompleteContainerView.addSubview(viewController.view)
        viewController.view.frame = placeCompleteContainerView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParent: self)
        currentlyAddedController = viewController
    }
    
    func removeViewControllerFromContainer(){
       currentlyAddedController?.willMove(toParent: nil)
      // Remove Child View From Superview
      currentlyAddedController?.view.removeFromSuperview()
      // Notify Child View Controller
      currentlyAddedController?.removeFromParent()
    }
    
    
    
    @IBAction func useCurrentLocationTapped(_ sender: Any) {
        print("use current location tapped")
        
        if UIConstants.lastKnowLocation.latitude != nil && UIConstants.lastKnowLocation.longitude != nil {
            afterUserLocationFound(lat: UIConstants.lastKnowLocation.latitude!, lng: UIConstants.lastKnowLocation.longitude!)
            return
        }
        
        searchPickpointTF.endEditing(true)
       // getCurrentLocation(viewController: self)
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestAlwaysAuthorization()
        manager.requestLocation()
        
        dialog?.setText(text: "Finding location")
        dialog?.show()
        //bottomViewController?.switchdrawer(currentPanel: .searchpickpoint)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        showhideLocationButtons = true
        if textField == searchPickpointTF {
             searchPickpointTF.becomeFirstResponder()
             bottomViewController?.changeDrawerPosition(pulleyPosition: .open)
        }
    }
    

    
    @IBAction func clearTextField(_ sender: UIButton?) {
        // clear text in text field
        searchPickpointTF.text = ""
        resetCreative()
        searchPickpointTF.endEditing(true)
    }

    
    
    func drawerPositionDidChange(drawer: PulleyViewController, bottomSafeArea: CGFloat) {
        print("did change drawer position called ")
        print("position => \(drawer.drawerPosition)")
        if drawer.drawerPosition == PulleyPosition.open && showhideLocationButtons {
            setView(view: selectViaMapSV, hidden: false)
            setView(view: useMyCurrentLocationSV, hidden: false)
            
        }
        
        if drawer.drawerPosition == PulleyPosition.collapsed  && showhideLocationButtons {
            searchPickpointTF.endEditing(true)
            setView(view: selectViaMapSV, hidden: true)
            setView(view: useMyCurrentLocationSV, hidden: true)
            resetCreative()
        }
        
        if drawer.drawerPosition == PulleyPosition.partiallyRevealed {
            print("drawer partialy revealed pp")
        }
    }
    
    func afterUserLocationFound(lat: Double, lng: Double){
        let dialog = AnyProgressDialog.newInstance(view: self.view).show()
        getAddressFromLatLon(pdblLatitude: lat, withLongitude: lng) { (address) in
            dialog.dismis()
            
            if address == nil {
                Toast.error(view: self.view, message: "Location not found")
                return
            }
            
            let resultingPlacecomplete = PlaceComplete()
            resultingPlacecomplete.name = address
            resultingPlacecomplete.longitude = lng
            resultingPlacecomplete.latitude = lat
            
            self.bottomViewController!.showPickUpDetailsForm(currentPanel: Panels.searchpickpoint,placeComplete: resultingPlacecomplete)
            
        }
    }
    
    
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            view.isHidden = hidden
        })
    }

}


extension PickPtViewController: OnPlaceCompletedDelegate {
    
    func doneSearching(foundPlaces: Bool) {
        if foundPlaces {
             showPlaceCompleteList()
        }else{
            resetCreative()
        }
    }
    
    func onPlaceSelected(placeComplete: PlaceComplete) {
        // get the lat lng of selected place
        getLatLngFromPlaceId(viewController: self, place: placeComplete) { (resultingPlacecomplete) in
            self.searchPickpointTF.endEditing(true)
            self.bottomViewController!.showPickUpDetailsForm(currentPanel: Panels.searchpickpoint,placeComplete: resultingPlacecomplete)
            
            self.clearTextField(nil)
           
        }
        
    }
    
}

extension PickPtViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        dialog?.setText(text: "Please wait")
        dialog?.dismis()
               Toast.error(view: self.view, message: "Unable to get your location")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        dialog?.setText(text: "Please wait")
        dialog?.dismis()
        print("location update receieved")
        if locations.first != nil {
            print("got location use current location")
           guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
              print("locations = \(locValue.latitude) \(locValue.longitude)")

            UIConstants.lastKnowLocation.latitude = locValue.latitude
            UIConstants.lastKnowLocation.longitude = locValue.longitude
            afterUserLocationFound(lat: locValue.latitude, lng: locValue.longitude)
           
        }
        
    }
}
