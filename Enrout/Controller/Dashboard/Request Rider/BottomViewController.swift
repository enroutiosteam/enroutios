//
//  BottomViewController.swift
//  Enrout
//
//  Created by Daniel Expresspay on 17/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit
import Pulley
import GoogleMaps
import ContactsUI

class BottomViewController: UIViewController {
    
    
    var currentViewController: UIViewController?
    var mainViewController: MainViewController?
    var inSelectViaMapMode = false
    var currentPointType: PointType = .PICKUP
    var mCurrentLocation: PlaceComplete?
    
    private lazy var pickupPtViewController: PickPtViewController? = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(identifier: "PickPtViewController") as? PickPtViewController
        return viewController
    }()
    
    private lazy var pickupDetailsSingleViewController: PickupDetailsSingleTripViewController? = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(identifier: "PickupDetailsSingleTripViewController") as? PickupDetailsSingleTripViewController
        return viewController
        
    }()
    
    private lazy var pickupMultiTableViewController: PickupMultiTableViewController? = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(identifier: "PickupMultiTableViewController") as? PickupMultiTableViewController
        return viewController
        
    }()
    
    private lazy var viaMapModeViewController: ViaMapModeViewController? = {
           let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
           let viewController = storyboard.instantiateViewController(identifier: "ViaMapModeViewController") as? ViaMapModeViewController
           return viewController
           
       }()
    
    private lazy var destinationPtViewController: DestinationPtViewController? = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        let viewController = storyboard.instantiateViewController(identifier:"DestinationPtViewController") as? DestinationPtViewController
        
        return viewController
    }()
   
    
    private lazy var destMultiTableViewController: DestMultiTableViewController? = {
           let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
           
           let viewController = storyboard.instantiateViewController(identifier:"DestMultiTableViewController") as? DestMultiTableViewController
           
           return viewController
       }()
    
    private lazy var destinationDetailSingleViewController: DestinationDetailSingleTripViewController? = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        let viewController = storyboard.instantiateViewController(identifier:"DestinationDetailSingleTripViewController") as? DestinationDetailSingleTripViewController
        
        return viewController
    }()
    
    private lazy var checkoutTableViewController: CheckoutTableViewController = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "CheckoutTableViewController") as! CheckoutTableViewController
        
        return viewController
    }()
    
    private lazy var senderDetailsViewController: SenderDetailsViewController? = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(identifier: "SenderDetailsViewController") as? SenderDetailsViewController
        return viewController
    }()
    
//    private lazy var
    
  
    func refreshPanels(){
        if UIConstants.activePickUps.count < 1 {
             add(asChildViewController: pickupPtViewController!)
        }else if UIConstants.activeTrip.isPickFromVendor{
            // vendor has been selected
            //add(asChildViewController: pickupPtViewController!)
            switchdrawer(currentPanel: .senderDetails)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("view will appear called")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        pickupPtViewController?.bottomViewController = self
        pulleyViewController?.initialDrawerPosition = PulleyPosition.partiallyRevealed
       refreshPanels()
        
        // get the user's actual current location
        mCurrentLocation = PlaceComplete()
        mCurrentLocation?.name = "Unamed Road"
        mCurrentLocation?.latitude = 5.5912045
        mCurrentLocation?.longitude = -0.2497703
        
    }
    
    private lazy var trackMainViewController: TrackMapViewController = {
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let viewcontroller = storyboard.instantiateViewController(identifier: "TrackMapViewController") as! TrackMapViewController
            return viewcontroller
        }()
      
      private lazy var drawerContentVC: TrackBottomViewController = {
          let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
          let viewcontroller = storyboard.instantiateViewController(identifier: "TrackBottomViewController") as! TrackBottomViewController
          return viewcontroller
      }()
    
    // handle notification
//    @IBAction func showSpinningWheel(_ notification: NSNotification) {
//
//      print("showSpinningWheel called")
//
//      if let tripData = notification.userInfo?["trip"] as? [String:Any] {
//      // do something with your image
//          let trip = Trip(dictionary: tripData)
//          openTrackTripVC(presenter: self, drawerContentVC: drawerContentVC, trackMainViewController: trackMainViewController, trip: trip)
//      }
//     }
    
//    override func viewDidAppear(_ animated: Bool) {
//         // Register to receive notification in your class
//           NotificationCenter.default.addObserver(self, selector: #selector(self.showSpinningWheel(_:)), name: NSNotification.Name(rawValue: "notificationTrip"), object: nil)
//    }
    
    
    private func add(asChildViewController viewController:
        UIViewController, animatefromLeft: Bool = true, animate: Bool = true) {
                
//        viewController.view.alpha = 0
//        viewController.view.layoutIfNeeded()
        
        removeCurrentViewController()
    
        //options: animatefromLeft ? .layoutSubviews : .layoutSubviews
        UIView.transition(with: self.view, duration: 0.5, options: .layoutSubviews, animations: {

            // Add Child View Controller
           self.addChild(viewController)
           
           // Add Child View as Subview
           self.view.addSubview(viewController.view)

           // Configure Child View
           viewController.view.frame = self.view.bounds
           viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]

           // Notify Child View Controller
           viewController.didMove(toParent: self)
            
           
//           viewController.view.alpha = 1
//           self.currentViewController?.view.alpha = 0
            
            
        }, completion: {
            finished in
            self.currentViewController = viewController
        })
        
//        UIView.animate(withDuration: 0.5, delay: 0.1, options: .transitionFlipFromLeft, animations:  {
//
//        }) {
//            finished in
//
//        }
    }
    
    func setMapMode(mapMode: MapModes, cordinate: CLLocationCoordinate2D){
        switch mapMode {
        case .beganDraggin:
           // if is select via map, show loader
            break
        case .settled:
         // if is select via map, show place with geocoder
            if inSelectViaMapMode{
                let tempPlaceComplete = PlaceComplete()
                tempPlaceComplete.longitude = cordinate.longitude
                tempPlaceComplete.latitude = cordinate.latitude
                viaMapModeViewController?.setSettleState(place: tempPlaceComplete)
            }
            break
        }
    }
          
      private func removeCurrentViewController(){
           currentViewController?.willMove(toParent: nil)
           // Remove Child View From Superview
           currentViewController?.view.removeFromSuperview()
           // Notify Child View Controller
           currentViewController?.removeFromParent()
       }
    
    func leaveViaMapMode() {
        inSelectViaMapMode = false
        mainViewController?.selectViaMapMode(source: currentPointType, isActive: false)
        pulleyViewController?.setDrawerPosition(position: .partiallyRevealed, animated: true)
        if currentPointType == .PICKUP{
            add(asChildViewController: self.pickupPtViewController!)
        }else if currentPointType == .DESTINATION {
            add(asChildViewController: self.destinationPtViewController!)
        }
    }
    
//    helper method for outside clases to trigger drawer
    func expandDrawer(animated: Bool){
        pulleyViewController?.setDrawerPosition(position: .open, animated: animated)
    }
    
    func partiallyRevealDrawer(animated: Bool){
        pulleyViewController?.setDrawerPosition(position: .partiallyRevealed, animated: animated)
    }
    
    func collapseDrawer(animated: Bool){
        pulleyViewController?.setDrawerPosition(position: .collapsed, animated: animated)
    }
    
    // call this func any time place complete is ready for pickup
    func showPickUpDetailsForm(currentPanel: Panels, placeComplete: PlaceComplete){
//         check if the selected location is with active locations
        validatePlace(viewController: self, placeComplete: placeComplete, pointType: .PICKUP) { (status, message) in
            if !status {
                //present a dialog with the mesage
                let title = "ENROUT is not operating in this area"
                presentModalController(presentingController: self, imageName: "modal_image", headerTitle: title, subHeaderTitle: message, doneHidden: false, cancelHidden: true, withHandler: { result in })
                
                return
            }
            
            
            let pickup = Pickup()
            pickup.isArrived = false
            pickup.isArrivedNotified = false
            pickup.isPickedup = false
            pickup.isPickedupNotified = false
    //        pickup1.itemCategory = "Book"
            pickup.pickedupAt = nil
    //        pickup1.pickUpName = "Yaa Daniels"
//            pickup.pickUpPersonPhone = "233551111121"
            pickup.place = placeComplete.name
            pickup.description = placeComplete.description
            
            var ppPos = Pos()
            ppPos.geohash = Constants.getGeoHash(latitude: placeComplete.latitude!, longitude: placeComplete.longitude!)
            ppPos.geopoint = PosLatLng(latitude: placeComplete.latitude!, longitude: placeComplete.longitude!)
            
            pickup.pos = ppPos
            
            UIConstants.activePickUps.append(pickup)
            self.mainViewController?.reRenderIconsOnMap()
            // move to next page
          self.switchdrawer(currentPanel: currentPanel)
        }

    }
    
    func showDestinationDetail(currentPanel: Panels, placeComplete: PlaceComplete){
        
        validatePlace(viewController: self, placeComplete: placeComplete, pointType: .DESTINATION) { (status, message) in
            if !status {
               //present a dialog with the mesage
               let title = "ENROUT is not operating in this area"
               presentModalController(presentingController: self, imageName: "modal_image", headerTitle: title, subHeaderTitle: message, doneHidden: false, cancelHidden: true, withHandler: { result in })
               
               return
           }
            
            let destination = Destination()
                destination.deliveredAt = Date()
                destination.isArrived = false
                destination.isArrivedNotified = false
                destination.isDelivered = false
                destination.isDeliveredNotified = false
                destination.place = placeComplete.name
            destination.description = placeComplete.description
//                destination.recipientName = "Jude Unknown"
//                destination.recipientNumber = "233544433321"
            
                var destPos = Pos()
                
                destPos.geohash = Constants.getGeoHash(latitude: placeComplete.latitude!, longitude: placeComplete.longitude!)
                destPos.geopoint = PosLatLng(latitude: placeComplete.latitude!, longitude: placeComplete.longitude!)
            
                destination.pos = destPos
                
            
            UIConstants.activeDestinations.append(destination)
            self.mainViewController?.reRenderIconsOnMap()
            print("active destinations => \(UIConstants.activeDestinations.count)")
            // move to next page
            self.switchdrawer(currentPanel: currentPanel)
        }
    }

}

   

extension BottomViewController: PulleyDrawerViewControllerDelegate{
    
    func collapsedDrawerHeight(bottomSafeArea: CGFloat) -> CGFloat
    {
        // For devices with a bottom safe area, we want to make our drawer taller. Your implementation may not want to do that. In that case, disregard the bottomSafeArea value.
        return (getScreenSize().height / 5) + (pulleyViewController?.currentDisplayMode == .drawer ? bottomSafeArea : 0.0)
    }

    
    func partialRevealDrawerHeight(bottomSafeArea: CGFloat) -> CGFloat
   {
       // For devices with a bottom safe area, we want to make our drawer taller. Your implementation may not want to do that. In that case, disregard the bottomSafeArea value.
       return (getScreenSize().height / 2) + (pulleyViewController?.currentDisplayMode == .drawer ? bottomSafeArea : 0.0)
   }
    
    func drawerPositionDidChange(drawer: PulleyViewController, bottomSafeArea: CGFloat) {
        let pulleyDelegate = currentViewController as? PulleyDrawerViewControllerDelegate
        pulleyDelegate?.drawerPositionDidChange?(drawer: drawer, bottomSafeArea: bottomSafeArea)
        mainViewController?.drawerPositionDidChange(drawer: drawer, bottomSafeArea: bottomSafeArea)
        
        if drawer.drawerPosition == PulleyPosition.collapsed {
           
        }

        if drawer.drawerPosition == PulleyPosition.partiallyRevealed {
             if inSelectViaMapMode {
                 leaveViaMapMode()
             }
        }
      }
}

extension BottomViewController: BottomContentViewControllerInteracted {
    
    func removeCurrentPannel() {
        removeCurrentViewController()
    }
    
    func changeDrawerPosition(pulleyPosition: PulleyPosition) {
        if pulleyPosition == PulleyPosition.open {
            pulleyViewController?.setDrawerPosition(position: .open, animated: true)
        }
    }
    
    
    func selectViaMapMode(source: PointType, isActive: Bool) {
        print("bottom view in select via map mode")
        currentPointType = source
        inSelectViaMapMode = isActive
        mainViewController?.selectViaMapMode(source: source, isActive: isActive)
        pulleyViewController?.setDrawerPosition(position: .collapsed, animated: true)
        
        if isActive {
            removeCurrentPannel()
            let mTitle = source == .PICKUP ? "Select pick up point" : "Select destination point"
            self.viaMapModeViewController?.bottomViewController = self
            self.viaMapModeViewController?.pointType = source
             // replace unamed road with current location
            self.viaMapModeViewController?.config(title: mTitle, place: mCurrentLocation!)
           
            add(asChildViewController: self.viaMapModeViewController!)
        }

    }
    
    private func switchToPickUpDetails(){
        let tripType = getTripType()
        if tripType == .SPSD {
            pickupDetailsSingleViewController?.bottomViewController = self
            add(asChildViewController: pickupDetailsSingleViewController!)
        }else if tripType == .MP {
            pickupMultiTableViewController?.bottomViewController = self
            add(asChildViewController: pickupMultiTableViewController!)
//            print("multiple pickup")
//            Toast.success(view: self.view, message: "In multiple pick up mode")
            
        }
       
    }
    
    private func switchDestDetails(){
        let tripType = getTripType()
        if tripType == .SPSD {
            destinationDetailSingleViewController?.bottomViewController = self
            add(asChildViewController: destinationDetailSingleViewController!)
        }else if tripType == .MD{
            destMultiTableViewController?.bottomViewController = self
            add(asChildViewController: destMultiTableViewController!)
        }else if tripType == .MP { destinationDetailSingleViewController?.bottomViewController = self
            add(asChildViewController: destinationDetailSingleViewController!)
        }
    }
    
    func switchdrawer(currentPanel: Panels) {
        pulleyViewController?.setDrawerPosition(position: .partiallyRevealed, animated: true)
        switch currentPanel {
        case .searchpickpoint:
              switchToPickUpDetails()
            break
        case .pickpointdetailsingle:
            senderDetailsViewController?.bottomViewController = self
            add(asChildViewController: senderDetailsViewController!)
            break
        case .senderDetails:
            if UIConstants.activeDestinations.count > 0 {
                switchDestDetails()
            }else{
                destinationPtViewController?.bottomViewController = self
                add(asChildViewController: destinationPtViewController!)
            }
            break
        case .searchdestinationpoint:
           switchDestDetails()
            break
        case .destinationpointdetailssingle:
            checkoutTableViewController.resetTripOnBackPressed = false
            present(checkoutTableViewController, animated: true, completion: nil)
            break
        case .viaMap:
            if currentPointType == .PICKUP {
                selectViaMapMode(source: currentPointType, isActive: false)
                switchToPickUpDetails()
            }else if currentPointType == .DESTINATION {
                // to do later
            }
            break
            
        case .pickpointdetailmultiple:
            senderDetailsViewController?.bottomViewController = self
            add(asChildViewController: senderDetailsViewController!)
            break
    
        case .destinationpointdetailsmultiple:
            present(checkoutTableViewController, animated: true, completion: nil)
            break
        }
    }
    
    func onbackPressed(currentPanel: Panels, additionalData: [String:Any]? = nil){
        pulleyViewController?.setDrawerPosition(position: .partiallyRevealed, animated: true)
        switch currentPanel {
            case .searchpickpoint:
                 if getTripType() == .SPSD{ pickupDetailsSingleViewController?.bottomViewController = self
                     add(asChildViewController: pickupDetailsSingleViewController!, animatefromLeft: false)
                 }else{ pickupMultiTableViewController?.bottomViewController = self
                     add(asChildViewController: pickupMultiTableViewController!, animatefromLeft: false)
                 }
                break
            case .pickpointdetailsingle:
                pickupPtViewController?.bottomViewController = self
                add(asChildViewController: pickupPtViewController!, animatefromLeft: false)
                break
            case .senderDetails:
                if getTripType() == .SPSD || getTripType() == .MD{ pickupDetailsSingleViewController?.bottomViewController = self
                    add(asChildViewController: pickupDetailsSingleViewController!, animatefromLeft: false)
                }else{
                    pickupMultiTableViewController?.bottomViewController = self
                    add(asChildViewController: pickupMultiTableViewController!, animatefromLeft: false)
                }
                break
            case .searchdestinationpoint:
                if UIConstants.activeTrip.isPickFromVendor {
                   mainViewController?.refreshSession()
                   return
               }
               senderDetailsViewController?.bottomViewController = self
               add(asChildViewController: senderDetailsViewController!, animatefromLeft: false)
                break
            case .destinationpointdetailssingle:
                if let data = additionalData, let type = data["type"] as? String{
                    if type == "ADD_DESTINATION" {
                        // add
                         if getTripType() == .MP{
                            senderDetailsViewController?.bottomViewController = self
                            add(asChildViewController: senderDetailsViewController!, animatefromLeft: false)
                        }else{
                          destinationPtViewController?.bottomViewController = self
                          add(asChildViewController: destinationPtViewController!, animatefromLeft: false)
                        }
                    }else{
                        senderDetailsViewController?.bottomViewController = self
                       add(asChildViewController: senderDetailsViewController!, animatefromLeft: false)
                    }
                }
                

                break
            case .viaMap:
                break
                
            case .pickpointdetailmultiple:
                pickupPtViewController?.bottomViewController = self
                add(asChildViewController: pickupPtViewController!, animatefromLeft: false)
            break
        
            case .destinationpointdetailsmultiple:
                if let data = additionalData, let type = data["type"] as? String {
                    if type == "ADD_DESTINATION" {
                        if getTripType() == .MP{
                            senderDetailsViewController?.bottomViewController = self
                           add(asChildViewController: senderDetailsViewController!, animatefromLeft: false)
                        }else{
                            destinationPtViewController?.bottomViewController = self
                            add(asChildViewController: destinationPtViewController!, animatefromLeft: false)
                        }
                        
                    }else{
                       senderDetailsViewController?.bottomViewController = self
                        add(asChildViewController: senderDetailsViewController!, animatefromLeft: false)
                    }
                }
                
                break
            }
    }
    
}


