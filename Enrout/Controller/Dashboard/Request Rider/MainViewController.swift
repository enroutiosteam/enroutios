//
//  MainViewController.swift
//  Enrout
//
//  Created by Daniel Kwakye on 28/06/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit
import GoogleMaps
import Pulley
import ViewAnimator
import SwiftyJSON
import CoreLocation
import SideMenu

class MainViewController: UIViewController {
    
    @IBOutlet weak var mMapView: GMSMapView!
    @IBOutlet weak var hamburgerButton: UIButton!

    @IBOutlet weak var dragIcon: UIImageView!
    
    @IBOutlet weak var expandedView: UIView!
    @IBOutlet weak var collapsedView: UIView!
    var inSelectViaMapMode: Bool = false
    var locationDragged = false
    var bottomViewController: BottomViewController?
    var containerVC: ContainerViewController?
    
    @IBOutlet weak var alertCardView: CardView!
    @IBOutlet weak var alertImageView: CircularImageView!
    @IBOutlet weak var alertLabel: UILabel!
    @IBOutlet weak var alertCloseBtn: UIButton!
    
    
    var polyline = GMSPolyline()
    var animationPolyline = GMSPolyline()
    var path = GMSPath()
    var animationPath = GMSMutablePath()
    var i: UInt = 0
    var timer: Timer!
    
    let manager = CLLocationManager()
    
    var mapIsReady = false
    
    private lazy var rateRiderViewController: RateRiderViewController = {
              // Load Storyboard
              let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
              
              // Instantiate View Controller
              var viewController = storyboard.instantiateViewController(withIdentifier: "RateRiderViewController") as! RateRiderViewController
              
              return viewController
          }()
       
       private lazy var payForTripViewController: PayForTripViewController = {
          // Load Storyboard
          let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
          
          // Instantiate View Controller
          var viewController = storyboard.instantiateViewController(withIdentifier: "PayForTripViewController") as! PayForTripViewController
          
          return viewController
      }()
       
    
    override func viewDidLoad() {
        super.viewDidLoad()
        makeNavBarTransparent()
        self.mMapView.delegate = self
        
        mapIsReady = false

        collapseMap(animation: false)
        hideDragginIcon()
//        // Do any additional setup after loading the view.(animation: false)
//
//        // Create a GMSCameraPosition that tells the map to display the
        // coordinate -33.86,151.20 at zoom level 6.
        let camera = GMSCameraPosition.camera(withLatitude: 5.5912045, longitude: -0.2497703, zoom: 15.0)

        self.mMapView.settings.zoomGestures = true
        self.mMapView.settings.compassButton = true
        self.mMapView.settings.rotateGestures = true
        self.mMapView.isMyLocationEnabled = true
        self.mMapView.settings.myLocationButton = true
        self.mMapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 30, right: 0)

        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
        marker.title = "Ghana"
        marker.snippet = "Accra"
        marker.map = mMapView
        
        mMapView.camera = camera

        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestAlwaysAuthorization()
        manager.requestLocation()
        
        alertCardView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(alertViewTapped)))
        alertCloseBtn.addTarget(self, action: #selector(alertCloseBtnTapped), for: .touchUpInside)
        
        alertCardView.isHidden = true
        
        UIView.animate(withDuration: 1000, delay: 1000, options: .curveLinear, animations: {
            self.alertCardView.isHidden = true
        }) { (status) in
            self.alertCardView.isHidden = false
        }
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
         hearbeatAnimation()
    }
    
    func mapViewSnapshotReady(_ mapView: GMSMapView) {
        if mapIsReady {
            return
        }
        if UIConstants.activeTrip.isPickFromVendor {
            alertCardView.backgroundColor = .black
            alertLabel.text = "Enter delivery details in the panel below"
            alertLabel.textColor = .white
            alertImageView.isHidden = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                self.alertCardView.isHidden = true
            }
            reRenderIconsOnMap()
        }
        
        mapIsReady = true
    }
    
    
    func hearbeatAnimation(){
        let pulse1 = CASpringAnimation(keyPath: "transform.scale")
        pulse1.duration = 0.6
        pulse1.fromValue = 1.0
        pulse1.toValue = 1.12
        pulse1.autoreverses = true
        pulse1.repeatCount = 1
        pulse1.initialVelocity = 0.5
        pulse1.damping = 0.8

        let animationGroup = CAAnimationGroup()
        animationGroup.duration = 2.7
        animationGroup.repeatCount = 1000
        animationGroup.animations = [pulse1]
        
        alertImageView.layer.add(animationGroup, forKey: "pulse")
    }
    
    
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//               let location = locations[0]
//               
//    }


    @IBAction func alertViewTapped(){
        print("alert item tapped")
        performSegue(withIdentifier: "showVendorsSegue", sender: self)
    }
    
    @IBAction func alertCloseBtnTapped(){
        alertCardView.isHidden = true
    }
    
    func expandMap(animation: Bool) {
        collapsedView.isHidden = true
        expandedView.isHidden = false
        mMapView.removeFromSuperview()
        expandedView.addSubview(mMapView)
        mMapView.frame = expandedView.bounds
        mMapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
    }

    
    func collapseMap(animation: Bool){
        collapsedView.isHidden = false
        expandedView.isHidden = true
        mMapView.removeFromSuperview()
        collapsedView.addSubview(mMapView)
        mMapView.frame = collapsedView.bounds
        mMapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    func selectViaMapMode(source: PointType, isActive: Bool) {
        print("main view in select via map mode")
        if isActive {
            inSelectViaMapMode = true
            // show icon for dragging
            showDraggingIcon()
            
        }else{
            inSelectViaMapMode = false
            // hide icon for dragging
            hideDragginIcon()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if UIConstants.activePickUps.count < 1 {
            mMapView.clear()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier! == "openSideMenu"{
           let navVC = segue.destination as! SideMenuNavigationController
            let sideMenuVC = navVC.viewControllers[0] as! DrawerTableViewController
            sideMenuVC.mainViewVC = self
            sideMenuVC.bottomViewVC = bottomViewController
            navVC.sideMenuDelegate = self
        }
    }
    
    private func showDraggingIcon(){
        dragIcon.isHidden = false
        let fromAnimation = AnimationType.zoom(scale: 0.5)
        let zoomAnimation = AnimationType.from(direction: .top, offset: 30.0)
        dragIcon.animate(animations: [fromAnimation, zoomAnimation])
    }
    
    private func hideDragginIcon(){
        dragIcon.isHidden = true
    }
    
    func refreshSession(){
        UIConstants.activePickUps = []
        UIConstants.activeDestinations = []
        UIConstants.activeTrip = Trip()
        UIConstants.activeTrip.isPickFromVendor = false
        UIConstants.activeTrip.payForMe = 0.0
        mMapView.clear()
        clearRoutes()
        bottomViewController?.refreshPanels()
        if self.timer != nil {
            self.timer.invalidate()
        }
    }
    
    func reRenderIconsOnMap(){

//        single pick up // single destination
       
            mMapView.clear()
            clearRoutes()
        if !UIConstants.activeTrip.isPickFromVendor {
            alertCardView.isHidden = true
        }
        
       var locationArray:[PlaceComplete] = []
       for p in UIConstants.activePickUps {
           let lat = (p.pos?.geopoint!.latitude)!
           let lng = (p.pos?.geopoint!.longitude)!
           let placeComplete = PlaceComplete()
           placeComplete.latitude = lat
           placeComplete.longitude = lng
           locationArray.append(placeComplete)
       }
       for d in UIConstants.activeDestinations {
           let lat = (d.pos?.geopoint!.latitude)!
           let lng = (d.pos?.geopoint!.longitude)!
           let placeComplete = PlaceComplete()
           placeComplete.latitude = lat
           placeComplete.longitude = lng
           locationArray.append(placeComplete)
       }
       fitMarkersToMap(map: self.mMapView, locationArray: locationArray)

    }
    
    func fitMarkersToMap(map: GMSMapView, locationArray: [PlaceComplete]){
        var bounds = GMSCoordinateBounds()
        let tripType = getTripType()
 
        for (index, _) in locationArray.enumerated()
        {
            switch tripType {
            case .SPSD:
                if (UIConstants.activePickUps.count == 1){
                    bounds = bounds.includingCoordinate(markerForSinglePickup().position)
                }
                if (UIConstants.activeDestinations.count == 1){
                   bounds = bounds.includingCoordinate(markerForSingleDest().position)
                }
                break
            case .MP:
                if index < UIConstants.activePickUps.count{
                    bounds = bounds.includingCoordinate(markerForMultPickup(index: index).position)
                }
                 if (UIConstants.activeDestinations.count == 1){
                    bounds = bounds.includingCoordinate(markerForSingleDest().position)
                }
                break
            case .MD:
                if (UIConstants.activePickUps.count == 1){
                    bounds = bounds.includingCoordinate(markerForSinglePickup().position)
                }
                if index < UIConstants.activeDestinations.count {
                    bounds = bounds.includingCoordinate(markerForMultDest(index: index).position)
                }
                break
            }
            
           
        }

        map.setMinZoom(1, maxZoom: 15)//prevent to over zoom on fit and animate if bounds be too small

//        let update = GMSCameraUpdate.fit(bounds, withPadding: 50)
//        map.animate(with: update)
        let camera = map.camera(for: bounds, insets:UIEdgeInsets(top: 50, left: 50, bottom: 50, right: 50))
        if let camera = camera {
             map.camera = camera
        }
       
        map.setMinZoom(1, maxZoom: 20) // allow the user zoom in more than level 15 again
        
        if UIConstants.activeDestinations.count < 1{
            return
        }
        
        getDirectionBtnPoints { (status, message, distance, time, routes) in
            if let routes = routes {
                 self.drawRoute(polylines: routes)
            }
        }
    }
    
    func markerForSinglePickup() -> GMSMarker{
        let pp = UIConstants.activePickUps[0]
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: (pp.pos?.geopoint!.latitude)!, longitude: (pp.pos?.geopoint!.longitude)!)
        marker.title = "PICK UP"
        marker.snippet = pp.place
        let img = UIImage(named: "pick_up_single")
        marker.icon = img
        marker.map = mMapView
        return marker
    }
    
    func markerForSingleDest() -> GMSMarker{
        let dd = UIConstants.activeDestinations[0]
          let marker = GMSMarker()
          marker.position = CLLocationCoordinate2D(latitude: (dd.pos?.geopoint!.latitude)!, longitude: (dd.pos?.geopoint!.longitude)!)
          marker.title = "DESTINATION"
          marker.snippet = dd.place
        let img = UIImage(named: "delivery_icon")
//           let img = UIImage(named: "delivery_icon")?.resizeImage(targetSize: CGSize(width: 60, height: 60))
           marker.icon = img
          marker.map = mMapView
        return marker
    }
    
    func markerForMultPickup(index: Int) -> GMSMarker{
        let pp = UIConstants.activePickUps[index]
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: (pp.pos?.geopoint!.latitude)!, longitude: (pp.pos?.geopoint!.longitude)!)
        marker.title = "PICK UP"
        marker.snippet = pp.place
        let img = UIImage(named: "pick_up_\(index + 1)")
        marker.icon = img
        marker.map = mMapView
        return marker
    }
    func markerForMultDest(index: Int) -> GMSMarker {
        let dd = UIConstants.activeDestinations[index]
          let marker = GMSMarker()
          marker.position = CLLocationCoordinate2D(latitude: (dd.pos?.geopoint!.latitude)!, longitude: (dd.pos?.geopoint!.longitude)!)
          marker.title = "DESTINATION \(String(describing: index + 1))"
          marker.snippet = dd.place
           let img = UIImage(named: "delivery_\(index + 1)")
           marker.icon = img
          marker.map = mMapView
        return marker
    }
    
    func clearRoutes (){
        drawRoute(polylines: ["points":""], clear: true)
    }
  
    func drawRoute(polylines: [String: Any], clear: Bool = false) {

            let points = polylines["points"]
        self.path = GMSPath.init(fromEncodedPath: points as! String)!

            self.polyline.path = path
        self.polyline.strokeColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
            self.polyline.strokeWidth = 3.0
        self.polyline.map = clear ? nil : self.mMapView

        self.timer = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(animatePolylinePath), userInfo: nil, repeats: true)

    }
    var hadAnimaterFirstLine = false
    @IBAction func animatePolylinePath() {
        if (self.i < self.path.count()) {
            self.animationPath.add(self.path.coordinate(at: self.i))
            self.animationPolyline.path = self.animationPath
            self.animationPolyline.strokeColor = UIColor.black
            self.animationPolyline.strokeWidth = 3
            self.animationPolyline.map = self.mMapView
            self.i += 1
            self.hadAnimaterFirstLine = true
        }
        else {
            if self.hadAnimaterFirstLine {
                self.polyline.strokeColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
            }
            self.i = 0
            self.animationPath = GMSMutablePath()
            self.animationPolyline.map = nil
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if self.timer != nil {
            self.timer.invalidate()
        }
        
        //
    }

}

extension MainViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        locationDragged = true
        self.bottomViewController?.setMapMode(mapMode: .beganDraggin, cordinate: mapView.camera.target)
//        mapView.camera.targ
//        bottomViewController
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        if locationDragged {
            locationDragged = false
            self.bottomViewController?.setMapMode(mapMode: .settled, cordinate: mapView.camera.target)
        }
    }
}

extension MainViewController: SideMenuNavigationControllerDelegate {
    func sideMenuDidAppear(menu: SideMenuNavigationController, animated: Bool) {
        self.hamburgerButton.setImage(UIImage(named: "close-cross"), for: .normal)
    }
    
    func sideMenuDidDisappear(menu: SideMenuNavigationController, animated: Bool) {
        self.hamburgerButton.setImage(UIImage(named: "menu_icon"), for: .normal)
         
    }
}



extension MainViewController: PulleyPrimaryContentControllerDelegate{
    func drawerPositionDidChange(drawer: PulleyViewController, bottomSafeArea: CGFloat) {
        print("Is view loaded => \(String(describing: isViewLoaded))")
        if isViewLoaded {
            if drawer.drawerPosition == PulleyPosition.collapsed {
                expandMap(animation: true)
            }

            if drawer.drawerPosition == PulleyPosition.partiallyRevealed {
                 collapseMap(animation: true)
            }
        }
    }
}

extension MainViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if locations.first != nil {
            print("got location")
              guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
                 print("locations = \(locValue.latitude) \(locValue.longitude)")
           //marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
            UIConstants.lastKnowLocation.latitude = locValue.latitude
            UIConstants.lastKnowLocation.longitude = locValue.longitude
            
               let camera = GMSCameraPosition.camera(withLatitude: locValue.latitude, longitude: locValue.longitude, zoom: 15.0)
           self.mMapView.animate(to: camera)
        }
       }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
           Toast.error(view: self.view, message: "\(error.localizedDescription)")
       }
}


