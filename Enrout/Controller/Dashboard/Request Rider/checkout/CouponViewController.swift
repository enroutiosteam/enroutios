//
//  CouponViewController.swift
//  Enrout
//
//  Created by Daniel Kwakye on 10/01/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import UIKit

class CouponViewController: UIViewController {
    
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var codeTF: UITextField!
    
    @IBOutlet weak var applyBtn: UIButton!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var source: CheckoutTableViewController?
    
    var showIndicator: Bool = false{
        willSet {
            activityIndicator.isHidden = !newValue
            if newValue {
                activityIndicator.startAnimating()
            }else{
                activityIndicator.stopAnimating()
            }
            applyBtn.isHidden = newValue
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        showIndicator = false
        
        applyBtn.addTarget(self, action: #selector(applyBtnTapped), for: .touchUpInside)
        closeBtn.addTarget(self, action: #selector(closeBtnTapped), for: .touchUpInside)
        applyBtn.curveCorners()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        codeTF.text = ""
    }
    
    @IBAction func closeBtnTapped(){
          self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func applyBtnTapped(){
        if codeTF.text == nil || codeTF.text! == ""{
            Toast.error(view: self.view, message: "Please ender code")
            return
        }
        showIndicator = true
        checkIfCouponCanBeApplied(viewController: self, code: codeTF.text!) { (error, coupon) in
            self.showIndicator = false
            
            if error != nil {
                Toast.error(view: self.view, message: error!.localizedDescription)
                return
            }
            
            self.source?.setCoupon(c: coupon!)
            
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
