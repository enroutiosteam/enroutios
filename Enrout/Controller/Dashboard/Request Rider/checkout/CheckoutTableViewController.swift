//
//  CheckoutTableViewController.swift
//  Enrout
//
//  Created by Daniel Expresspay on 19/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit
import LinearProgressBarMaterial
import Pulley
import MaterialComponents.MaterialBottomSheet
import Presentr
import CoreLocation
import M13Checkbox

class CheckoutTableViewController: UITableViewController {

    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var estimatedTime: UILabel!
    @IBOutlet weak var pfmAdd: UIButton!
    @IBOutlet weak var pfmMinue: UIButton!
    @IBOutlet weak var pfmTextfield: UITextField!
    @IBOutlet weak var cashTitle: UIButton!
    @IBOutlet weak var walletSuggestionAmount: UILabel!
    @IBOutlet weak var tripCostLabel: UILabel!
    @IBOutlet weak var paymentOptionMenu: UIButton!
    @IBOutlet weak var applyPromoBtn: UIButton!
    @IBOutlet weak var dispatchBtn: UIButton!
    
    @IBOutlet weak var surgeMessageLbl: UILabel!
    
    @IBOutlet weak var useBoxCheckbox: M13Checkbox!
    
    
    
    
    var isPriceEstimated = false
    var isSurgeCharge = false
    var isTimeEstimated = false
    
    var pickups: [Pickup]!
    var destinations: [Destination]!
    var trip: Trip!
    
    let manager = CLLocationManager()
    var canDipatchRequest = false
    
    var resetTripOnBackPressed = false
    
    @IBOutlet weak var payOptionIcon: UIImageView!
    
    
    @IBOutlet var ppIcons: [UIImageView]!
    @IBOutlet var ppTitles: [UILabel]!
    @IBOutlet var ppDescriptions: [UILabel]!
    
    @IBOutlet var ddIcons: [UIImageView]!
    @IBOutlet var ddTitles: [UILabel]!
    @IBOutlet var ddDecriptions: [UILabel]!
    
    var dialog: AnyProgressDialog?
    
    var isInsufficientBalance = false
    
    
    @IBOutlet weak var prossiveLinearView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTargets()
        
        addSenderDetails()
        
        useBoxCheckbox.boxType = .square
        
        //Simply, Call Progress Bar
        let linearBar: LinearProgressBar = LinearProgressBar()
        linearBar.backgroundProgressBarColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        linearBar.progressBarColor = #colorLiteral(red: 0, green: 0.2640928626, blue: 0.2655927539, alpha: 1)
        linearBar.startAnimation()
        prossiveLinearView.addSubview(linearBar)
        linearBar.frame = prossiveLinearView.bounds
        
        estimatedTime.isHidden = true
        dispatchBtn.curveCorners()
        dispatchBtn.isEnabled = false
        applyPromoBtn.isEnabled = false
        
        
        UIConstants.activeTrip.customerType = "IOS"
        dialog = AnyProgressDialog.newInstance(view: self.view)
        
        Constants.TEMP_COUPON = nil
  
    }
    
    
    func presentPaymentOption(){
        // View controller the bottom sheet will hold
        paymentOptionViewController.source = self
        // Initialize the bottom sheet with the view controller just created
        let bottomSheet: MDCBottomSheetController = MDCBottomSheetController(contentViewController: paymentOptionViewController)
        // Present the bottom sheet
        present(bottomSheet, animated: true, completion: nil)
    }
    
    @IBAction func useBoxChanged(_ sender: M13Checkbox) {
        
        if sender.checkState == .checked {
            UIConstants.activeTrip.useBox = true
        }else if sender.checkState == .unchecked {
             UIConstants.activeTrip.useBox = false
        }
    }
    
    
    
    func setPaymentOptions(p: PaymentOption){
        UIConstants.activeTrip.paymentMethod = p.name
        payOptionIcon.image = p.image
        cashTitle.setTitle(p.name, for: .normal)
    }
    
    func setCoupon(c: Coupon){
        Constants.TEMP_COUPON = c
        displaySlashedPriceWithCoupon()
    }
    
    func displaySlashedPriceWithCoupon(){
        if let c = Constants.TEMP_COUPON {
            
            let newCostToDisplay = UIConstants.activeTrip.estimatedCost! - (UIConstants.activeTrip.estimatedCost! * c.value!);
            
            tripCostLabel.text = String(newCostToDisplay)
        }
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        trip = UIConstants.activeTrip
        destinations = UIConstants.activeDestinations
        pickups = UIConstants.activePickUps
        
        if UIConstants.activeTrip.isPickFromVendor {
             pfmTextfield.text = String(describing: UIConstants.activeTrip.payForMe!)
             pfmTextfield.isEnabled = false
             pfmAdd.isEnabled = false
             pfmMinue.isEnabled = false
        
         }else{
             pfmTextfield.text = "\(0.0)"
             pfmTextfield.isEnabled = true
             pfmAdd.isEnabled = true
             pfmMinue.isEnabled = true
             
         }
        
        let tripType = getTripType()
        if tripType == .SPSD{
            setSinglePickUpValues()
            setSingleDestinationValues()
        }else if tripType == .MD {
            setSinglePickUpValues()
            for (index,destination) in destinations.enumerated() {
                setMultiDestinationValues(index: index, destination: destination)
            }
            
        }else if tripType == .MP{
            for (index,pickup) in pickups.enumerated() {
                setMultiPickupValues(index: index, pickup: pickup)
            }
            if UIConstants.activeDestinations.count == 1{
                setSingleDestinationValues()
            }
        }
        
        UIConstants.activeTrip.couponApplied = false
        UIConstants.activeTrip.couponName = nil
        UIConstants.activeTrip.couponValueInPercentage = 0.0
        UIConstants.activeTrip.paymentMethod = "CASH"
        UIConstants.activeTrip.useBox = false
        
        UIConstants.activeTrip.riderId = nil
        UIConstants.activeTrip.riderName = nil
        UIConstants.activeTrip.riderImage = nil
        UIConstants.activeTrip.riderPhoneNumber = nil
        UIConstants.activeTrip.riderPlateNumber = nil
        UIConstants.activeTrip.isAccepted = nil
        UIConstants.activeTrip.isArrived = nil
        UIConstants.activeTrip.coupon = nil
        UIConstants.activeTrip.couponPreviousPrice = nil
        
        payOptionIcon.image = UIImage(named: "cash")
        
        tableView.reloadData()
        
        estimateCostOfTrip()
        estimateDistanceTime()
        
        if UIConstants.activeTrip.couponApplied == true {
            displaySlashedPriceWithCoupon()
        }
        
        Constants.TEMP_COUPON = nil
    }
    
    func showProgress(){
        prossiveLinearView.isHidden = false
    }
    func hideProgress(){
        prossiveLinearView.isHidden = true
    }
    
    func enableDispatchBtn(){
        applyPromoBtn.isEnabled = true
        dispatchBtn.isEnabled = true
    }
    
    func disableDispatchBtn(){
        applyPromoBtn.isEnabled = false
        dispatchBtn.isEnabled = false
    }
    
    func estimateDistanceTime(){
        estimatedTime.isHidden = true
        getDirectionBtnPoints { (status, errorMessage, distance, time, routes) in
            if !status {
                // show dialog for with connection error
                return
            }
            
            self.estimatedTime.text = "\(String(describing: time!.text!))"
            UIConstants.activeTrip.estimatedTimeInText = time!.text!
            UIConstants.activeTrip.estimatedDistanceInText = distance!.text!
            
            UIConstants.activeTrip.estimatedTimeInSeconds = Int(time!.value!)
            UIConstants.activeTrip.estimatedDistanceInMeters = distance!.value!
            
            self.estimatedTime.isHidden = false
//            self.tableView.reloadData()
           
        }
    }
    
    func estimateCostOfTrip(){
        
        isPriceEstimated = false
        showProgress()
        tableView.reloadData()
        disableDispatchBtn()
        
        estimateCost(viewController: self) { (message, fare, isSurgeCharge, status) in
            self.hideProgress()
               if !status{
                self.isInsufficientBalance = false
                   // present a prompt saying no connection, click to retry again
                   presentSweetAlert(presentingController: self, message: "There was a problem with connection, please try again", showCancelBtn: true, delegate: self)
                   return
               }
            
            self.isPriceEstimated = true
            self.tripCostLabel.text = "GHS \(String(describing: fare!))"
            UIConstants.activeTrip.estimatedCost = fare
            //let walletFee  = fare! - (fare! * 0.01);
            
            self.walletSuggestionAmount.text = "Loading ..."
            // show wallet balance
            Wallet.getInstance(view: self.view, viewController: self).getWallet { (err) in
                
                self.walletSuggestionAmount.text =  Constants.applicationUser?.currentBalance != nil ? "GHS \(String(describing: Constants.applicationUser!.currentBalance))" : "0.00"
                
            }
            
            if let isSurgeCharge = isSurgeCharge {
                if isSurgeCharge {
                     self.isSurgeCharge = isSurgeCharge
                     self.surgeMessageLbl.text = message
                }
            }
            self.tableView.reloadData()
            self.enableDispatchBtn()
           }
    }
    
    
    
    private func addSenderDetails(){
        // change to user sending the request
        UIConstants.activeTrip.senderName = Constants.applicationUser?.fullName
        UIConstants.activeTrip.senderPhoneNumber = Constants.applicationUser?.phoneNumber
    }
    
    private func setMultiPickupValues(index: Int, pickup: Pickup){
        let countingNumber = index + 1
        
       let ppIcon = ppIcons[index]
       let ppTitle = ppTitles[index]
       let ppDescription = ppDescriptions[index]
       
       ppIcon.image = UIImage(named: "pickup_raw_\(String(describing: countingNumber))")
       ppTitle.text = pickup.place
        
        var desc = ""
        if pickup.pickUpName != nil {
            desc = "\(String(describing: pickup.pickUpName!))"
        }
        if pickup.pickUpPersonPhone != nil {
            desc = "\(desc) \(String(describing: pickup.pickUpPersonPhone!))"
        }
        
        ppDescription.text = desc
//       ppDescription.text = "\(String(describing: pickup.pickUpName!)) , \(String(describing: pickup.pickUpPersonPhone!))"
    }
    
    private func setSinglePickUpValues(){
        let ppIcon = ppIcons[0]
        let ppTitle = ppTitles[0]
        let ppDescription = ppDescriptions[0]
        
        let pickup = pickups[0]
        
        ppIcon.image = UIImage(named: "pick_up_icon")
        ppTitle.text = pickup.place
        ppDescription.text = "\(String(describing: pickup.pickUpName!)) , \(String(describing: pickup.pickUpPersonPhone!))"
    }
    
    private func setSingleDestinationValues(){
        let ddIcon = ddIcons[0]
        let ddTitle = ddTitles[0]
        let ddDescription = ddDecriptions[0]
        
        let destination = destinations[0]
        
        ddIcon.image = UIImage(named: "delivery_raw")
        ddTitle.text = destination.place
        ddDescription.text = "\(String(describing: destination.recipientName!)) , \(String(describing: destination.recipientNumber!))"
    }
    
    private func setMultiDestinationValues(index: Int, destination: Destination){
        let countingNumber = index + 1
        
        let ddIcon = ddIcons[index]
        let ddTitle = ddTitles[index]
        let ddDescription = ddDecriptions[index]
        
        ddIcon.image = UIImage(named: "deliver_raw_\(String(describing: countingNumber))")
        ddTitle.text = destination.place
        
        var desc = ""
        if destination.recipientName != nil {
            desc = "\(String(describing: destination.recipientName!))"
        }
        if destination.recipientNumber != nil {
            desc = "\(desc) \(String(describing: destination.recipientNumber!))"
        }
        
        ddDescription.text = desc
    }
    
    private func setupTargets(){
        closeBtn.addTarget(self, action: #selector(self.closeBtnTapped(_:)), for: .touchUpInside)
        dispatchBtn.addTarget(self, action: #selector(self.dispatchBtnTapped(_:)), for: .touchUpInside)
        pfmAdd.addTarget(self, action: #selector(reducePFM), for: .touchUpInside)
        pfmMinue.addTarget(self, action: #selector(increasePFM), for: .touchUpInside)
        paymentOptionMenu.addTarget(self, action: #selector(paymentOptionMenuTapped), for: .touchUpInside)
        applyPromoBtn.addTarget(self, action: #selector(applyPromoTapped), for: .touchUpInside)
    }
    
    @IBAction func paymentOptionMenuTapped(){
        presentPaymentOption()
    }
    
    @IBAction func applyPromoTapped(){
        presentCouponDialog()
    }
    
    @IBAction func increasePFM(){
        var pfmValue = Double(pfmTextfield.text!)!
        pfmValue = pfmValue + 1
        pfmTextfield.text = String(pfmValue)
    }
    
    @IBAction func reducePFM(){
        var pfmValue = Double(pfmTextfield.text!)!
        pfmValue = pfmValue - 1
        if pfmValue < 1 {
            pfmValue = 0
        }
        pfmTextfield.text = String(pfmValue)
    }
    
    @IBAction func closeBtnTapped(_ sender: Any){
        if resetTripOnBackPressed {
            UIConstants.activeTrip = Trip()
            UIConstants.activePickUps = [Pickup]()
            UIConstants.activeDestinations = [Destination]()
        }
             
        self.dismiss(animated: true, completion: nil)
    }
    
    private lazy var trackMainViewController: TrackMapViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewcontroller = storyboard.instantiateViewController(identifier: "TrackMapViewController") as! TrackMapViewController
        return viewcontroller
    }()
    
    private lazy var paymentOptionViewController: PaymentOptionViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewcontroller = storyboard.instantiateViewController(identifier: "PaymentOptionViewController") as! PaymentOptionViewController
        return viewcontroller
    }()
    
    private lazy var couponViewController: CouponViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewcontroller = storyboard.instantiateViewController(identifier: "CouponViewController") as! CouponViewController
        return viewcontroller
    }()
    
    private lazy var trackBottomViewController: TrackBottomViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewcontroller = storyboard.instantiateViewController(identifier: "TrackBottomViewController") as! TrackBottomViewController
        return viewcontroller
    }()
    
    private lazy var paymentViewController: PaymentViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewcontroller = storyboard.instantiateViewController(identifier: "PaymentViewController") as! PaymentViewController
        return viewcontroller
    }()
    
    func presentCouponDialog(){
        couponViewController.source = self
        self.customPresentViewController(getCustomPresenter(enterAnimation: .coverHorizontalFromLeft), viewController: couponViewController, animated: true)
    }

    
    @IBAction func dispatchBtnTapped(_ sender: Any?){
        
        if !isPriceEstimated {
            return
        }
        canDipatchRequest = true
        
        if UIConstants.lastKnowLocation.latitude != nil && UIConstants.lastKnowLocation.longitude != nil {
            afterGettingUserLocation(lat: UIConstants.lastKnowLocation.latitude!, lng: UIConstants.lastKnowLocation.longitude!)
            return
        }

        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestAlwaysAuthorization()
        manager.requestLocation()
        
        dialog?.setText(text: "confirming location")
        dialog?.show()
        
        // get user current location first, then dispatch request

    }
    
    private func dispatchRequestNow(userCurLatLng: PosLatLng){
        if !canDipatchRequest { return }
        dispatchRequest(viewController: self, userCurrLatLng: userCurLatLng) { (status, message) in
            if !status {
                Toast.error(view: self.view, message: message!)
                print("v2 search message => \(message!)")
                return
            }
            let drawerContentVC = self.trackBottomViewController
            
            openTrackTripVC(presenter: self, drawerContentVC: drawerContentVC, trackMainViewController: self.trackMainViewController, trip: UIConstants.activeTrip)
            UIConstants.activeTrip = Trip()
            UIConstants.activePickUps = [Pickup]()
            UIConstants.activeDestinations = [Destination]()
            self.canDipatchRequest = false
            
        }
    }
    
    func afterGettingUserLocation(lat: Double, lng: Double){
        
        let userCurlatLng = PosLatLng(latitude: lat, longitude: lng)
        UIConstants.activeTrip.payForMe = Double(pfmTextfield.text!)
                              // check if user has enough balance in wallet for pfm
              if UIConstants.activeTrip.payForMe != nil && UIConstants.activeTrip.payForMe! > 0 {
                  
                  let dialog = AnyProgressDialog.newInstance(view: self.view).show()
                  Wallet.getInstance(view: self.view, viewController: self).getWallet { (error) in
                      dialog.dismis()
                      if let err = error {
                          Toast.error(view: self.view, message: err.localizedDescription)
                          return
                      }
                      
                      if UIConstants.activeTrip.payForMe! > Constants.applicationUser!.currentBalance {
                        self.isInsufficientBalance = true
                         presentSweetAlert(presentingController: self, message: "Kindly pay for item before dispatching request", showCancelBtn: true, delegate: self)
                          
                          return
                          
                      }
                      
                      self.trip.isPayForMe = true
                      self.dispatchRequestNow(userCurLatLng: userCurlatLng)

                  }
              }else{
                  dispatchRequestNow(userCurLatLng: userCurlatLng)
              }
    }
    
   

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 7
    }
    

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1{
            return UIConstants.activePickUps.count // make dynamic
        }else if section == 2 {
            return UIConstants.activeDestinations.count // make dynamic
        }
        else if section == 4{
            if isPriceEstimated {
                // set estimated price
                if isSurgeCharge {
                    return 2
                }else{
                    return 1
                }
            }else{
                return 0
            }
        }
        else if section == 5 {
            // apply coupon section
            return 2
        }
        else{
            return 1 // leave as static
        }
    
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        if indexPath.section == 4 {
            presentPaymentOption()
        }else if indexPath.section == 5 {
            if indexPath.row == 0{
                presentCouponDialog()
            }else if indexPath.row == 1 {
                if useBoxCheckbox.checkState == .checked {
                    useBoxCheckbox.setCheckState(.unchecked, animated: true)
                    UIConstants.activeTrip.useBox = false
                }else if useBoxCheckbox.checkState == .unchecked {
                     useBoxCheckbox.setCheckState(.checked, animated: true)
                     UIConstants.activeTrip.useBox = true
                }
            }
            
        }else if indexPath.section == 6 {
            dispatchBtnTapped(nil)
        }
    }

}


extension CheckoutTableViewController: SweetAlertDelegate{
    func onConfirm() {
        if isInsufficientBalance {
            var fixedAmount: Double = Double(self.pfmTextfield.text!)!
            fixedAmount = fixedAmount + 1.0
            paymentViewController.fixedAmount =  fixedAmount
            self.present(paymentViewController, animated: true, completion: nil)
            return
        }
        estimateCostOfTrip()
    }
    
    func onCancel() {
        self.dismiss(animated: true, completion: nil)
    }
}

    extension CheckoutTableViewController: CLLocationManagerDelegate {
        
        func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
            dialog?.setText(text: "Please wait")
            dialog?.dismis()
            
            if locations.first != nil {
                print("got location use current location")
                guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
                    print("locations = \(locValue.latitude) \(locValue.longitude)")
                
                UIConstants.lastKnowLocation.latitude = locValue.latitude
                UIConstants.lastKnowLocation.longitude = locValue.longitude
                    
                afterGettingUserLocation(lat: locValue.latitude, lng: locValue.longitude)
                    
                    
            }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        dialog?.setText(text: "Please wait")
        dialog?.dismis()
        Toast.error(view: self.view, message: "Unable to get your location")
    }
}
