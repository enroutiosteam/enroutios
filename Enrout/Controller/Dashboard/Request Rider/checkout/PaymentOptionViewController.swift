//
//  PaymentOptionViewController.swift
//  Enrout
//
//  Created by Daniel Kwakye on 10/01/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import UIKit

class PaymentOptionViewController: UIViewController {
    
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var pageTitle: UILabel!
    
    @IBOutlet weak var cashBtn: UIButton!
    @IBOutlet weak var cardBtn: UIButton!
    @IBOutlet weak var momoBtn: UIButton!
    @IBOutlet weak var walletBtn: UIButton!
    
    var source: CheckoutTableViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        cashBtn.tag = 0
        cardBtn.tag = 1
        momoBtn.tag = 2
        walletBtn.tag = 3
        
        cashBtn.addTarget(self, action: #selector(paymentOptionTapped(_:)), for: .touchUpInside)
        cardBtn.addTarget(self, action: #selector(paymentOptionTapped(_:)), for: .touchUpInside)
        momoBtn.addTarget(self, action: #selector(paymentOptionTapped(_:)), for: .touchUpInside)
        walletBtn.addTarget(self, action: #selector(paymentOptionTapped(_:)), for: .touchUpInside)
        
        closeBtn.addTarget(self, action: #selector(onCloseBtnTapped), for: .touchUpInside)
        
    }
    
    @IBAction func paymentOptionTapped(_ sender: UIButton){
        let tag = sender.tag
        var p = PaymentOption()
        if tag == 0 {
            p.id = 0
            p.name = "CASH"
            p.image = UIImage(named: "cash")
        }else if tag == 1 {
            p.id = 1
            p.name = "CARD"
            p.image = UIImage(named: "card")
        }
        else if tag == 2 {
            p.id = 2
            p.name = "MOMO"
            p.image = UIImage(named: "momo")
        }
        else if tag == 3 {
            p.id = 3
            p.name = "WALLET"
            p.image = UIImage(named: "wallet_blue")
        }
        source?.setPaymentOptions(p: p)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onCloseBtnTapped(){
        dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
