//
//  PayForTripViewController.swift
//  Enrout
//
//  Created by Daniel Kwakye on 20/01/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import UIKit

class PayForTripViewController: UIViewController, SweetAlertDelegate {

    @IBOutlet weak var deliveryLocation: UILabel!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var pageTitle: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var momoBtn: UIButton!
    @IBOutlet weak var cardBtn: UIButton!
    
    var containerVC: ContainerViewController?
    var swtAlt: SweetAlertViewController?
    
    var trip: Trip!
    var limitExceeded: Bool = false
    
    private lazy var paymentViewController: PaymentViewController = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
        
        return viewController
    }()
    
    private lazy var paymentSuccessfulViewController: PaymentSuccessfulViewController = {
           let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
           let viewController = storyboard.instantiateViewController(identifier: "PaymentSuccessfulViewController") as! PaymentSuccessfulViewController
           return viewController
       }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        closeBtn.addTarget(self, action: #selector(closeBtnTapped), for: .touchUpInside)
      
        amount.text = "GHS \(trip.actualCost!)"
        deliveryLocation.text = shortenPickupsDestinations(trip: trip)
        
        momoBtn.addTarget(self, action: #selector(momoBtnTapped), for: .touchUpInside)
        cardBtn.addTarget(self, action: #selector(cardBtnTapped), for: .touchUpInside)
    }
    
    override func viewDidAppear(_ animated: Bool) {
          activityIndicator.isHidden = true
    }
    
    @IBAction func closeBtnTapped(){
        if limitExceeded {
           swtAlt = presentSweetAlert(presentingController: self, message: "You have to make payment before requesting another trip")
         return
        }
        self.containerVC?.switchToMainView()
    }
    
    @IBAction func momoBtnTapped(){
        navigateToPaymentPage()
    }
    
    @IBAction func cardBtnTapped(){
        // pay with wallet not card
        Wallet.getInstance(view: self.view, viewController: self).payForTripWithWallet(trip: trip) { (error, insufficientFunds) in
            
            if insufficientFunds {
//                self.swtAlt = presentSweetAlert(presentingController: self, message: error!.localizedDescription, showCancelBtn: true, delegate: self)
                Toast.error(view: self.view, message: error!.localizedDescription)
                //self.swtAlt?.delegate = self
                return
            }
            
            if let error = error {
                presentSweetAlert(presentingController: self, message: error.localizedDescription)
                //self.swtAlt?.delegate = self
                return
            }
            
            self.paymentSuccessfulViewController.delegate = self
            self.paymentSuccessfulViewController.shouldGoToMainView = true
            self.present(self.paymentSuccessfulViewController, animated: true, completion: nil)
            
        }
        //navigateToPaymentPage()
    }
    
    func onConfirm() {
        swtAlt?.dismiss(animated: true, completion: nil)
        print("on confirm called again => self = \(self)")
        self.performSegue(withIdentifier: "addFundsSegue", sender: nil)
    }
    
//    override func onConfirm() {
//        // take him to wallet to addFunds
//        DispatchQueue.main.async(){
//           print("on confirm called => self = \(self)")
//            self.performSegue(withIdentifier: "addFundsSegue", sender: self)
//        }
//
//    }
    
    private func navigateToPaymentPage(){
        paymentViewController.trip = trip
        paymentViewController.paymentType = .payForTrip
        paymentViewController.fixedAmount = trip.actualCost
        present(paymentViewController, animated: true, completion: nil)
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "addFundsSegue" {
//            let paymentVC = segue.destination as! PaymentViewController
//            paymentVC.delegate = self
//        }
//    }

}

extension PayForTripViewController: PaymentSuccessDelegate {
    func onDoneTapped() {
        
          performSegue(withIdentifier: "goToMainSegue", sender: nil)
    }
}

//extension PayForTripViewController: SweetAlertDelegate {
//
//}
