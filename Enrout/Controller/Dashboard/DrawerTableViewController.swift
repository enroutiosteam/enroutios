//
//  DrawerTableViewController.swift
//  Enrout
//
//  Created by Jude Botchwey on 02/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit
import Presentr

class DrawerTableViewController: UITableViewController {

    var mainViewVC: MainViewController?
    var bottomViewVC: BottomViewController?
    
    @IBOutlet weak var helloName: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        makeNavBarTransparent()
        tableView.delegate = self
        tableView.dataSource = self
        
        helloName.text = "Hello \(Constants.applicationUser!.fullName!)"
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 7
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 70
        }
        
        return CGFloat()
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            
            if indexPath.row == 0 {
                 UIConstants.activeTrip = Trip()
                 UIConstants.activePickUps = [Pickup]()
                 UIConstants.activeDestinations = [Destination]()
                mainViewVC?.refreshSession()
                dismiss(animated: true, completion: nil)
                performSegue(withIdentifier: "refreshSegue", sender: nil) 
            }else
            if indexPath.row == 4 {
                //faq
                openFaq(viewcontroller: self)
            }else if indexPath.row == 5 {
                //contact
                contactUs(viewcontroller: self)
            }else if indexPath.row == 6 {
                //logout
                displayAlertLogout()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("drawer prepare for segue called")
    }
    
    

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}



extension DrawerTableViewController {
    
    func displayAlertLogout(){
        let presenter = Presentr(presentationType: PresentationType.custom(width: ModalSize.full, height: ModalSize.half, center: ModalCenterPosition.center))
        presenter.transitionType = .coverHorizontalFromRight
        presenter.roundCorners = true
        presenter.cornerRadius = 10
                
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(withIdentifier: "LogoutAlertViewController") as! LogoutAlertViewController
        self.customPresentViewController(presenter, viewController: viewController, animated: true, completion: nil)
    }
    
}
