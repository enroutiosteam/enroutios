//
//  DeliveriesMastersViewController.swift
//  Enrout
//
//  Created by Daniel Expresspay on 13/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit

class DeliveriesMastersViewController: UIViewController {

    var currentVC: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    private lazy var ongoingTripsViewController : OnGoingTableViewController = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)

        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "OnGoingTripsViewController") as! OnGoingTableViewController

        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)

        return viewController
    }()
    
    private var shimmerVC = ShimmerViewController.shimmerViewController
    
    private lazy var cancelledTripsViewController : CancelledTableViewController = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)

        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "CancelledTripsViewController") as! CancelledTableViewController

        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)

        return viewController
    }()
    
    private lazy var completedTripsViewController : CompletedTableViewController = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)

        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "CompletedTripsViewController") as! CompletedTableViewController

        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)

        return viewController
    }()
    
    private lazy var noTripsFoundViewController : NoTripsFoundViewController = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)

        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "NoTripsFoundViewController") as! NoTripsFoundViewController

        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)

        return viewController
    }()
    
    
     private func add(asChildViewController viewController: UIViewController) {
                
            remove()
        // Add Child View Controller
                addChild(viewController)

               // Add Child View as Subview
               view.addSubview(viewController.view)

               // Configure Child View
               viewController.view.frame = view.bounds
               viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]

               // Notify Child View Controller
                viewController.didMove(toParent: self)
            currentVC = viewController
        }
        
        private func remove() {
            // Notify Child View Controller
            currentVC?.willMove(toParent: nil)

            // Remove Child View From Superview
            currentVC?.view.removeFromSuperview()

            // Notify Child View Controller
            currentVC?.removeFromParent()
        }
    
        func switchSegment(segmentedIndex: Int){
                if segmentedIndex == 0 {
                    fetchAndupdateOngoing(type: .ONGOING, onlyToday: true)
                }else if segmentedIndex == 1 {
                    fetchAndupdateOngoing(type: .CANCELLED)
                } else if segmentedIndex == 2 {
                    fetchAndupdateOngoing(type: .DELIVERED)
            }
        }
    
    func fetchAndupdateOngoing(type: TripHistoryTypes, onlyToday: Bool = false){
        add(asChildViewController: shimmerVC)
        getTrips(by: type, onlyToday: onlyToday) { (trips, error) in
               if let err = error{
                print("err => \(err.localizedDescription)")
                self.add(asChildViewController: self.noTripsFoundViewController)
                   return
               }
            
            
            if trips!.count < 1 {
                self.add(asChildViewController: self.noTripsFoundViewController)
                return
            }
            
            switch type {
            case .ONGOING:
                print("No of ongoing trips => \(trips!.count) ")
                self.ongoingTripsViewController.trips = trips
                self.add(asChildViewController: self.ongoingTripsViewController)
                break
            case .CANCELLED:
                print("No of cancelled trips => \(trips!.count) ")
                self.cancelledTripsViewController.trips = trips
                self.add(asChildViewController: self.cancelledTripsViewController)
                break
            case .DELIVERED:
                print("No of delivered trips => \(trips!.count) ")
                self.completedTripsViewController.trips = trips
                self.add(asChildViewController: self.completedTripsViewController)
                break
            }
               
        }
    }

}
