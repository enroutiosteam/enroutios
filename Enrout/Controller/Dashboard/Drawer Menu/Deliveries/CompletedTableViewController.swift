//
//  CompletedTableViewController.swift
//  Enrout
//
//  Created by Daniel Expresspay on 13/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit
import MGSwipeTableCell

class CompletedTableViewController: UITableViewController {

    var trips: [Trip]? = [Trip]()
       let drawerContentVC = TrackBottomViewController()
       
    private lazy var checkoutTableViewController: CheckoutTableViewController = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "CheckoutTableViewController") as! CheckoutTableViewController
        
        return viewController
    }()
    
    private lazy var receiptViewController: ReceiptViewController = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "ReceiptViewController") as! ReceiptViewController
        
        return viewController
    }()
       
       override func viewDidLoad() {
           super.viewDidLoad()
           
       }

       // MARK: - Table view data source

       override func numberOfSections(in tableView: UITableView) -> Int {
           return 1
       }

       override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           
           if section == 0{
               return trips!.count
           }else {
               return 0
           }
       }


       override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! DeliveriesTableViewCell
           
           let trip = trips![indexPath.row]
        
           let delivery = Delivery(leftIcon: "completed", title: shortenPickupsDestinations(trip: trip), subTitle: String(describing: trip.requestDatetime!), rightIcon: "receipt", rightText: "Receipt")
           
           cell.update(with: delivery)
        
        cell.delegate = self

           return cell
       }
       
       private lazy var trackMainViewController: TrackMapViewController = {
           let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
           let viewcontroller = storyboard.instantiateViewController(identifier: "TrackMapViewController") as! TrackMapViewController
           return viewcontroller
       }()

       
       override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           tableView.deselectRow(at: indexPath, animated: true)
           
        receiptViewController.trip = trips?[indexPath.row]
        self.present(receiptViewController, animated: true, completion: nil)
            
       }

}

extension CompletedTableViewController: MGSwipeTableCellDelegate{
    
    func swipeTableCell(_ cell: MGSwipeTableCell, swipeButtonsFor direction: MGSwipeDirection, swipeSettings: MGSwipeSettings, expansionSettings: MGSwipeExpansionSettings) -> [UIView]? {
        
        if direction == .leftToRight {
            return nil
        }
        
        swipeSettings.transition = MGSwipeTransition.border;
        expansionSettings.buttonIndex = 0;
        
        expansionSettings.fillOnTrigger = true;
        expansionSettings.threshold = 1.1;
        let padding = 15;
        let color1 = #colorLiteral(red: 0, green: 0.8364669681, blue: 0.8598158956, alpha: 1)
        
        let trash = MGSwipeButton(title: "Request Again", icon: UIImage(systemName: "checkmark.cirle")?.withTintColor(UIColor.white), backgroundColor: color1, padding: padding, callback: {
            cell in

            let indexPath = (self.tableView.indexPath(for: cell)?.row)!
            print("row swiped at \(indexPath)")
            
            
            let loader = AnyProgressDialog.newInstance(view: self.view).show()
            let trip = self.trips![indexPath]
            requestTripAgain(tripId: trip.requestId!) { (returnedTrip, pickups, destinations, error) in
                loader.dismis()
                
                if let err = error {
                    Toast.error(view: self.view, message: err.localizedDescription)
                    
                    return
                }
                
                UIConstants.activePickUps = pickups!
                UIConstants.activeDestinations = destinations!
                UIConstants.activeTrip = returnedTrip!
                
                self.present(self.checkoutTableViewController, animated: true, completion: nil)
                
            }
            
            return false
        });
        
        
        return [trash]
        
    }
    
}
