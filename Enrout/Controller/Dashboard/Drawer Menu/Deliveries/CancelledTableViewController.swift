//
//  CancelledTableViewController.swift
//  Enrout
//
//  Created by Daniel Expresspay on 13/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit

class CancelledTableViewController: UITableViewController {

    var trips: [Trip]? = [Trip]()
       let drawerContentVC = TrackBottomViewController()
    
    private lazy var checkoutTableViewController: CheckoutTableViewController = {
           // Load Storyboard
           let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
           
           // Instantiate View Controller
           var viewController = storyboard.instantiateViewController(withIdentifier: "CheckoutTableViewController") as! CheckoutTableViewController
           
           return viewController
       }()
       
       
       override func viewDidLoad() {
           super.viewDidLoad()
        
       }

       // MARK: - Table view data source

       override func numberOfSections(in tableView: UITableView) -> Int {
           return 1
       }

       override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           
        return trips!.count
          
       }


       override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! DeliveriesTableViewCell
           
           let trip = trips![indexPath.row]
           
            let delivery = Delivery(leftIcon: "cancelled", title: shortenPickupsDestinations(trip: trip), subTitle: String(describing: trip.requestDatetime!), rightIcon: "redo", rightText: "Retry")
            
            cell.update(with: delivery)
        
           return cell
       }
       
       private lazy var trackMainViewController: TrackMapViewController = {
           let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
           let viewcontroller = storyboard.instantiateViewController(identifier: "TrackMapViewController") as! TrackMapViewController
           return viewcontroller
       }()

       
       override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           tableView.deselectRow(at: indexPath, animated: true)
        
        let trip = trips![indexPath.row]
           
        let dialog = UIAlertController(title: "Request Trip", message: "Do you want to request for trip again?", preferredStyle: .actionSheet)
        let doneBtn = UIAlertAction(title: "Yes", style: .default) { (action) in
            let loader = AnyProgressDialog.newInstance(view: self.view).show()
            requestTripAgain(tripId: trip.requestId!) { (returnedTrip, pickups, destinations, error) in
                loader.dismis()
                
                if let err = error {
                    Toast.error(view: self.view, message: err.localizedDescription)
                    
                    return
                }
                
                UIConstants.activePickUps = pickups!
                UIConstants.activeDestinations = destinations!
                UIConstants.activeTrip = returnedTrip!
                
                self.present(self.checkoutTableViewController, animated: true, completion: nil)
                
            }
        }
        let cancelBtn = UIAlertAction(title: "No", style: .cancel) { (action) in
            dialog.dismiss(animated: true, completion: nil)
        }
        dialog.addAction(doneBtn)
        dialog.addAction(cancelBtn)
        
        present(dialog, animated: true, completion: nil)
       }

}
