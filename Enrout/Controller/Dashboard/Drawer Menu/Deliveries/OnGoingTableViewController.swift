//
//  OnGoingTableViewController.swift
//  Enrout
//
//  Created by Daniel Expresspay on 13/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit
import Pulley

class OnGoingTableViewController: UITableViewController {

    var trips: [Trip]? = [Trip]()
    private lazy var drawerContentVC: TrackBottomViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewcontroller = storyboard.instantiateViewController(identifier: "TrackBottomViewController") as! TrackBottomViewController
        return viewcontroller
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return trips!.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! DeliveriesTableViewCell
        
        let trip = trips![indexPath.row]
        
        let delivery = Delivery(leftIcon: "ongoing", title: shortenPickupsDestinations(trip: trip), subTitle: String(describing: trip.requestDatetime!), rightIcon: "icons8-location-off-100", rightText: "Track")
        
        cell.update(with: delivery)

        return cell
    }
    
    private lazy var trackMainViewController: TrackMapViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewcontroller = storyboard.instantiateViewController(identifier: "TrackMapViewController") as! TrackMapViewController
        return viewcontroller
    }()

    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let trip = trips?[indexPath.row]
        openTrackTripVC(presenter: self, drawerContentVC: drawerContentVC, trackMainViewController: trackMainViewController, trip: trip!)
       
    }
    

}
