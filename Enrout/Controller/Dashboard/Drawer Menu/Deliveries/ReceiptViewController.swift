//
//  ReceiptViewController.swift
//  Enrout
//
//  Created by Daniel Kwakye on 20/01/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import UIKit

class ReceiptViewController: UIViewController {

    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var riderName: UILabel!
    @IBOutlet weak var dateTime: UILabel!
    @IBOutlet weak var timeCovered: UILabel!
    
    @IBOutlet weak var distanceCovered: UILabel!
    @IBOutlet weak var orderNo: UILabel!
    
    @IBOutlet weak var paymentMethod: UILabel!
    
    @IBOutlet weak var amountPaid: UIButton!
    
    @IBOutlet weak var doneBtn: UIButton!
    
    @IBOutlet weak var sendReceiptBtn: UIButton!
    
    var trip: Trip?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        closeBtn.addTarget(self, action: #selector(closeBtnTapped), for: .touchUpInside)
        
        riderName.text = trip?.riderName
        dateTime.text = String(describing: trip!.requestDatetime!)
        timeCovered.text = trip!.estimatedTimeInText ?? ""
        distanceCovered.text = trip!.estimatedDistanceInText ?? ""
        orderNo.text = "Order No - \(String(describing: trip!.orderNumber!))"
        paymentMethod.text = trip?.paymentMethod!
        amountPaid.setTitle("GHS \(String(describing: trip!.actualCost!))", for: .normal)
        amountPaid.curveCorners()
        sendReceiptBtn.curveCorners()
        sendReceiptBtn.addTarget(self, action: #selector(sendReceipt), for: .touchUpInside)
        
        
    }
    
    @IBAction func closeBtnTapped(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sendReceipt(){
        var text = "Enrout Delivery Services\n"
        text = text + "Order No - \(trip!.orderNumber!)"
        text = text + "Date - \(String(describing: trip!.requestDatetime!))"
        text = text + "Rider - \(trip!.riderName!)"
        text = text + "Plate - \(trip!.riderPlateNumber!)"
        text = text + "PICK UP (S)\n"
        text = text + "---------------- \n"
        for item in trip!.pickupNames! {
            text = text + item
        }
        
        text = text + "\n\n"
        text = text + "DESTINATIONS (S)\n"
        text = text + "----------------\n"
        
        for item in trip!.destinationNames! {
            text = text + item
        }
        
        text  = text + "Thank your for using Enrout\n"
        text = text + "Contact us on : 020 111 2225\n"
        
        let textShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textShare , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "deliveryLocationsSegue"{
           let destVc = segue.destination as! ReceiptDeliveryLocationsVC
            destVc.pickupNames = trip!.pickupNames!
            destVc.destinationNames = trip!.destinationNames!
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
