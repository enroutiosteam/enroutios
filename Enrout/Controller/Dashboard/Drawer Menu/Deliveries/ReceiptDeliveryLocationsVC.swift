//
//  ReceiptDeliveryLocationsVC.swift
//  Enrout
//
//  Created by Daniel Kwakye on 20/01/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import UIKit

class ReceiptDeliveryLocationsVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var pickupNames: [String] = []
    var destinationNames: [String] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        closeBtn.addTarget(self, action: #selector(closeBtnTapped), for: .touchUpInside)
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        pickupNames.count + destinationNames.count
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           
         let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! ReceiptLocationsTableViewCell
        
        if indexPath.row < (pickupNames.count) {
            let index = indexPath.row
            let pickup = pickupNames[index]
            cell.update(place: pickup, point: .PICKUP)
        }else{
            let index = indexPath.row - (pickupNames.count)
            let destination = destinationNames[index]
            cell.update(place: destination, point: .DESTINATION)
        }
        
         
         return cell
       }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    @IBAction func closeBtnTapped(){
        self.dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
