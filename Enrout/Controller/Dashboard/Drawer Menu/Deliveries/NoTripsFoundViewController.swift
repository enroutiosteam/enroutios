//
//  NoTripsFoundViewController.swift
//  Enrout
//
//  Created by Daniel Kwakye on 06/01/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import UIKit

class NoTripsFoundViewController: UIViewController {

    @IBOutlet weak var message: UILabel!
    
    @IBOutlet weak var requestNowBtn: UIButton!
    
    @IBOutlet weak var requestTripBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        requestNowBtn.curveCorners()
        // Do any additional setup after loading the view.
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
