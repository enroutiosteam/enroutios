//
//  WalletViewController.swift
//  Enrout
//
//  Created by Daniel Expresspay on 12/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit

class WalletViewController: UIViewController {
    
    @IBOutlet weak var addFundsBtn: UIButton!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var viewTransactionsBtn: UIButton!
    
    private lazy var paymentTransViewController: PaymentTransViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(identifier: "PaymentTransViewController") as! PaymentTransViewController
        return viewController
    }()
    
    private lazy var paymentViewController: PaymentViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(identifier: "PaymentViewController") as! PaymentViewController
        return viewController
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        amountLbl.text = "GHS \(0.0)"
        addFundsBtn.addTarget(
            self, action: #selector(addFundsTapped), for: .touchUpInside)
        viewTransactionsBtn.addTarget(self, action: #selector(viewTransactionsTapped), for: .touchUpInside)
        addFundsBtn.curveCorners()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        loadingIndicator.isHidden = false
        loadingIndicator.startAnimating()
        amountLbl.isHidden = true
        
        Wallet.getInstance(view: self.view, viewController: self).getWallet { (error) in
            self.loadingIndicator.isHidden = true
            self.amountLbl.isHidden = false
           // Constants.applicationUser
            self.amountLbl.text = "GHS \(Constants.applicationUser!.currentBalance)"
        }
    }
    
    @IBAction func addFundsTapped(){
        paymentViewController.delegate = self
        present(paymentViewController, animated: true, completion: nil)
    }
    
    @IBAction func viewTransactionsTapped(){
        present(paymentTransViewController, animated: true, completion: nil)
    }
    

    @IBAction func closeButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }


}

extension WalletViewController: PaymentCompleteDelegate {
    func onPayentComplete(error: Error?) {
        print("on payment complete called")
//        if paymentViewController.isViewLoaded && (paymentViewController.view!.window != nil) {
//            paymentViewController.dismiss(animated: true, completion: nil)
//        }
        
    }
}
