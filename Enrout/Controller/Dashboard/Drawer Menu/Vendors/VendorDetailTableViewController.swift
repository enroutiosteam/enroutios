//
//  VendorDetailTableViewController.swift
//  Enrout
//
//  Created by Daniel Kwakye on 05/02/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import UIKit

class VendorDetailTableViewController: UITableViewController {

    var vendor: Vendor?

    @IBOutlet weak var vendorDescription: UILabel!
    @IBOutlet weak var vendorBanner: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        makeNavBarTransparent()
        if let vendor = vendor {
            title = vendor.vendorName
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0 {
            return 1
        }else{
            return vendor?.products?.count ?? 2 // 2 is the number of shimmer view to show
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell?
        
        if indexPath.section == 0 {
           let headerCell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell", for: indexPath) as! VendorDetailTableViewCell
            
            if let vendor = vendor {
                headerCell.hideAnimation()
                headerCell.updateUI(vendor: vendor)
            }
            cell = headerCell
        }else{
            let productCell = tableView.dequeueReusableCell(withIdentifier: "ProductCell", for: indexPath) as! ProductsTableViewCell

            if let vendor = vendor, let products = vendor.products {
                let product = products[indexPath.row]
                productCell.hideAnimation()
               
                productCell.productImage.tag = indexPath.row
            productCell.productImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.productImageTapped(sender:))))
                
                productCell.updateUI(product: product)
            }
            cell = productCell
        }

        return cell!
    }
    
    @IBAction func productImageTapped(sender: UITapGestureRecognizer){
        let imageview = sender.view as! UIImageView
        let index = imageview.tag
        let product = vendor?.products?[index]
        if let image = imageview.image {
            presentProductImageEnlargened(presentingController: self, image: image, title: product!.productTitle ?? product?.productDescription ?? "", price: product!.productPrice!)
        }
    }
    
    var selectedProduct = -1
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 1 && vendor != nil {
            
            //1. Create the alert controller.
            let alert = UIAlertController(title: "Quantity", message: "How many do you want?", preferredStyle: .alert)

            //2. Add the text field. You can configure it however you need.
            alert.addTextField { (textField) in
                textField.text = "1"
                textField.keyboardType = .numberPad
            }

            // 3. Grab the value from the text field, and print it when the user clicks OK.
            alert.addAction(UIAlertAction(title: "Done", style: .default, handler: { [weak alert] (_) in
                let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
                //print("Text field: \(textField.text)")
                if Double(textField!.text!) == nil {
                    Toast.error(view: self.view, message: "Please enter a valid quantity")
                    return
                }
                let product = self.vendor!.products![indexPath.row]
                self.selectedProduct = indexPath.row
                UIConstants.activeTrip.payForMe = product.productPrice! * Double(textField!.text!)!
                self.performSegue(withIdentifier: "goToMain", sender: self)
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

            // 4. Present the alert.
            self.present(alert, animated: true, completion: nil)
            
          
        }
        
         tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToMain" {
            if let vendor = vendor {
                let product = vendor.products![self.selectedProduct]
                
               let pickup = Pickup()
                pickup.instruction = "Please buy me \(UIConstants.activeTrip.payForMe! / product.productPrice!) of \(product.productName!) worth GHS \(String(describing: UIConstants.activeTrip.payForMe!))"
               pickup.isArrived = false
               pickup.isArrivedNotified = false
               pickup.isPickedup = false
               pickup.isPickedupNotified = false
               pickup.itemCategory = "OTHER"
               pickup.pickedupAt = nil
               pickup.pickUpName = vendor.vendorName!
               pickup.pickUpPersonPhone = vendor.vendorPhoneNumber
                pickup.place = vendor.vendorLocation?.address

               var ppPos = Pos()
                ppPos.geohash = Constants.getGeoHash(latitude: vendor.vendorLocation!.latitude!, longitude: vendor.vendorLocation!.longitude!)
                ppPos.geopoint = vendor.vendorLocation
               
               pickup.pos = ppPos
                UIConstants.activeTrip.isPickFromVendor = true
                
                UIConstants.activePickUps = []
                UIConstants.activePickUps.append(pickup)
                
                selectedProduct = -1
            }
        }
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
