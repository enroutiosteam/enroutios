//
//  ProductModalViewController.swift
//  Enrout
//
//  Created by Daniel Kwakye on 10/02/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import UIKit

class ProductModalViewController: UIViewController {

    
    
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var price: UILabel!
    
    var image: UIImage!
    var mTitle: String!
    var formattedPrice: String!
    
    
    public static var productModalViewController: ProductModalViewController = {
          return UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(identifier: "ProductModalViewController") as! ProductModalViewController
      }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        closeBtn.addTarget(self, action: #selector(closeBtnTapped), for: .touchUpInside)
    }
    
    override func viewDidAppear(_ animated: Bool) {
          productImage.image = image
          productTitle.text = mTitle
          price.text = formattedPrice
    }
    
    @IBAction func closeBtnTapped(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func configure(image: UIImage, title: String, price: Double){
        self.image = image
        self.mTitle = title
        self.formattedPrice = "GHS \(String(describing: price))"
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
