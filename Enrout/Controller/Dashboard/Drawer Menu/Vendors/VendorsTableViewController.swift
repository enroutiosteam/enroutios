//
//  VendorsTableViewController.swift
//  Enrout
//
//  Created by Daniel Kwakye on 05/02/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import UIKit

class VendorsTableViewController: UITableViewController {

//    var poorConnectionViewController = PoorConnectionViewController.poorConnectionViewController
    var vendors: [Vendor]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        fetchVendors { (vendors) in
            guard let vendors = vendors else{
                // open no connection page
                
                self.performSegue(withIdentifier: "showPoorConnectionSegue", sender: self)
                return
            }
            
            self.vendors = vendors
            self.tableView.reloadData()
        }
    }
    
    
    @IBAction func itemBackBtnTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return vendors?.count ?? 5
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! VendorsTableViewCell

        if let vendors =  vendors {
            let vendor = vendors[indexPath.row]
            cell.hideAnimation()
            cell.updateUI(vendor: vendor)
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if vendors != nil {
            performSegue(withIdentifier: "VendorDetail", sender: nil)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "VendorDetail" {
           let destination = segue.destination as! VendorDetailTableViewController
            if let vendors = vendors {
                destination.vendor = vendors[tableView.indexPathForSelectedRow!.row]
            }
        }
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
