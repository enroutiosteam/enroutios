//
//  RecentPayTransTableViewController.swift
//  Enrout
//
//  Created by Daniel Kwakye on 15/01/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import UIKit

class RecentPayTransTableViewController: UITableViewController {

    var transactions: [WalletTransaction]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return transactions!.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PayTransTableViewCell

        // Configure the cell...
        let trans = transactions![indexPath.row]
        cell.updateValues(amount: "GHS \(String(trans.amount!))", date: String(describing: trans.createdAt!), details: "\(trans.narration!) made by \(trans.accountNumber!)")

        return cell
    }
    

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

}
