//
//  PaymentTransViewController.swift
//  Enrout
//
//  Created by Daniel Kwakye on 15/01/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import UIKit

class PaymentTransViewController: UIViewController {
    
    @IBOutlet weak var backBtn: UIButton!
    
    @IBOutlet weak var containerView: UIView!
    
    var currentViewController: UIViewController?
    
    private var shimmerViewController: ShimmerViewController = ShimmerViewController.shimmerViewController
    
    private lazy var recentPayTransTableViewController: RecentPayTransTableViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(identifier: "RecentPayTransTableViewController") as! RecentPayTransTableViewController
        return viewController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        backBtn.addTarget(self, action: #selector(closeBtnTapped), for: .touchUpInside)
        
        swapCurrentViewController(withChildViewController: shimmerViewController)
        
        Wallet.getInstance(view: view, viewController: self).getTransactions { (transactions, error) in
            
            if let err = error {
                self.removeCurrentViewController()
                print(err.localizedDescription)
                Toast.error(view: self.view, message: err.localizedDescription)
                return
            }
            
            self.recentPayTransTableViewController.transactions = transactions
            self.swapCurrentViewController(withChildViewController: self.recentPayTransTableViewController)
        }
    }
    
    private func swapCurrentViewController(withChildViewController viewController: UIViewController) {
        
        //remove currentViewCOntroller
        removeCurrentViewController()
        
        containerView.addSubview(viewController.view)
        viewController.view.frame = containerView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParent: self)
        currentViewController = viewController
        
    }
    
    private func removeCurrentViewController(){
        currentViewController?.willMove(toParent: nil)
        // Remove Child View From Superview
        currentViewController?.view.removeFromSuperview()
        // Notify Child View Controller
        currentViewController?.removeFromParent()
    }
    

    @IBAction func closeBtnTapped(){
        self.dismiss(animated: true, completion: nil)
    }

}
