//
//  DeliveriesViewController.swift
//  Enrout
//
//  Created by Daniel Expresspay on 12/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit

class DeliveriesViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    var containerViewController: DeliveriesMastersViewController?

    
    private func updateView() {
        containerViewController?.switchSegment(segmentedIndex:segmentControl.selectedSegmentIndex)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func setupSegmentedControl() {
        // Configure Segmented Control
        self.segmentControl.removeAllSegments()
        self.segmentControl.insertSegment(withTitle: "ON GOING", at: 0, animated: false)
        self.segmentControl.insertSegment(withTitle: "CANCELLED", at: 1, animated: false)
        self.segmentControl.insertSegment(withTitle: "DELIVERED", at: 2, animated: false)
        self.segmentControl.addTarget(self, action: #selector(selectionDidChange(_:)), for: .valueChanged)

        // Select First Segment
        segmentControl.selectedSegmentIndex = 0
    }
    
    @objc func selectionDidChange(_ sender: UISegmentedControl) {
        updateView()
    }
    
    func setupView() {
        setupSegmentedControl()
        updateView()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "containerViewSegue" {
            containerViewController = segue.destination as? DeliveriesMastersViewController
            containerViewController?.switchSegment(segmentedIndex: segmentControl.selectedSegmentIndex)
        }
    }

}
