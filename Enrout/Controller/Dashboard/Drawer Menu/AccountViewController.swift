//
//  AccountViewController.swift
//  Enrout
//
//  Created by Jude Botchwey on 07/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit
import DLRadioButton

class AccountViewController: UIViewController , UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var femaleBtn: DLRadioButton!
    
    @IBOutlet weak var maleBtn: DLRadioButton!
    
    @IBOutlet weak var userNameLabel: UILabel!
    
    
    @IBOutlet weak var fullNameTF: UITextField!
    @IBOutlet weak var phoneNumberTF: UITextField!
    
    @IBOutlet weak var emailTF: UITextField!
    
    
    @IBOutlet weak var profileImageView: CircularImageView!
    
    @IBOutlet weak var updateButton: UIButton!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var numberView: UIView!
    @IBOutlet weak var emailView: UIView!
    var sexValue = ""
    let imagePicker = UIImagePickerController()
    var shouldUploadImage = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        
        setValues()
        
        
        imagePicker.delegate = self
       imagePicker.sourceType = .photoLibrary
        
        profileImageView.isUserInteractionEnabled = true
        profileImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(profileImageTapped)))
       
        
    }
    
    func setValues(){
        fullNameTF.text = Constants.applicationUser?.fullName
        userNameLabel.text = Constants.applicationUser?.fullName
        phoneNumberTF.text = Constants.applicationUser?.phoneNumber
        emailTF.text = Constants.applicationUser?.email
        
        if let gender = Constants.applicationUser?.gender {
            if gender == "Male" {
                sexValue = "M"
                maleBtn.isSelected = true
            }else if gender == "Female"{
                sexValue = "F"
                 femaleBtn.isSelected = true
            }
            
        }
        
        if let photoUrl = Constants.applicationUser?.photoUrl {
            profileImageView.downloaded(from: photoUrl)
        }
        
        shouldUploadImage = false
        
        
    }
    
    @IBAction func profileImageTapped(){
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
           print("image selected")
           
           guard let selectedImage = info[.originalImage] as? UIImage else{
               return
           }
           
           profileImageView.image = selectedImage
           shouldUploadImage = true
          dismiss(animated: true, completion: nil)
           
       }
    
    
    func setupView(){
        nameView.layer.cornerRadius = 10
        updateButton.layer.cornerRadius = 10
        numberView.layer.cornerRadius = 10
        emailView.layer.cornerRadius = 10
    }
    
    
    
    @IBAction func malePressed(_ sender: Any) {
        sexValue = "M"
    }
    
    
    @IBAction func femaleFriend(_ sender: Any) {
        sexValue = "F"
    }
    
    @IBAction func updateTapped(_ sender: Any) {
        
        Constants.applicationUser?.email = emailTF.text
        Constants.applicationUser?.phoneNumber = phoneNumberTF.text
        Constants.applicationUser?.fullName = fullNameTF.text
        if sexValue != "" {
            Constants.applicationUser?.gender = sexValue == "M" ? "Male" : "Female"
        }
        
        if shouldUploadImage {
            let imageData = profileImageView.image?.jpegData(compressionQuality: 0.8)!
            updateUserDetails(viewController: self, user: Constants.applicationUser!, imageData: imageData, completed: {
                status in
                self.setValues()
            })
            return
        }

        updateUserDetails(viewController: self, user: Constants.applicationUser!, imageData: nil, completed: {
            status in
            self.setValues()
        })
        
        
    }
    
    
    

    @IBAction func closeTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

}
