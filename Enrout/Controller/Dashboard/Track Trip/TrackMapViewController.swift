//
//  TrackMapViewController.swift
//  Enrout
//
//  Created by Daniel Expresspay on 20/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit
import GoogleMaps
import Pulley
import SocketIO
import Pulsator


class TrackMapViewController: UIViewController, GMSMapViewDelegate {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var dim: UIView!
    @IBOutlet weak var expandedView: UIView!
    @IBOutlet weak var collapsedView: UIView!
    var mSocket: SockIOConnection?
    var trip: Trip?
    var pulseEffect: LFTPulseAnimation!
    var riderMarker: GMSMarker?
    var destinationMarker: GMSMarker?
    var mTargetLat: Double = 0.0
    var mTargetLong: Double = 0.0
    
    var trackBottomVC: TrackBottomViewController?
    
    
   @IBOutlet weak var mapActivityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var arrivalTime: UILabel!
    
    var polyline = GMSPolyline()
    var animationPolyline = GMSPolyline()
    var path = GMSPath()
    var animationPath = GMSMutablePath()
    var i: UInt = 0
    var timer: Timer!
    
    @IBOutlet weak var pulseRiderIcon: UIImageView!
    
    @IBOutlet weak var viewForSpin: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("view did load")
        //makeNavBarTransparent()
        arrivalTime.isHidden = true
        mapActivityIndicator.startAnimating()
        mapActivityIndicator.isHidden = true
        pulseEffect = LFTPulseAnimation(repeatCount: Float.infinity, radius:200, position: view.center)
        // Do any additional setup after loading the view.
        collapseMap(animation: false)
        self.mapView.delegate = self
        
        mSocket = SockIOConnection.getSocketInstance()
        mSocket?.socket.connect()
        
        setSearchRiderMode(on: false)
        
        
        let camera = GMSCameraPosition.camera(withLatitude: 5.5912045, longitude: -0.2497703, zoom: 15.0)
       mapView.camera = camera
       self.mapView.settings.zoomGestures = true
       self.mapView.settings.compassButton = true
       self.mapView.settings.rotateGestures = true

    }
    
    override func viewWillAppear(_ animated: Bool) {
        mSocket?.socket.emit("request::join::live-locations", ["requestId" : trip?.requestId])

   
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // Creates a marker in the center of the map.
//             closeBtn.addTarget(self, action: #selector(self.closeBtnTapped(_:)), for:  .touchUpInside)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        mSocket?.socket.emit("request::leave::live-locations", ["requestId" : trip?.requestId])
    }
    
    // this method is called from track bottom viewcontroller
    func tripStatusChanged(status: TripStatus){
        print("track map status changed => \(String(describing: status))")
        UserDefaults.standard.set(status.rawValue, forKey: RIDER_TRIP_STATUS)
        switch status {
                       
           case .PENDING:
               setSearchRiderMode(on: true)
                break
           case .CANCELLED:
               setSearchRiderMode(on: false)
               break
           case .CANCELLED_R:
               setSearchRiderMode(on: false)
               break
           case .ONGOING:
                // show rider position on the map
            setSearchRiderMode(on: false)
            trackRider()
               break
           case .COMPLETED:
             setSearchRiderMode(on: false)
               break
           case .NOT_FOUND:
              setSearchRiderMode(on: false)
               break
           case .PROCESSING:
            setSearchRiderMode(on: true)
              break
           case .DELAYED:
              setSearchRiderMode(on: false)
              
               break
               
           }
    }

    var DEFAULT_ZOOM = 15.0
    var zoomValue = 15.0
    var mTimer: Timer?
    
    private func setSearchRiderMode(on: Bool){
        
        
        if on {
            
            if  let pointToBeginSearch = trip?.pointToBeginSearch {
                let camera = GMSCameraPosition.camera(withLatitude: pointToBeginSearch.latitude!, longitude: pointToBeginSearch.longitude!, zoom: 15.0)
                self.mapView.animate(to: camera)
                 
            }
            
            dim.isHidden = false
            pulseRiderIcon.isHidden = false
            viewForSpin.isHidden = false
            pulseEffect.isHidden = false
            addHalo()
            
            self.mTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(zoomoutMap), userInfo: nil, repeats: true)
        
            
        }else{
            dim.isHidden = true
            viewForSpin.isHidden = true
            pulseRiderIcon.isHidden = true
            pulseEffect.isHidden = true
        }
    }
    
    @IBAction private func zoomoutMap() {
 
       let tripStatus = UserDefaults.standard.string(forKey: RIDER_TRIP_STATUS)
       let status = TripStatus(rawValue: tripStatus!)
        if status != TripStatus.PENDING{
           if status != TripStatus.PROCESSING {
                return
            }
        }
    
        zoomValue = zoomValue - 0.1
        if zoomValue <= 10{
            zoomValue = DEFAULT_ZOOM
        }
        if  let pointToBeginSearch = trip?.pointToBeginSearch {
            let camera = GMSCameraPosition.camera(withLatitude: pointToBeginSearch.latitude!, longitude: pointToBeginSearch.longitude!, zoom: Float(zoomValue))
           self.mapView.animate(to: camera)
            
       }
    }
    
    private func trackRider(){
        mapActivityIndicator.isHidden = false
        mapActivityIndicator.startAnimating()
        dim.isHidden = false
        mSocket?.socket.on("request::trip-ongoing::customer", callback: { (data, ark) in
            let obj = data[0] as! [String:Any]
            
            if self.trip!.requestId! != obj["requestId"] as! String {
                return
            }
            
            let confirmStatus = UserDefaults.standard.string(forKey: RIDER_TRIP_STATUS)
            if confirmStatus != "ONGOING" {
                return
            }
            
            let latitude = obj["latitude"] as! Double
            let longitude = obj["longitude"] as! Double
            
            let posLatLng = PosLatLng(latitude: latitude, longitude: longitude)
            //posLatLng
            let encodedData = try? JSONEncoder().encode(posLatLng)
            UserDefaults.standard.set(encodedData, forKey: RIDER_LAST_KNOWN_LOCATION)
            
            let targetLongitude = obj["targetLongitude"] as! Double
            let targetLatitude = obj["targetLatitude"] as! Double
            
            let isHeadingToPickup = obj["tripType"] as! Bool
            let triptimeUpdate = obj["triptimeUpdate"] as? String
            
            let isTapped = obj["isTapped"] as! Bool
            var bounds = GMSCoordinateBounds()
            
            let isFull = obj["isFull"] as? Bool
            let senderId = obj["customerId"] as? String
            
            if self.riderMarker == nil {
                self.riderMarker = self.markerForRider(latlng: posLatLng)
                
                self.riderMarker?.snippet = triptimeUpdate != nil && "calculating..." != triptimeUpdate ? "Arrives in " + triptimeUpdate! : self.trip!.riderPlateNumber
            }
            
            if !isTapped {
                
                self.trackBottomVC?.riderFoundViewController?.showHideCancelBtn(show: true)
                
                self.riderMarker?.snippet = triptimeUpdate != nil && "calculating..." != triptimeUpdate ? "" : self.trip?.riderPlateNumber
            
 
                self.riderMarker!.position = CLLocationCoordinate2D(latitude: posLatLng.latitude!, longitude: posLatLng.longitude!)
                
                bounds = bounds.includingCoordinate(self.riderMarker!.position)
                
                 
                
            }else{
                
                self.trackBottomVC?.riderFoundViewController?.showHideCancelBtn(show: false)
                
                var place = obj["place"] as! String
                if "the location" == place {
                    place = ""
                }
                
                self.riderMarker!.position = CLLocationCoordinate2D(latitude: posLatLng.latitude!, longitude: posLatLng.longitude!)
                
                bounds = bounds.includingCoordinate(self.riderMarker!.position)
              
                if self.mTargetLong != targetLongitude && self.mTargetLat != targetLatitude {
                    
                    
                    self.mTargetLong = targetLongitude
                    self.mTargetLat = targetLatitude
                    
                    let destLatLng = PosLatLng(latitude: self.mTargetLat, longitude: self.mTargetLong)
                    
                    // draw route
                    getRoute(origin: posLatLng, destination: destLatLng) { (status, routes) in
                        if let routes = routes {
                             self.drawRoute(polylines: routes)
                        }
                    }
                    
                    if self.destinationMarker != nil {
                                            // remove destination marker
                        self.destinationMarker?.map = nil
                    }
                    
                    self.destinationMarker = self.markerForTarget(latlng: destLatLng, isHeadingToPickup)
                    
                     bounds = bounds.includingCoordinate(self.destinationMarker!.position)

                }else{
                    
                    if self.destinationMarker == nil{
                        
                        self.destinationMarker = self.markerForTarget(latlng: PosLatLng(latitude: self.mTargetLat, longitude: self.mTargetLong), isHeadingToPickup)
                    }
                    bounds = bounds.includingCoordinate(self.destinationMarker!.position)
                }
                
  
             
            }
            
            self.mapActivityIndicator.isHidden = true
            self.dim.isHidden = true
            
            if triptimeUpdate != nil {
                self.arrivalTime.text = triptimeUpdate
                self.arrivalTime.isHidden = false
            }
            
            self.trackBottomVC?.riderFoundViewController?.trackingRider(isFull: isFull, tripSenderId: senderId)
            
            self.mapView.setMinZoom(1, maxZoom: 15)//prevent to over zoom on fit and animate if bounds be too small

            //        let update = GMSCameraUpdate.fit(bounds, withPadding: 50)
            //        map.animate(with: update)
            let camera = self.mapView.camera(for: bounds, insets:UIEdgeInsets(top: 100, left: 100, bottom: 100, right: 100))
//            self.mapView.animate(to: camera!)
                self.mapView.camera = camera!

            self.mapView.setMinZoom(1, maxZoom: 20) // allow the user zoom in more than level 15 again
                        
                             
            
        })
    }
    
    func drawRoute(polylines: [String: Any]) {

            let points = polylines["points"]
            self.path = GMSPath.init(fromEncodedPath: points as! String)!

            self.polyline.path = path
            self.polyline.strokeColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
            self.polyline.strokeWidth = 3.0
            self.polyline.map = self.mapView

            self.timer = Timer.scheduledTimer(timeInterval: 0.010, target: self, selector: #selector(animatePolylinePath), userInfo: nil, repeats: false)

    }
    
    @IBAction func animatePolylinePath() {
        if (self.i < self.path.count()) {
            self.animationPath.add(self.path.coordinate(at: self.i))
            self.animationPolyline.path = self.animationPath
            self.animationPolyline.strokeColor = UIColor.black
            self.animationPolyline.strokeWidth = 3
            self.animationPolyline.map = self.mapView
            self.i += 1
        }
        else {
            self.i = 0
            self.animationPath = GMSMutablePath()
            self.animationPolyline.map = nil
        }
    }
    
    func markerForRider(latlng: PosLatLng) -> GMSMarker {
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: latlng.latitude!, longitude: latlng.longitude!)
        
            marker.title = self.trip?.riderName
        let img = UIImage(named: "rider_icon")?.resizeImage(targetSize: CGSize(width: 40, height: 40))
        marker.icon = img
        marker.map = self.mapView
        return marker
    }
    
    func markerForTarget(latlng: PosLatLng, _ isHeadingToPickup: Bool) -> GMSMarker {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: latlng.latitude!, longitude: latlng.longitude!)
        
            marker.title = "Destination"
        let img = UIImage(named: isHeadingToPickup ? "pick_up_single" : "delivery_raw")?.resizeImage(targetSize: CGSize(width: 40, height: 40))
        marker.icon = img
        marker.map = self.mapView
        return marker
    }
    
    
    func expandMap(animation: Bool) {
        collapsedView.isHidden = true
        expandedView.isHidden = false
        mapView.removeFromSuperview()
        expandedView.addSubview(mapView)
        mapView.frame = expandedView.bounds
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
    }

    
    func collapseMap(animation: Bool){
        collapsedView.isHidden = false
        expandedView.isHidden = true
        mapView.removeFromSuperview()
        collapsedView.addSubview(mapView)
        mapView.frame = collapsedView.bounds
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    private func addHalo() {
        pulseEffect.backgroundColor = #colorLiteral(red: 0, green: 0.8364669681, blue: 0.8598158956, alpha: 1)
        view.layer.insertSublayer(pulseEffect, below: pulseRiderIcon.layer)
    }
    

}

extension TrackMapViewController: PulleyPrimaryContentControllerDelegate{
    func drawerPositionDidChange(drawer: PulleyViewController, bottomSafeArea: CGFloat) {
        print("Is view loaded => \(String(describing: isViewLoaded))")
        if isViewLoaded {
            if drawer.drawerPosition == PulleyPosition.collapsed {
                 expandMap(animation: true)
            }

            if drawer.drawerPosition == PulleyPosition.partiallyRevealed {
                collapseMap(animation: true)
            }
        }
    }
}
