//
//  ChatViewController.swift
//  Enrout
//
//  Created by Daniel Kwakye on 07/01/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import UIKit
import MessengerKit
import Firebase


class ChatViewController: MSGMessengerViewController {
    
        var trip: Trip?

        let steve = CUser(displayName: "Steve", avatar: #imageLiteral(resourceName: "pick_up_single"), avatarUrl: nil, isSender: true)

        let tim = CUser(displayName: "Tim", avatar: #imageLiteral(resourceName: "delivery_3"), avatarUrl: nil, isSender: false)
        
        var id = 100
        
        override var style: MSGMessengerStyle {
            var style = MessengerKit.Styles.iMessage
            style.headerHeight = 0
            
            //        style.inputPlaceholder = "Message"
            //        style.alwaysDisplayTails = true
            style.outgoingBubbleColor = #colorLiteral(red: 0, green: 0.8364669681, blue: 0.8598158956, alpha: 1)
            //        style.outgoingTextColor = .black
            //        style.incomingBubbleColor = .green
            //        style.incomingTextColor = .yellow
            style.backgroundColor = UIColor.systemGroupedBackground
                   // style.inputViewBackgroundColor = .purple
            return style
        }
        
        lazy var messages: [[MSGMessage]] = []
        
//        lazy var messages: [[MSGMessage]] = {
//            return [
//                [
//                    MSGMessage(id: 1, body: .emoji("🐙💦🔫"), user: tim, sentAt: Date()),
//                    ],
//                [
//                    MSGMessage(id: 2, body: .text("Yeah sure, gimme 5"), user: steve, sentAt: Date()),
//                    MSGMessage(id: 3, body: .text("Okay ready when you are"), user: steve, sentAt: Date())
//                ]
//            ]
//        }()
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
    //        title = "iMessage"
            
            dataSource = self
            delegate = self
            
            updateUI()
            
            title = trip?.riderName
         
        }
    
    func updateUI(){
        let mTrip = trip!
        let mRiderId = mTrip.riderId!
        observeMessages(riderId: mRiderId, riderName: mTrip.riderName!, riderPhone: mTrip.riderPhoneNumber!) { (chats) in
            var mMessages = [MSGMessage]()
            
            // clear all data in the list
            self.messages = []
            self.collectionView.reloadData()
            
            for (index,chat) in chats!.enumerated() {
                
                print("index \(index) => message => \(chat.messageText!)")
                
                let user = CUser(displayName: Constants.applicationUser!.fullName!, avatar: UIImage(named: ""), avatarUrl: nil, isSender: chat.sentBy! == "customer")
                
                let message = MSGMessage(id: index, body: .text(chat.messageText!), user: user, sentAt: chat.createdTimestamp!)
                mMessages.append(message)
                
            }
            
            
            self.insert(mMessages)
            
            
            
        }
    }
        
        override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(animated)
            
            collectionView.scrollToBottom(animated: false)
        }
        
        override func inputViewPrimaryActionTriggered(inputView: MSGInputView) {
//            id += 1
            
//            let body: MSGMessageBody = (inputView.message.containsOnlyEmoji && inputView.message.count < 5) ? .emoji(inputView.message) : .text(inputView.message)
//
//            let message = MSGMessage(id: id, body: body, user: steve, sentAt: Date())
            let chat = Chat(messageText: inputView.message, riderId: trip!.riderId!, riderName: trip!.riderName!, riderPhone: trip!.riderPhoneNumber!)
            
                sendMessage(chat: chat, completion: {
                    error in
                    if error != nil {
                        return
                    }
                    
                   // self.collectionView.
                    
                    // there's a listener already observing the messages
    //                self.updateUI()
                })
            
            //insert(message)
        }
    
    
        
        override func insert(_ message: MSGMessage) {
            
            collectionView.performBatchUpdates({
                if let lastSection = self.messages.last, let lastMessage = lastSection.last, lastMessage.user.displayName == message.user.displayName {
                    self.messages[self.messages.count - 1].append(message)
                    
                    let sectionIndex = self.messages.count - 1
                    let itemIndex = self.messages[sectionIndex].count - 1
                    self.collectionView.insertItems(at: [IndexPath(item: itemIndex, section: sectionIndex)])
                    
                } else {
                    self.messages.append([message])
                    let sectionIndex = self.messages.count - 1
                    self.collectionView.insertSections([sectionIndex])
                }
            }, completion: { (_) in
                self.collectionView.scrollToBottom(animated: true)
                self.collectionView.layoutTypingLabelIfNeeded()
            })
            
        }
        
        override func insert(_ messages: [MSGMessage], callback: (() -> Void)? = nil) {
            
            collectionView.performBatchUpdates({
                for message in messages {
                    if let lastSection = self.messages.last, let lastMessage = lastSection.last, lastMessage.user.displayName == message.user.displayName {
                        self.messages[self.messages.count - 1].append(message)
                        
                        let sectionIndex = self.messages.count - 1
                        let itemIndex = self.messages[sectionIndex].count - 1
                        self.collectionView.insertItems(at: [IndexPath(item: itemIndex, section: sectionIndex)])
                        
                    } else {
                        self.messages.append([message])
                        let sectionIndex = self.messages.count - 1
                        self.collectionView.insertSections([sectionIndex])
                    }
                }
            }, completion: { (_) in
                self.collectionView.scrollToBottom(animated: false)
                self.collectionView.layoutTypingLabelIfNeeded()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    callback?()
                }
            })
            
        }
        
        
        @IBAction func backPressed(_ sender: Any) {
            dismiss(animated: true, completion: nil)
        }
        

}


// MARK: - MSGDataSource

extension ChatViewController: MSGDataSource {
    
    func numberOfSections() -> Int {
        return messages.count
    }
    
    func numberOfMessages(in section: Int) -> Int {
        return messages[section].count
    }
    
    func message(for indexPath: IndexPath) -> MSGMessage {
        return messages[indexPath.section][indexPath.item]
    }
    
    func footerTitle(for section: Int) -> String? {
       // return messages[section].first?.sentAt
        let message = messages[section].first!
        return dateToString(date: message.sentAt)
    }
    
    func headerTitle(for section: Int) -> String? {
        return messages[section].first?.user.displayName
    }

}


// MARK: - MSGDelegate
extension ChatViewController: MSGDelegate {
    
    func linkTapped(url: URL) {
        print("Link tapped:", url)
    }
    
    func avatarTapped(for user: MSGUser) {
        print("Avatar tapped:", user)
    }
    
    func tapReceived(for message: MSGMessage) {
        print("Tapped: ", message)
    }
    
    func longPressReceieved(for message: MSGMessage) {
        print("Long press:", message)
    }
    
    func shouldDisplaySafari(for url: URL) -> Bool {
        return true
    }
    
    func shouldOpen(url: URL) -> Bool {
        return true
    }
    
}
