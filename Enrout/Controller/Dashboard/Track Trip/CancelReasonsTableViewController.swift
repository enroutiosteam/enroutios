//
//  CancelReasonsTableViewController.swift
//  Enrout
//
//  Created by Daniel Kwakye on 07/01/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import UIKit

class CancelReasonsTableViewController: UITableViewController {
    @IBOutlet weak var closeBtn: UIButton!
    
    @IBOutlet var reasons: [UIButton]!
    @IBOutlet var icons: [UIImageView]!
    var selected:[Int:String] = [:]
    var trip: Trip?
    
    @IBOutlet weak var noBtn: UIButton!
    @IBOutlet weak var yesBtn: UIButton!
    
    var delegate: CancelOrderDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        closeBtn.addTarget(self, action: #selector(closeBtnTapped), for: .touchUpInside)
        noBtn.addTarget(self, action: #selector(closeBtnTapped), for: .touchUpInside)
        yesBtn.addTarget(self, action: #selector(doneBtnTapped), for: .touchUpInside)
        
        for (index,btn) in reasons.enumerated() {
            btn.tag = index
            btn.addTarget(self, action: #selector(reasonBtnTapped(_:)), for: .touchUpInside)
        }
    }
    
    @IBAction func reasonBtnTapped(_ sender: UIButton){
  
            let icon = self.icons[sender.tag]
            if icon.image == UIImage(named: "uncheck"){
                icon.image = UIImage(named: "checked")
                selected[sender.tag] = sender.titleLabel?.text
            }else{
                icon.image = UIImage(named: "uncheck")
                selected.removeValue(forKey: sender.tag)
            }
            
            self.icons[sender.tag].image = icon.image
            tableView.reloadData()

    }
    
    @IBAction func closeBtnTapped(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneBtnTapped(){
        var stringList = ""
        for item in selected {
           stringList = stringList + "\(item.value)"
           stringList = stringList + "|"
        }
        print("cancel order => \(stringList)")

       cancelTrip(viewController: self, trip: trip!, reasons: stringList, completion:   {
           self.dismiss(animated: true, completion: nil)
           self.delegate?.onConfirm(reasons: stringList)
        
       })
       
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 1{
            return 8
        }else{
            return 1
        }
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }
    

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 1 {
                let icon = self.icons[indexPath.row]
                if icon.image == UIImage(named: "uncheck"){
                    icon.image = UIImage(named: "checked")
                    selected[indexPath.row] = reasons[indexPath.row].titleLabel?.text
                }else{
                    icon.image = UIImage(named: "uncheck")
                    selected.removeValue(forKey: indexPath.row)
                }
                
                self.icons[indexPath.row].image = icon.image
                tableView.reloadData()
        }
        tableView.deselectRow(at: indexPath, animated: true)

    }

}


