//
//  TrackBottomViewController.swift
//  Enrout
//
//  Created by Daniel Expresspay on 20/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit
import Pulley

class TrackBottomViewController: UIViewController {

    var stop:Bool!
    var currentViewController: UIViewController?
    var mapViewController: TrackMapViewController?
    
    var trip: Trip?
      
    lazy var riderFoundViewController: RiderFoundViewController? = {
          let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
          let viewController = storyboard.instantiateViewController(identifier: "RiderFoundViewController") as! RiderFoundViewController
          return viewController
      }()
    
    private lazy var lookingForRiderViewController: LookingForRiderViewController? = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(identifier: "LookingForRiderViewController") as! LookingForRiderViewController
        return viewController
    }()
    
//    private lazy var rateRiderViewController: RateRiderViewController = {
//        return UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(identifier: "RateRiderViewController") as! RateRiderViewController
//    }()
    
    private lazy var delayedViewController: DelayedViewController = {
        return UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(identifier: "DelayedViewController") as! DelayedViewController
    }()
    
    private lazy var cancelledViewController: CancelledViewController = {
        return UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(identifier: "CancelledViewController") as! CancelledViewController
    }()
    
    private var shimmerViewController: ShimmerViewController = ShimmerViewController.shimmerViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //add(asChildViewController: ShimmerViewController.shimmerViewController)
        pulleyViewController?.setDrawerPosition(position: .collapsed, animated: true)
        add(asChildViewController: shimmerViewController)
        // Do any additional setup after loading the view.

        
        
        // get the current trip
        listenToStatusChange(tripId: trip!.requestId!) { (trip, status, error) in
           
            if let err = error {
                presentSweetAlert(presentingController: self, message: "Check your internet connection", showCancelBtn: false, delegate: self, title: "No connection")
                print(err.localizedDescription)
                return
            }
            
            
            print(" trip status => \(String(describing: status))")
            
            self.trip = trip
            
            // notify track mapviewcontroller of status change
            self.mapViewController?.tripStatusChanged(status: status!)
            switch status! {
  
            case .PENDING:
                self.pulleyViewController?.setDrawerPosition(position: .collapsed, animated: true)
                self.removeCurrentViewController()
                self.lookingForRiderViewController?.trip = trip
                self.lookingForRiderViewController?.trackBottomVC = self
                self.add(asChildViewController: self.lookingForRiderViewController!)
                
//                scheduleNotification(title: "Connecting", content: "Enrout is connecting you to the closest delivery partner")
                break
                
            case .CANCELLED:
                self.pulleyViewController?.setDrawerPosition(position: .partiallyRevealed, animated: true)
                self.removeCurrentViewController()
                self.add(asChildViewController: self.cancelledViewController)
                if !trip!.tripStatusNotified!.CANCELLED {
                    scheduleNotification(title: "Cancelled", content: "Your trip was cancelled. We're sorry for any inconvenience", trip: trip)
                    trip?.tripStatusNotified?.CANCELLED = true
                    markTripStatusAsNotified(trip: trip!)
                }
                break
            case .CANCELLED_R:
                self.pulleyViewController?.setDrawerPosition(position: .partiallyRevealed, animated: true)
                self.removeCurrentViewController()
                self.add(asChildViewController: self.cancelledViewController)
                
                if !trip!.tripStatusNotified!.CANCELLED_R {
                    scheduleNotification(title: "Cancelled", content: "Your trip was cancelled. We're sorry for any inconvenience", trip: trip)
                    trip?.tripStatusNotified?.CANCELLED_R = true
                     markTripStatusAsNotified(trip: trip!)
                }
               
                break
            case .ONGOING:
                self.pulleyViewController?.setDrawerPosition(position: .partiallyRevealed, animated: true)
                self.removeCurrentViewController()
                self.riderFoundViewController?.trip = trip
                self.add(asChildViewController: self.riderFoundViewController!)
    
                if !trip!.tripStatusNotified!.ONGOING {
                    let content = trip!.riderName! + " ( \(String(describing: trip!.riderPlateNumber!))  ) " + " has accepted your request"
                    scheduleNotification(title: "Rider Found", content: content, trip: trip)
                    trip?.tripStatusNotified?.ONGOING = true
                    markTripStatusAsNotified(trip: trip!)
                }
                
               
                break
            case .COMPLETED:
                

                if !trip!.tripStatusNotified!.COMPLETED {
                    let content = "Delivery completed. Kindly pay GHS  \(trip!.actualCost!) to \(trip!.riderName!)"
                    scheduleNotification(title: "Trip Completed", content: content, trip: trip)
                    trip?.tripStatusNotified?.COMPLETED = true
                    markTripStatusAsNotified(trip: trip!)
                     self.performSegue(withIdentifier: "rateRiderSegue", sender: nil)
                }
                
               
//                self.present(self.rateRiderViewController, animated: true, completion: nil)
//                if !self.riderFoundViewController!.isModal {
//
//                }
                
                break
            case .NOT_FOUND:
                self.pulleyViewController?.setDrawerPosition(position: .collapsed, animated: true)
                self.removeCurrentViewController()
                self.delayedViewController.trip = trip
                self.add(asChildViewController: self.delayedViewController)
                
                if !trip!.tripStatusNotified!.NOT_FOUND {
                    scheduleNotification(title: "Finding rider", content: "Please be patient. a delivery partner will contact you shortly", trip: trip)
                    trip?.tripStatusNotified?.NOT_FOUND = true
                    markTripStatusAsNotified(trip: trip!)
                }
                
                break
            case .PROCESSING:
                self.pulleyViewController?.setDrawerPosition(position: .collapsed, animated: true)
                self.removeCurrentViewController()
                self.lookingForRiderViewController?.trip = trip
                self.lookingForRiderViewController?.trackBottomVC = self
                self.add(asChildViewController: self.lookingForRiderViewController!)
                
                if !trip!.tripStatusNotified!.PROCESSING {
                    scheduleNotification(title: "Connecting", content: "Enrout is connecting you to the closest delivery partner", trip: trip)
                  trip?.tripStatusNotified?.PROCESSING = true
                  markTripStatusAsNotified(trip: trip!)
                }
                
                break
            case .DELAYED:
                self.pulleyViewController?.setDrawerPosition(position: .collapsed, animated: true)
                self.removeCurrentViewController()
                self.delayedViewController.trip = trip
                self.add(asChildViewController: self.delayedViewController)
                
                if !trip!.tripStatusNotified!.DELAYED {
                    scheduleNotification(title: "Delayed", content: "please be patient. a delivery partner will contact you shortly", trip: trip)
                                trip?.tripStatusNotified?.DELAYED = true
                                markTripStatusAsNotified(trip: trip!)
                }
            
                break
                
            }
            
           
        }
    
    }
    
    
    
    private func add(asChildViewController viewController: UIViewController) {
                    
    //        viewController.view.alpha = 0
    //        viewController.view.layoutIfNeeded()
        
        // Add Child View Controller
      self.addChild(viewController)
      
      // Add Child View as Subview
      self.view.addSubview(viewController.view)

      // Configure Child View
      viewController.view.frame = self.view.bounds
      viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]

      // Notify Child View Controller
      viewController.didMove(toParent: self)
                       
         self.currentViewController = viewController
        
//            UIView.transition(with: self.view, duration: 0.5, options: .transitionFlipFromLeft, animations: {
//
//               
//
//    //           viewController.view.alpha = 1
//    //           self.currentViewController?.view.alpha = 0
//
//
//            }, completion: {
//                finished in
//
//            })
            
    //        UIView.animate(withDuration: 0.5, delay: 0.1, options: .transitionFlipFromLeft, animations:  {
    //
    //        }) {
    //            finished in
    //
    //        }
        }
    
    private func removeCurrentViewController(){
          currentViewController?.willMove(toParent: nil)
          // Remove Child View From Superview
          currentViewController?.view.removeFromSuperview()
          // Notify Child View Controller
          currentViewController?.removeFromParent()
      }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "rateRiderSegue"{
//            self.rateRiderViewController.name = trip?.riderName
//            self.rateRiderViewController.image = self.riderFoundViewController?.riderImage
            let rateRiderVc = segue.destination as! RateRiderViewController
            rateRiderVc.name = trip?.riderName
            rateRiderVc.image = self.riderFoundViewController?.riderImage
            rateRiderVc.trip = trip!
        }
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension TrackBottomViewController: PulleyDrawerViewControllerDelegate {
   
    func collapsedDrawerHeight(bottomSafeArea: CGFloat) -> CGFloat
     {
         // For devices with a bottom safe area, we want to make our drawer taller. Your implementation may not want to do that. In that case, disregard the bottomSafeArea value.
         return (getScreenSize().height / 5) + (pulleyViewController?.currentDisplayMode == .drawer ? bottomSafeArea : 0.0)
     }

     
     func partialRevealDrawerHeight(bottomSafeArea: CGFloat) -> CGFloat
    {
        // For devices with a bottom safe area, we want to make our drawer taller. Your implementation may not want to do that. In that case, disregard the bottomSafeArea value.
        return (getScreenSize().height / 2) + (pulleyViewController?.currentDisplayMode == .drawer ? bottomSafeArea : 0.0)
    }
       
}

extension TrackBottomViewController: SweetAlertDelegate{
    func onConfirm() {
        self.dismiss(animated: true, completion: nil)
    }
}
