//
//  RateRiderViewController.swift
//  Enrout
//
//  Created by Daniel Expresspay on 20/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit
import Cosmos

class RateRiderViewController: UIViewController {
    @IBOutlet weak var removeRider: UIButton!
    @IBOutlet weak var couldBeBetter: UIButton!
    
    @IBOutlet weak var awesome: UIButton!
    @IBOutlet weak var belowAverage: UIButton!
    @IBOutlet weak var notTheBest: UIButton!
    @IBOutlet weak var commentTF: UITextField!
    
    @IBOutlet weak var riderName: UILabel!
    @IBOutlet weak var riderImage: CircularImageView!
    
    @IBOutlet weak var rateRider: UIButton!
    
    var image: CircularImageView?
    var name: String?
    
    var trip: Trip!
    
    var param = [
        "removeRider": false,
        "couldBeBetter": false,
        "awesome": false,
        "belowAverage": false,
        "notTheBest": false
    ]
    
    @IBOutlet weak var ratingBar: CosmosView!
    var mRatingValue = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupViews()
        addTargets()
        
        ratingBar.didTouchCosmos = {
            rating in
            let val = Int(rating)
            if val == 1 {
                self.switchButton(key: "removeRider", button: self.removeRider, value: 1)
            }else if val == 2 {
                self.switchButton(key: "notTheBest", button: self.notTheBest,value: 2)
            }
            else if val == 3 {
                self.switchButton(key: "belowAverage", button: self.belowAverage, value: 3)
            }
            else if val == 4 {
                self.switchButton(key: "couldBeBetter", button: self.couldBeBetter, value: 4)
            }
            else if val == 5 {
                self.switchButton(key: "awesome", button: self.awesome, value: 5)
            }
    
        }
        
        if let image = image {
            riderImage.image = image.image
        }else{
            riderImage.downloaded(from: trip.riderImage!)
        }
        riderName.text = name
 
    }
    
    func setupViews(){
        removeRider.curveCorners()
        removeRider.applyBorder()
        
        couldBeBetter.curveCorners()
        couldBeBetter.applyBorder()
        
        awesome.curveCorners()
        awesome.applyBorder()
        
        belowAverage.curveCorners()
        belowAverage.applyBorder()
        
        notTheBest.curveCorners()
        notTheBest.applyBorder()
        
        rateRider.curveCorners()
    }
    
    func addTargets(){
        removeRider.addTarget(self, action: #selector(removeRiderTapped), for: .touchUpInside)
        couldBeBetter.addTarget(self, action: #selector(couldBeBetterTapped), for: .touchUpInside)
        awesome.addTarget(self, action: #selector(awesomeTapped), for: .touchUpInside)
        belowAverage.addTarget(self, action: #selector(belowAverageTapped), for: .touchUpInside)
        notTheBest.addTarget(self, action: #selector(notTheBestTapped), for: .touchUpInside)
        
        // rate rider btn
        rateRider.addTarget(self, action: #selector(rateRiderTapped), for: .touchUpInside)
    }
    
    @IBAction func removeRiderTapped() {
        switchButton(key: "removeRider", button: removeRider, value: 1)
    }
    @IBAction func couldBeBetterTapped() {
        switchButton(key: "couldBeBetter", button: couldBeBetter, value: 4)
    }
    @IBAction func awesomeTapped() {
        switchButton(key: "awesome", button: awesome, value: 5)
    }
    @IBAction func belowAverageTapped() {
        switchButton(key: "belowAverage", button: belowAverage, value: 3)
    }
    @IBAction func notTheBestTapped() {
        switchButton(key: "notTheBest", button: notTheBest,value: 2)
    }
    
    func switchButton(key: String, button: UIButton, value: Int){
        clearAllButtons()
       let ret: Bool = param[key]!
       let isTrue =  ret == true ? false : true
        let dargreen = #colorLiteral(red: 0, green: 0.2122267783, blue: 0.2130401433, alpha: 1)
        button.backgroundColor = dargreen
        button.setTitleColor(UIColor.white, for: .normal)
      
       param[key] = isTrue
       mRatingValue = value
       reloadRatingValue()
       commentTF.text = button.titleLabel?.text
    }
    
    func clearAllButtons(){
        
        removeRider.backgroundColor = UIColor.clear
        removeRider.setTitleColor(UIColor.secondaryLabel, for: .normal)
        
        notTheBest.backgroundColor = UIColor.clear
        notTheBest.setTitleColor(UIColor.secondaryLabel, for: .normal)
        
        belowAverage.backgroundColor = UIColor.clear
        belowAverage.setTitleColor(UIColor.secondaryLabel, for: .normal)
        
        couldBeBetter.backgroundColor = UIColor.clear
        couldBeBetter.setTitleColor(UIColor.secondaryLabel, for: .normal)
        
        awesome.backgroundColor = UIColor.clear
        awesome.setTitleColor(UIColor.secondaryLabel, for: .normal)

    }
    
    func reloadRatingValue(){
        ratingBar.rating = Double(mRatingValue)
    }
    
    @IBAction func rateRiderTapped(){
        if mRatingValue == 0{
            Toast.error(view: self.view, message: "Please rate rider")
            return
        }
        
        Enrout.rateRider(viewController: self, riderId: trip.riderId!, tripId: trip.requestId!, mark: Double(mRatingValue), message: commentTF.text) { (error) in
            if let err = error {
                print(err.localizedDescription)
                Toast.error(view: self.view, message: "Sorry please try again")
                return
            }
            
            // refresh app
            Toast.success(view: self.view, message: "Done")
            self.performSegue(withIdentifier: "goToMainView", sender: self)
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
