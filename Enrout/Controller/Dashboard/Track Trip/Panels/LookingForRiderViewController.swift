//
//  LookingForRiderViewController.swift
//  Enrout
//
//  Created by Daniel Expresspay on 20/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit

class LookingForRiderViewController: UIViewController {
    
    @IBOutlet weak var cancelTripBtn: UIButton!
    
    private lazy var cancelReasonsTableViewController: CancelReasonsTableViewController = {
        return UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(identifier: "CancelReasonsTableViewController") as! CancelReasonsTableViewController
    }()
    
    var trip: Trip?
    var trackBottomVC: TrackBottomViewController?

    @IBOutlet weak var displayText: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        cancelTripBtn.addTarget(self, action: #selector(cancelTripTapped), for: .touchUpInside)
    }
    
    
    @IBAction func cancelTripTapped(){
        cancelReasonsTableViewController.delegate = self
        cancelReasonsTableViewController.trip = trip
        present(cancelReasonsTableViewController, animated: true, completion: nil)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension LookingForRiderViewController: CancelOrderDelegate{
      func onConfirm(reasons: String) {
          // initiate trip cancellation
          print("cancel order => \(reasons)")
        self.dismiss(animated: true, completion: nil)
//          cancelTrip(viewController: self, trip: trip, reasons: reasons, completion:   {
//              self.dismiss(animated: true, completion: nil)
//          })
          
      }
  }
