//
//  DelayedViewController.swift
//  Enrout
//
//  Created by Daniel Kwakye on 06/01/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import UIKit

class DelayedViewController: UIViewController {

    var trip: Trip?
    
    @IBOutlet weak var requestAgainBtn: UIButton!
    @IBOutlet weak var cancelTripBtn: UIButton!
    
    private lazy var cancelReasonsTableViewController: CancelReasonsTableViewController = {
           return UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(identifier: "CancelReasonsTableViewController") as! CancelReasonsTableViewController
       }()
    
    private lazy var checkoutTableViewController: CheckoutTableViewController = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "CheckoutTableViewController") as! CheckoutTableViewController
        
        return viewController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        requestAgainBtn.curveCorners()
        cancelTripBtn.curveCorners()
    }
    
    @IBAction func cancelTripTapped(_ sender: Any) {
        cancelReasonsTableViewController.delegate = self
        cancelReasonsTableViewController.trip = trip
        present(cancelReasonsTableViewController, animated: true, completion: nil)
    }
    

    @IBAction func requestAgainTapped(_ sender: Any) {
//        let loader = AnyProgressDialog.newInstance(view: self.view).show()
//           requestTripAgain(tripId: trip!.requestId!) { (returnedTrip, pickups, destinations, error) in
//               loader.dismis()
//
//               if let err = error {
//                   Toast.error(view: self.view, message: err.localizedDescription)
//
//                   return
//               }
//
//               UIConstants.activePickUps = pickups!
//               UIConstants.activeDestinations = destinations!
//               UIConstants.activeTrip = returnedTrip!
//
//                self.checkoutTableViewController.resetTripOnBackPressed = true
//               self.present(self.checkoutTableViewController, animated: true, completion: nil)
//
//           }
        scheduleNotification(title: "Request Booked", content: "A rider will contact you shortly")
       // self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension DelayedViewController: CancelOrderDelegate{
      func onConfirm(reasons: String) {
          // initiate trip cancellation
          print("cancel order => \(reasons)")
        self.dismiss(animated: true, completion: nil)
//          cancelTrip(viewController: self, trip: trip, reasons: reasons, completion:   {
//              self.dismiss(animated: true, completion: nil)
//          })
          
      }
  }
