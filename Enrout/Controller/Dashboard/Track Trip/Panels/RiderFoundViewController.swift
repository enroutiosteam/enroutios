//
//  RiderFoundViewController.swift
//  Enrout
//
//  Created by Daniel Expresspay on 20/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit
import Cosmos
import FirebaseFirestore
import FirebaseAuth

class RiderFoundViewController: UIViewController {
    @IBOutlet weak var riderImage: CircularImageView!
    @IBOutlet weak var riderRating: CosmosView!
    
    @IBOutlet weak var cancelTripBtn: UIButton!
    @IBOutlet weak var orderNo: UILabel!
    @IBOutlet weak var riderPlate: UILabel!
    @IBOutlet weak var riderName: UILabel!
    
    var trip: Trip?
    var pickups = [Pickup]()
    var destinations = [Destination]()
    let db = Firestore.firestore()
    var trackBottomVC: TrackBottomViewController?
    
    @IBOutlet weak var completingTrip: UILabel!
    
    
    @IBOutlet weak var tripPtsContainerView: UIView!
    
    private lazy var trippointlistTableViewController: TripPointsListTableViewController = {
        return UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(identifier: "TripPointsListTableViewController") as! TripPointsListTableViewController
    }()
    
    private lazy var cancelReasonsTableViewController: CancelReasonsTableViewController = {
           return UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(identifier: "CancelReasonsTableViewController") as! CancelReasonsTableViewController
       }()
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        cancelTripBtn.curveCorners()
        completingTrip.isHidden = true
        
        riderName.text = trip?.riderName
        riderPlate.text = trip?.riderPhoneNumber
        orderNo.text = "Order no - \(String(describing: trip!.orderNumber!))"
        
        if let imageUrl = trip?.riderImage {
            riderImage.downloaded(from: imageUrl)
        }
        
        self.trippointlistTableViewController.pickups = self.pickups
        self.trippointlistTableViewController.destinations = self.destinations
    self.tripPtsContainerView.addSubview(self.trippointlistTableViewController.view)
        
        trippointlistTableViewController.view.frame = tripPtsContainerView.bounds
        trippointlistTableViewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        trippointlistTableViewController.didMove(toParent: self)
        
        fetchPickpoints()
        
        fetchDriver()
        
        observeMessages(riderId: trip!.riderId!, riderName: trip!.riderName!, riderPhone: trip!.riderPhoneNumber!) { (_) in
        }
        
    }
    
    func fetchPickpoints(){
       
        listenToPickupChanges(trip: trip) { (pickups) in
            self.pickups = pickups
            self.trippointlistTableViewController.updatePoint(pickups: self.pickups, destinations: self.destinations)
            self.fetchDestinations()
            
            for pp in pickups {
                if !pp.isArrivedNotified! && pp.isArrived! {
                    let title = "Delivery partner Arrived"
                    let content = "Your delivery partner has arrived at pickup (\(pp.place!))"
                    scheduleNotification(title: title, content: content, trip: self.trip!)
                    markIsPickupArrivedAsNotified(pickup: pp)
                    break
                }
                
                if !pp.isPickedupNotified! && pp.isPickedup! {
                     let item = pp.itemCategory! == "" ? "item" : pp.itemCategory!
                    let title = "Item picked up"
                    let content = "Your \(item) has been picked up"
                    scheduleNotification(title: title, content: content, trip: self.trip!)
                    markIsPickedupAsNotified(pickup: pp)
                    break
                }
            }
        }
    }
    
    func showHideCancelBtn(show : Bool = true){
        if show {
            cancelTripBtn.isEnabled = true
        }else{
            cancelTripBtn.isEnabled = false
        }
    }
    
    func fetchDriver(){
        
        getRiderRating(riderId: trip!.riderId!) { (value) in
            if let val = value {
                let y = val.rounded(toPlaces: 2)
                self.riderRating.text = String(describing: y)
            }
           
        }
    }
    
    func fetchDestinations(){
    
        listenToDestChanges(trip: trip) { (destinations) in
            self.destinations = destinations
            self.trippointlistTableViewController.updatePoint(pickups: self.pickups, destinations: self.destinations)
            
            for dd in destinations {
                if !dd.isArrivedNotified! && dd.isArrived! {
                    let title = "Delivery partner Arrived"
                    let content = "Your delivery partner has arrived at pickup (\(dd.place!))"
                    scheduleNotification(title: title, content: content, trip: self.trip!)
                    markIsDestinationArrivedAsNotified(destination: dd)
                    break
                }
                if !dd.isDeliveredNotified! && dd.isDelivered! {
                   
                    let title = "Item delivered"
                    let content = "\(self.trip!.riderName!) has delivered your item"
                    scheduleNotification(title: title, content: content, trip: self.trip!)
                    markIsDestinationDeliveredAsNotified(destination: dd)
                }
            }
        }
            

    }
    
    func trackingRider(isFull: Bool?, tripSenderId: String?){
  
        if let isFull = isFull, let tripSenderId = tripSenderId {
            if isFull && tripSenderId == Auth.auth().currentUser!.uid {
               completingTrip.isHidden = false
           }else{
               completingTrip.isHidden = true
           }
        }
       
    }
    
    @IBAction func cancelOrderTapped(_ sender: Any) {
        cancelReasonsTableViewController.delegate = self
        cancelReasonsTableViewController.trip = trip
        present(cancelReasonsTableViewController, animated: true, completion: nil)
    }
    
    @IBAction func messageRider(_ sender: Any) {
        performSegue(withIdentifier: "showChatSegue", sender: self)
    }
    
    
    @IBAction func callRider(_ sender: Any) {
        
        guard let number = trip?.riderPhoneNumber else { return }
        if let url = URL(string: "tel://\(number)") {
            UIApplication.shared.open(url)
        }
      
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showChatSegue"{
            let navVC = segue.destination as? UINavigationController
            let chatVC = navVC?.viewControllers[0] as? ChatViewController
            chatVC?.trip = self.trip
        }
       
    }


}

extension RiderFoundViewController: CancelOrderDelegate{
    func onConfirm(reasons: String) {
        dismiss(animated: true, completion: nil)
    }
}
