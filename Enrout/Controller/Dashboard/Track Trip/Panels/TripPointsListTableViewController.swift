//
//  TripPointsListTableViewController.swift
//  Enrout
//
//  Created by Daniel Expresspay on 20/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit

class TripPointsListTableViewController: UITableViewController {

    var pickups: [Pickup]!
    var destinations: [Destination]!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source


    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return pickups.count + destinations.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TripPtTableViewCell

        // Configure the cell...
        if indexPath.row < (pickups.count) {
            
            print("pickup index => \(index)")
            // this is a pick up
            let point = pickups[indexPath.row]
            var status = "ON GOING"
            var color = UIColor.red
            if point.isPickedup != nil && point.isPickedup! {
                status = "ITEM PICKED UP"
                color = UIColor(red: 0, green: 118, blue: 141, alpha: 1)
            }else if point.isArrived != nil && point.isArrived! {
                status = "RIDER HAS ARRIVED"
                color = UIColor.yellow
            }
            cell.updateCell(icon: "pick_up_icon", title: "PICK UP", place: point.place, detail: "\(point.pickUpName != nil ? String(describing: point.pickUpName!) : "" ) \(point.pickUpPersonPhone != nil ? String(describing: point.pickUpPersonPhone!) : "" )", status: status, color: color)
            
        }else{
//            this is a destination point
            let index = indexPath.row - (pickups.count)
            print("destination index => \(index)")
            let point = destinations[index]
            var status = "ON GOING"
            var color = UIColor.red
            
            if point.isDelivered != nil && point.isDelivered! {
                status = "ITEM DELIVERED"
                color = #colorLiteral(red: 0, green: 0.2122267783, blue: 0.2130401433, alpha: 1)
            }else if point.isArrived != nil && point.isArrived! {
                status = "RIDER HAS ARRIVED"
                color = UIColor.yellow
            }
            
            cell.updateCell(icon: "delivery_raw", title: "DELIVERY", place: point.place, detail: "\(point.recipientName != nil ? String(describing: point.recipientName!) : "") \(point.recipientNumber != nil ? String(describing: point.recipientNumber!) : "")", status: status, color: color)
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func updatePoint(pickups: [Pickup], destinations: [Destination]){
        self.pickups = pickups
        self.destinations = destinations
        tableView.reloadData()
    }
    

    /* 
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
