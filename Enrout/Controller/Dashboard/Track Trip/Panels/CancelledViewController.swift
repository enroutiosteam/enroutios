//
//  CancelledViewController.swift
//  Enrout
//
//  Created by Daniel Kwakye on 06/01/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import UIKit

class CancelledViewController: UIViewController {

    @IBOutlet weak var requestAgainBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        requestAgainBtn.curveCorners()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
