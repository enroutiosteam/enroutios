//
//  PaymentSuccessfulViewController.swift
//  Enrout
//
//  Created by Daniel Kwakye on 15/01/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import UIKit

class PaymentSuccessfulViewController: UIViewController {
    
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var bellIcon: UIImageView!
    var delegate: PaymentSuccessDelegate?
    var shouldGoToMainView = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        doneBtn.curveCorners()
        doneBtn.addTarget(self, action: #selector(doneBtnTapped), for: .touchUpInside)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {

        
    }
    
    @IBAction func doneBtnTapped(){
        if shouldGoToMainView {
            performSegue(withIdentifier: "goToMainView", sender: nil)
            return
        }
        delegate?.onDoneTapped()
        self.dismiss(animated: true, completion: nil)
    }
    

}
