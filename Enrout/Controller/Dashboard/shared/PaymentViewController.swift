//
//  PaymentViewController.swift
//  Enrout
//
//  Created by Daniel Kwakye on 14/01/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import UIKit
import McPicker

class PaymentViewController: UIViewController {
    
    @IBOutlet weak var pageTitle: UILabel!
    @IBOutlet weak var pageSubTitle: UILabel!
    @IBOutlet weak var chooseNetwork: UITextField!
    @IBOutlet weak var momoNumber: UITextField!
    @IBOutlet weak var amountTF: UITextField!
    @IBOutlet weak var payNowBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var vodafoneAuthToken: UITextField!
    var delegate: PaymentCompleteDelegate?
    var paymentType: PaymentType = .payForWallet
    
    var fixedAmount: Double?
    var trip: Trip?
    
    private lazy var paymentSuccessfulViewController: PaymentSuccessfulViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(identifier: "PaymentSuccessfulViewController") as! PaymentSuccessfulViewController
        return viewController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        payNowBtn.curveCorners()
        //chooseNetwork.delegate = self
        chooseNetwork.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(chooseNetworkTapped)))
        momoNumber.delegate = self
        amountTF.delegate = self
        vodafoneAuthToken.delegate = self
        
        chooseNetwork.addTarget(self, action: #selector(chooseNetworkTapped), for: .editingDidBegin)
        
        backBtn.addTarget(self, action: #selector(closeBtnTapped), for: .touchUpInside)
        payNowBtn.addTarget(self, action: #selector(payNowBtnTapped), for: .touchUpInside)
        pageSubTitle.text = "Kindly enter your mobile money details. You'd receive a prompt"
        momoNumber.text = Constants.applicationUser?.phoneNumber
        vodafoneAuthToken.isHidden = true
        
        if fixedAmount != nil {
            amountTF.text = String(describing: fixedAmount!)
            amountTF.isEnabled = false
        }
        
        momoNumber.tintColor = UIColor.black
        
    }
    
    @IBAction func chooseNetworkTapped(){
        print("choose network tapped")
        amountTF.endEditing(true)
        momoNumber.endEditing(true)
        vodafoneAuthToken.endEditing(true)
        chooseNetwork.endEditing(true)
        McPicker.show(data: [["MTN", "AIRTEL/TIGO", "VODAFONE"]]) {  [weak self] (selections: [Int : String]) -> Void in
                if let name = selections[0] {
                    self?.chooseNetwork.text = name
                    if name == "VODAFONE" {
                        self!.vodafoneAuthToken.isHidden = false
                    }else{
                        self!.vodafoneAuthToken.isHidden = true
                    }
                }
        }
    }
    
    @IBAction func closeBtnTapped(){
        if paymentType == .payForTrip{
            performSegue(withIdentifier: "showMainViewSegue", sender: nil)
            return
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func payNowBtnTapped(){
        if chooseNetwork.text == ""{
            Toast.error(view: self.view, message: "Select network")
            return
        }
        if momoNumber.text! == ""{
            Toast.error(view: self.view, message: "Please enter your momo number")
            return
        }
        if amountTF.text! == ""{
            Toast.error(view: self.view, message: "Please enter amount")
            return
        }
        
        let amount = Double(amountTF.text!)!
        if amount <= 2.0 {
            Toast.error(view: self.view, message: "Amount cannot be less than GHS 3")
            return
        }
        
        if "VODAFONE" == chooseNetwork.text! {
            if vodafoneAuthToken.text! == "" {
                Toast.error(view: self.view, message: "Your vodafone token is required")
                return
            }
        }
        
        var momoNetwork: MomoNetwork = .MTN
        if chooseNetwork.text! == "MTN"{
            momoNetwork = .MTN
        }else if chooseNetwork.text! == "AIRTEL/TIGO"{
            momoNetwork = .AIRTEL
        }
        else if chooseNetwork.text! == "VODAFONE"{
            momoNetwork = .VODAFONE
        }
        
        switch paymentType {
        case .payForWallet:
            processPaymentForWallet(amount: amount, momoNetwork: momoNetwork)
            break
        case .payForTrip:
            processPaymentForTrip(amount: amount, momoNetwork: momoNetwork)
            break
        case .payForMe:
           processPaymentForTrip(amount: amount, momoNetwork: momoNetwork)
            break
        }
       
        
    }
    
    private func processPaymentForTrip(amount: Double, momoNetwork: MomoNetwork){
        Wallet.getInstance(view: self.view, viewController: self).payForTripWithMomo(trip: trip!, momoNumber: momoNumber.text!, network: momoNetwork, amount: amount, vodafoneAuthToken: vodafoneAuthToken.text) { (status, error) in
            if !status {
                Toast.error(view: self.view, message: error!.localizedDescription)
                return
            }
            
            self.paymentSuccessfulViewController.delegate = self
            if self.paymentType == .payForTrip {
                self.paymentSuccessfulViewController.shouldGoToMainView = true
            }else {
                self.paymentSuccessfulViewController.shouldGoToMainView = false
            }
    
        }
    }
    
    private func processPaymentForWallet(amount: Double, momoNetwork: MomoNetwork){
        
        Wallet.getInstance(view: self.view, viewController: self).addFunds(momoNumber: momoNumber.text!, network: momoNetwork, amount: amount, vodafoneAuthToken: vodafoneAuthToken.text) { (status, error) in
            if !status {
                Toast.error(view: self.view, message: error!.localizedDescription)
                return
            }
            
            self.paymentSuccessfulViewController.delegate = self
            self.present(self.paymentSuccessfulViewController, animated: true, completion: nil)
            
            
        }
    }

}

extension PaymentViewController: UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
//        if textField == momoNumber {
//            print("textFieldDidEndEditing momoNumber")
//        }else if textField == amountTF {
//             print("textFieldDidEndEditing amountTF")
//        }
//        else if textField == chooseNetwork {
//             print("textFieldDidEndEditing chooseNetwork")
//        }
//        else if textField == vodafoneAuthToken {
//             print("textFieldDidEndEditing vodafoneAuthToken")
//        }
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
//        if textField == momoNumber {
//            print("textFieldDidBeginEditing momoNumber")
//        }else if textField == amountTF {
//             print("textFieldDidBeginEditing amountTF")
//        }
//        else if textField == chooseNetwork {
//             print("textFieldDidBeginEditing chooseNetwork")
//        }
//        else if textField == vodafoneAuthToken {
//             print("textFieldDidBeginEditing vodafoneAuthToken")
//        }
//
//        if textField == chooseNetwork {
//
//
//
//        }
    }
}

extension PaymentViewController: PaymentSuccessDelegate {
    func onDoneTapped() {
        if paymentType == .payForWallet{
             self.delegate?.onPayentComplete(error: nil)
             self.dismiss(animated: true, completion: nil)
        }else if paymentType == .payForTrip {
             self.performSegue(withIdentifier: "showMainViewSegue", sender: nil)
        }
       
    }
}
