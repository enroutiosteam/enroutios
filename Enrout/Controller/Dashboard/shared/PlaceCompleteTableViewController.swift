//
//  PlaceCompleteTableViewController.swift
//  Enrout
//
//  Created by Daniel Expresspay on 26/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit
import MGSwipeTableCell

class PlaceCompleteTableViewController: UITableViewController {
    
    public static var placeCompleteViewController: PlaceCompleteTableViewController = {
           return UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(identifier: "PlaceCompleteTableViewController") as! PlaceCompleteTableViewController
    }()
    var placeList = [PlaceComplete]()
    var point: PointType = .PICKUP
    
    var placeCompleteSource: OnPlaceCompletedDelegate?
    var swipeCompleteSource: PlaceCompleteSwipedDelegate?
    var rowIsSelectable = true
    var rowIsSwipable = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        var place = PlaceComplete()
//        place.name = "Teksol Limited"
//        place.description = "Accra Ghana"
//        place.icon = UIImage(named: "pick_up_icon")
//        placeList.append(place)
    }


    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return placeList.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PlaceCompleteTableViewCell

        let place = placeList[indexPath.row]
        place.index = indexPath.row
        if point == .PICKUP {
            place.icon = UIImage(named: "pick_up_icon")
        }else{
            place.icon = UIImage(named: "delivery_raw")
        }
        cell.updateDetails(placeComplete: place)
        cell.delegate = self
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let placeSelected = placeList[indexPath.row]
        if rowIsSelectable {
            placeCompleteSource?.onPlaceSelected(placeComplete: placeSelected)
        }
       
    }
    

}

extension PlaceCompleteTableViewController: MGSwipeTableCellDelegate {
    
    func swipeTableCell(_ cell: MGSwipeTableCell, swipeButtonsFor direction: MGSwipeDirection, swipeSettings: MGSwipeSettings, expansionSettings: MGSwipeExpansionSettings) -> [UIView]? {
        
        if direction == .leftToRight {
            return nil
        }
        
        swipeSettings.transition = MGSwipeTransition.border;
        expansionSettings.buttonIndex = 0;
        
        expansionSettings.fillOnTrigger = true;
        expansionSettings.threshold = 1.1;
        let padding = 15;
        let color1 = UIColor.init(red:1.0, green:59/255.0, blue:50/255.0, alpha:1.0);
        
        let trash = MGSwipeButton(title: "Delete", icon: UIImage(systemName: "trash")?.withTintColor(UIColor.white), backgroundColor: color1, padding: padding, callback: {
            cell in
            cell.rightExpansion.fillOnTrigger = false
                        //print("swipe offset => \(String(describing: ce))")
            let indexPath = (self.tableView.indexPath(for: cell)?.row)!
            self.swipeCompleteSource?.onSwipe(placeComplete: self.placeList[indexPath])
            return false
        });
        
        
        return [trash]
        
    }
    
    func swipeTableCell(_ cell: MGSwipeTableCell, canSwipe direction: MGSwipeDirection, from point: CGPoint) -> Bool {
        return rowIsSwipable
    }
}

extension PlaceCompleteTableViewController : OnPlaceCompletedDelegate {
     
      func onTextChanged(query: String) {
         // query from the server and relaod table
        print("text received - query - \(query)")
        searchGooglePlaces(query: query) { (placeList) in
            
            guard let placeList = placeList else {
                return
            }
            
            self.placeList = placeList
            
            self.tableView.reloadData()
            self.placeCompleteSource?.doneSearching(foundPlaces: true)
        }
        
     }
    
    func addItem(placeComplete: PlaceComplete){
        self.placeList = []
        self.placeList.append(placeComplete)
        
        self.tableView.reloadData()
    }
}
