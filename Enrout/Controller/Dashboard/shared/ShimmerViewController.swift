//
//  ShimmerViewController.swift
//  Enrout
//
//  Created by Daniel Expresspay on 20/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit
import ShimmerSwift

class ShimmerViewController: UIViewController {
    @IBOutlet weak var view1: ShimmeringView!
    @IBOutlet weak var view2: ShimmeringView!
    @IBOutlet weak var view3: ShimmeringView!
    @IBOutlet weak var view4: ShimmeringView!
    @IBOutlet weak var view5: ShimmeringView!
    
     
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupShimmer(shimmerView: view1)
        setupShimmer(shimmerView: view2)
        setupShimmer(shimmerView: view3)
        setupShimmer(shimmerView: view4)
        setupShimmer(shimmerView: view5)
        
        // Setup ShimmeringView
        
        
//
//        let shimmerView3 = ShimmeringView(frame: self.view3.bounds)
//        shimmerView3.isShimmering = true
//        self.view3.addSubview(shimmerView3)
//
//        let shimmerView4 = ShimmeringView(frame: self.view4.bounds)
//        shimmerView4.isShimmering = true
//        self.view4.addSubview(shimmerView4)
        
        // Do any additional setup after loading the view.
    }
    
    func setupShimmer(shimmerView: ShimmeringView){
        
        let bg = shimmerView.backgroundColor
        let mView = UIView(frame: shimmerView.bounds)
        mView.backgroundColor = bg
        shimmerView.backgroundColor = nil

        shimmerView.contentView = mView
        
        shimmerView.isShimmering = true
        shimmerView.shimmerSpeed = 400
        shimmerView.shimmerPauseDuration = 0.0

    }
    
    public static var shimmerViewController: ShimmerViewController = {
        return UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(identifier: "ShimmerViewController") as! ShimmerViewController
    }()

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
