//
//  PoorConnectionViewController.swift
//  Enrout
//
//  Created by Daniel Kwakye on 07/02/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import UIKit

class PoorConnectionViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    public static var poorConnectionViewController: PoorConnectionViewController = {
           return UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(identifier: "PoorConnectionViewController") as! PoorConnectionViewController
       }()

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
