//
//  ItemCategoriesViewController.swift
//  Enrout
//
//  Created by Daniel Expresspay on 28/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit

class ItemCategoriesViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var cancelBtn: UIButton!
    public static var itemCategoriesViewController: ItemCategoriesViewController = {
        return UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(identifier: "ItemCategoriesViewController") as! ItemCategoriesViewController
    }()
    
    var delegate: ItemCategoryDelegate?
    
    @IBOutlet weak var collectionView: UICollectionView!
    let list = [
        ItemCategory(icon: "order_history_icon", label: "Documents"),
        ItemCategory(icon: "cake", label: "Cakes"),
        ItemCategory(icon: "appliance", label: "Appliance"),
        ItemCategory(icon: "books", label: "Books"),
        ItemCategory(icon: "groceries", label: "Groceries"),
        ItemCategory(icon: "device", label: "Device"),
        ItemCategory(icon: "food", label: "Food"),
        ItemCategory(icon: "others", label: "Others")
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        collectionView.dataSource = self
        collectionView.delegate = self
        
        cancelBtn.addTarget(self, action: #selector(cancelBtnTapped(_:)), for: .touchUpInside)
        
    }
    
    @IBAction func cancelBtnTapped(_ sender: Any){
        delegate?.onCancel()
        self.dismiss(animated: true, completion: nil)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! ItemCategoryViewCell
        let item = list[indexPath.row]
        item.index = indexPath.row
        cell.image.image = UIImage(named: item.icon!)
        cell.label.text = item.label
        return cell
    }
    
    
    // make collection view 2 colums
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
        let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
        let size:CGFloat = (collectionView.frame.size.width - space) / 2.0
        return CGSize(width: size, height: size)
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let padding: CGFloat =  50
//        let collectionViewSize = self.collectionView.frame.size.width - padding
//
//        return CGSize(width: collectionViewSize/2, height: collectionViewSize/2)
//    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = list[indexPath.row]
        delegate?.onSelect(item: item)
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
