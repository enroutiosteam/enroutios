//
//  VendorsTableViewCell.swift
//  Enrout
//
//  Created by Daniel Kwakye on 05/02/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import UIKit
import SkeletonView
import ImageLoader

class VendorsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var details: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        title.showAnimatedSkeleton()
        logo.showAnimatedSkeleton()
        details.showAnimatedSkeleton()
    }
    
    func hideAnimation(){
        title.hideSkeleton()
        details.hideSkeleton()
    }
    
    func updateUI(vendor: Vendor) {
        if let logo = vendor.vendorLogo, logo != "" {
            self.logo.load.request(with: logo, onCompletion: {
                image, error ,_  in
                if let err = error {
                   print("Error loading vendor image => \(err.localizedDescription)")
                   DispatchQueue.main.async {
                       self.logo.hideSkeleton()
                   }
                    
                    return
               }
                guard image != nil else {
                    DispatchQueue.main.async {
                         self.logo.hideSkeleton()
                    }
                    return
                }
                
                self.logo.hideSkeleton()
                self.logo.image = image

            })
        }else{
            self.logo.hideSkeleton()
        }
        
        title.text = vendor.vendorName
        details.text = vendor.vendorDescription
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
