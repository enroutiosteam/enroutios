//
//  CheckoutTableViewCell.swift
//  Enrout
//
//  Created by Daniel Expresspay on 19/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit

class CheckoutTableViewCell: UITableViewCell {

    @IBOutlet weak var place: UILabel!
    @IBOutlet weak var details: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func update(place: String?, details: String?){
        self.place.text = place
        self.details.text = details
    }

}
