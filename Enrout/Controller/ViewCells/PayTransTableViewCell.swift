//
//  PayTransTableViewCell.swift
//  Enrout
//
//  Created by Daniel Kwakye on 15/01/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import UIKit

class PayTransTableViewCell: UITableViewCell {
    
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var dateTime: UILabel!
    @IBOutlet weak var details: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func updateValues(amount: String, date: String, details: String){
        self.amount.text = amount
        self.dateTime.text = date
        self.details.text = details
    }

}
