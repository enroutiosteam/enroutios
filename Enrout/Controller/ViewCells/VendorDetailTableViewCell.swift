//
//  VendorDetailTableViewCell.swift
//  Enrout
//
//  Created by Daniel Kwakye on 05/02/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import UIKit
import SkeletonView

class VendorDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var vendorBanner: UIImageView!
    @IBOutlet weak var vendorDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        vendorDescription.showSkeleton()
        vendorBanner.showSkeleton()
    }
    
    func updateUI(vendor: Vendor){
        if let banner = vendor.vendorBanner, banner != "" {
            self.vendorBanner.load.request(with: banner){ image, error,_ in
                if let err = error {
                    print("Error loading banner => \(err.localizedDescription)")
                    DispatchQueue.main.async {
                        self.vendorBanner.hideSkeleton()
                    }
                    
                    return
                }
                guard image != nil else {
                    DispatchQueue.main.async {
                        self.vendorBanner.hideSkeleton()
                    }
                    return
                }
                
                self.vendorBanner.hideSkeleton()
                self.vendorBanner.image = image
                
                
            }
        }else{
            self.vendorBanner.hideSkeleton()
        }
        
        vendorDescription.text = vendor.vendorDescription
    }
    
    func hideAnimation(){
        vendorDescription.hideSkeleton()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
