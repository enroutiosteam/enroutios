//
//  TripPtTableViewCell.swift
//  Enrout
//
//  Created by Daniel Expresspay on 20/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit

class TripPtTableViewCell: UITableViewCell {
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var place: UILabel!
    @IBOutlet weak var detail: UILabel!
    @IBOutlet weak var status: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateCell(icon: String, title: String?, place: String?, detail: String?, status: String?, color: UIColor = UIColor.black) {
        self.icon.image = UIImage(named: icon)
        self.title.text = title
        self.place.text = place
        self.detail.text = detail
        self.status.text = status
        self.status.textColor = color
    }

}
