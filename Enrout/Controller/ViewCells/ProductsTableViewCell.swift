//
//  ProductsTableViewCell.swift
//  Enrout
//
//  Created by Daniel Kwakye on 07/02/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import UIKit
import SkeletonView
import ImageLoader

class ProductsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var productImage: CircularImageView!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var productDescription: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        productImage.showSkeleton()
        productTitle.showSkeleton()
        productDescription.showSkeleton()
        productPrice.showSkeleton()
    }
    
    func updateUI(product: Product){
        if let image = product.productImage, image != "" {
            self.productImage.load.request(with: image, onCompletion: {
                image,error,_ in
                if let err = error {
                    print("Error loading product => \(err.localizedDescription)")
                    DispatchQueue.main.async {
                         self.productImage.hideSkeleton()
                    }
                    
                    return
                }
                guard image != nil else {
                   DispatchQueue.main.async {
                        self.productImage.hideSkeleton()
                   }
                   return
               }
                
                self.productImage.hideSkeleton()
                self.productImage.image = image
              
            })
        }else{
            self.productImage.hideSkeleton()
        }
        
        productTitle.text = product.productTitle
        productDescription.text = product.productDescription
        productPrice.text = "GHS \(String(product.productPrice!))"
    }
    
    func hideAnimation(){
        productTitle.hideSkeleton()
        productDescription.hideSkeleton()
        productPrice.hideSkeleton()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
