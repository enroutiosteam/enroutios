//
//  ReceiptLocationsTableViewCell.swift
//  Enrout
//
//  Created by Daniel Kwakye on 20/01/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import UIKit

class ReceiptLocationsTableViewCell: UITableViewCell {

    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var place: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func update(place: String, point: PointType){
        self.place.text = place
        if point == .PICKUP {
            title.text = "PICK UP"
            icon.image = UIImage(named: "pick_up_icon")
        }else{
            title.text = "DESTINATION"
            icon.image = UIImage(named: "delivery_raw")
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
