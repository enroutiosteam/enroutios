//
//  ItemCategoryViewCell.swift
//  Enrout
//
//  Created by Daniel Expresspay on 28/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit

class ItemCategoryViewCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var label: UILabel!
    
}
