//
//  PlaceCompleteTableViewCell.swift
//  Enrout
//
//  Created by Daniel Expresspay on 26/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit
import MGSwipeTableCell

class PlaceCompleteTableViewCell: MGSwipeTableCell{
    
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var desc: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateDetails(placeComplete: PlaceComplete){
        title.text = placeComplete.name
        desc.text = placeComplete.description
        icon.image = placeComplete.icon
    }

}
