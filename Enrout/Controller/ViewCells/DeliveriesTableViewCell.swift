//
//  DeliveriesTableViewCell.swift
//  Enrout
//
//  Created by Daniel Expresspay on 13/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit
import MGSwipeTableCell

class DeliveriesTableViewCell: MGSwipeTableCell {

    @IBOutlet weak var leftIcon: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var rightIcon: UIImageView!
    @IBOutlet weak var rightText: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func update(with delivery: Delivery) {
        leftIcon.image = UIImage(named: delivery.leftIcon ?? "ongoing")
        title.text = delivery.title
        subtitle.text = delivery.subTitle
        rightIcon.image = UIImage(named: delivery.rightIcon ?? "ongoing")
        rightText.text = delivery.rightText
    }

}
