//
//  RegisterViewController.swift
//  Enrout
//
//  Created by Daniel Kwakye on 28/06/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit
import MaterialTextField
import FlagPhoneNumber

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var username: MFTextField!
    @IBOutlet weak var userPhoneNumber: FPNTextField!
    
    @IBOutlet weak var continueBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        makeNavBarTransparent()
        // Do any additional setup after loading the view.
        userPhoneNumber.setFlag(key: .GH)
        continueBtn.addTarget(self, action: #selector(continueBtnTapped), for: .touchUpInside)
    }
    
    @IBAction func continueBtnTapped(){
        if username.text == nil || username.text == "" {
            Toast.error(view: self.view, message: "Your name is required")
            username.becomeFirstResponder()
            return
        }
        let phone = userPhoneNumber.getFormattedPhoneNumber(format: .E164)
        print("phone number => \(phone)")
        if phone == nil || phone == "+(null)(null)" {
            Toast.error(view: self.view, message: "Invalid phone number")
            userPhoneNumber.becomeFirstResponder()
            return
        }
        performSegue(withIdentifier: "goToRegOptionsSegue", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToRegOptionsSegue" {
            let optionsVc = segue.destination as! RegistrationOptionViewController
            optionsVc.username = username.text
            optionsVc.phoneNumber = userPhoneNumber.getFormattedPhoneNumber(format: .E164)
            
            print("phone number => \(optionsVc.phoneNumber!)")
        }
    }
    
    
    @IBAction func openTermsAndConditions(_ sender: Any) {
        openTerms(viewcontroller: self)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    


}
