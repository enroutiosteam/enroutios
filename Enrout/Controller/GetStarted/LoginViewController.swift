//
//  LoginViewController.swift
//  Enrout
//
//  Created by Daniel Kwakye on 28/06/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        makeNavBarTransparent()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    

    @IBAction func openTermsAndConditions(_ sender: UIButton) {
        openTerms(viewcontroller: self)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func continueWithGoogle(_ sender: Any) {
        signInWithGoogle()
    }
    
    @IBAction func continueWithPhoneNumber(_ sender: Any) {
        
    }
    
    
    @IBAction func continueWithFacebook(_ sender: Any) {
        signInWithFacebook()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
           if segue.identifier == "goToVerifySegue" {
               let vc = segue.destination as! VerifyViewController
                vc.isLogin = true
            
        
           }
       }
    
    
    
    
    
    
    

}
