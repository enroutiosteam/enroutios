//
//  InitialViewController.swift
//  Enrout
//
//  Created by Jude Botchwey on 20/11/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit


@objc public protocol InitialViewControllerDelegate: class {
    
    func displayLoginViewController()
    func displaySavedLoginViewController()
    @objc optional func displayHomeViewController()
    @objc optional func loginSuccessfullyCompleted()

}


class InitialViewController: UIViewController {

    
    @IBOutlet weak var gettingStartedButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    weak open var delegate:InitialViewControllerDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        makeNavBarTransparent()
        setupView()
    }
    
    
    func setupView(){
        loginButton.layer.cornerRadius = 12.0
        loginButton.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        loginButton.layer.borderWidth = 1.0
        
        gettingStartedButton.layer.cornerRadius = 12.0
        gettingStartedButton.layer.borderColor = #colorLiteral(red: 0, green: 0.8078431373, blue: 0.8196078431, alpha: 1); gettingStartedButton.layer.borderWidth = 1.0
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    //IB Actions
    
    @IBAction func loginTapped(_ sender: Any) {
        delegate?.displayLoginViewController()
    }
    
}
