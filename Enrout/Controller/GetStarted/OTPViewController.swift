//
//  OTPViewController.swift
//  Enrout
//
//  Created by Jude Botchwey on 26/11/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit


@objc public protocol OTPViewControllerDelegate{
    
    @objc optional func displayHomeViewController()
    @objc optional func displayLoginViewController()
    @objc optional func otpProcessingCompleted()
    
}



class OTPViewController: UIViewController {

    @IBOutlet weak var verificationCodeMessage: UILabel!
    @IBOutlet weak var customPinView: CustomPinView!
    @IBOutlet weak var resendButton: UIButton!
    weak open var delegate:OTPViewControllerDelegate?
    
    var userName: String?
    var phoneNumber: String?
    var isLogin = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customPinView.addTarget(self, action: #selector(onPINEditing(_:)), for: .editingDidEnd)
        resendButton.layer.cornerRadius = 8
        resendButton.layer.borderWidth = 1
        resendButton.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        sendVerificationCode()
//
        verificationCodeMessage.text = "Verification code has been sent to \(phoneNumber!)"
    }
    
    func sendVerificationCode(){
        if isLogin {
            loginWithPhoneNumber(phoneNumber: phoneNumber!)
        }else{
            registerWithPhoneNumber(username: userName!, phoneNumber: phoneNumber!)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        customPinView.becomeFirstResponder()
    }
    
    @IBAction func onPINEditing(_ sender: CustomPinView) {
         print("PIN is ---> \(sender.pinString)")
         doProcessing(otpValue: sender.pinString)
         sender.resignFirstResponder()
         customPinView.becomeFirstResponder()
      }
    
    
    @IBAction func resendOTPTapped(_ sender: Any) {
        sendVerificationCode()
    }
    
    private func doProcessing(otpValue: String){
        let defaults = UserDefaults.standard
        defaults.set(true, forKey: OTP_SUCCESSFUL)
        
        verifyCode(verificationCode: otpValue)
        
    }
    


}
