//
//  RegistrationOptionViewController.swift
//  Enrout
//
//  Created by Daniel Kwakye on 28/06/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit

class RegistrationOptionViewController: UIViewController {

    var username: String?
    var phoneNumber: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        makeNavBarTransparent()
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func registerWithGoogle(_ sender: Any) {
        registerWithGoogle(username: self.username!, phoneNumber: self.phoneNumber!)
    }
    
    @IBAction func registerWithFacebook(_ sender: Any) {
       registerWithFacebook(username: self.username!, phoneNumber: self.phoneNumber!)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToPinSegue" {
            let vc = segue.destination as! OTPViewController
            vc.userName = self.username
            vc.phoneNumber = self.phoneNumber
            vc.isLogin = false
        }
    }
    
    
}
