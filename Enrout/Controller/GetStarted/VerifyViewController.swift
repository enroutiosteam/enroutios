//
//  VerifyViewController.swift
//  Enrout
//
//  Created by Jude Botchwey on 23/11/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit
import FlagPhoneNumber

class VerifyViewController: UIViewController {

    @IBOutlet weak var verifyButton: UIButton!
    @IBOutlet weak var phoneNumber: FPNTextField!
    
    var isLogin = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        makeNavBarTransparent()
        setupView()
        // Do any additional setup after loading the view.
        verifyButton.addTarget(self, action: #selector(verifyBtnTapped), for: .touchUpInside)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func verifyBtnTapped(){
        let phone = phoneNumber.getFormattedPhoneNumber(format: .E164)
        print("phone number => \(phone)")
        if phone == nil || phone == "+(null)(null)" {
            Toast.error(view: self.view, message: "Invalid phone number")
            return
        }
        performSegue(withIdentifier: "goToOTP", sender: nil)
    }
    
    
    func setupView(){
        phoneNumber.setFlag(key: .GH)
        verifyButton.layer.cornerRadius = 12.0
        verifyButton.layer.borderColor = #colorLiteral(red: 0, green: 0.8078431373, blue: 0.8196078431, alpha: 1); verifyButton.layer.borderWidth = 1.0
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToOTP" {
            
          let otpVc = segue.destination as! OTPViewController
            otpVc.phoneNumber = phoneNumber.getFormattedPhoneNumber(format: .E164)
            otpVc.isLogin = self.isLogin
        }
    }
    
    
    
    @IBAction func closeButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
