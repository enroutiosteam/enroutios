//
//  ContainerViewController.swift
//  Enrout
//
//  Created by Jude Botchwey on 18/11/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit
import Pulley
import FirebaseAuth
import SocketIO
import StoreKit

class ContainerViewController: UIViewController, InitialViewControllerDelegate{

    let defaults = UserDefaults.standard
    public var currentViewController: UIViewController?
    var mSocket: SockIOConnection?
    //let drawerContentVC = UIViewController(
        
//        .instantiateViewController(withIdentifier: "BottomContentViewController")
    var pulleyController: PulleyViewController?
    var mapView: MainViewController?
    

    private lazy var initialViewController: UINavigationController = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        // Instantiate View Controller
        var navigationController = storyboard.instantiateViewController(withIdentifier: "initialNavigationController") as! UINavigationController
        var viewController = navigationController.viewControllers[0] as! InitialViewController
        viewController.delegate = self
        
        return navigationController
    }()
    
    private lazy var mainViewController: UINavigationController = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        // Instantiate View Controller
        var navigationController = storyboard.instantiateViewController(withIdentifier: "MainNavigationController") as! UINavigationController
        var viewController = navigationController.viewControllers[0] as! MainViewController
        //viewController.delegate = self
        
        return navigationController
    }()
    
    
    
    private lazy var testViewController: UIViewController = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "TestNavigationController") as! TestViewController
        
        return viewController
    }()
    
    private lazy var rateRiderViewController: RateRiderViewController = {
           // Load Storyboard
           let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
           
           // Instantiate View Controller
           var viewController = storyboard.instantiateViewController(withIdentifier: "RateRiderViewController") as! RateRiderViewController
           
           return viewController
       }()
    
    private lazy var payForTripViewController: PayForTripViewController = {
       // Load Storyboard
       let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
       
       // Instantiate View Controller
       var viewController = storyboard.instantiateViewController(withIdentifier: "PayForTripViewController") as! PayForTripViewController
       
       return viewController
   }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView = mainViewController.viewControllers[0] as? MainViewController
        mapView?.containerVC = self
        setupView()
        
        
        if Auth.auth().currentUser == nil {
            return
        }
        
        if currentViewController != pulleyController {
            return
        }
        
        mSocket = SockIOConnection.getSocketInstance()
           mSocket?.socket.on("connection::check::isConnected", callback: { (data, ark) in
               self.mSocket?.socket.emit("connection::check::isConnected::received", [])
           })
       
        
       performAppChecks()
        
    }
    
    
    
    private func attemptAppReview(){
        shouldShowReviewDialog { (show) in
            if !show {
                return
            }
            
            presentModalController(presentingController: self, imageName: "modal_image", headerTitle: "Rate our services", subHeaderTitle: "Kindly rate us to indicate how you feel about our services ", doneHidden: false, cancelHidden: false) { (result) in
                                          
                  if result == 1 {
                      //
                    let dialog = AnyProgressDialog.newInstance(view: self.view).show()
                    updateAppReviewStatusToTrue { (updated, user) in
                        dialog.dismis()
                        SKStoreReviewController.requestReview()
                    }
                      
                  }else{
                      // cancelled
                  }
              }

            print("show dialog")
        }
    }
    
    private func performAppChecks(){
        checkIfTheresUnratedTrip { (trip) in
                   if let trip = trip {
                       // check if there's a trip that needs to be rated
                    
                       self.rateRiderViewController.trip = trip
                       self.rateRiderViewController.name = trip.riderName!
                       self.swapCurrentViewController(withChildViewController: self.rateRiderViewController)
                   }else{
                       // check if there's an outstanding payment
                       checkOutstandingPayment { (foundPendingTrip, foundTrip, exceededTripRequest) in
                           if !foundPendingTrip {
                            self.attemptAppReview()
                               return
                           }
                           
                           if exceededTripRequest {
                               // go straight to payment page
                               self.payForTripViewController.trip = foundTrip!
                               self.payForTripViewController.limitExceeded = exceededTripRequest
                               self.payForTripViewController.containerVC = self
                               self.swapCurrentViewController(withChildViewController: self.payForTripViewController)
                               return
                           }
                        
                        
                           // there are outstanding payment
                           presentModalController(presentingController: self, imageName: "outstanding_icon", headerTitle: "Outstanding Payment(s)", subHeaderTitle: "To request another delivery partner, kindly clear all outstanding", doneHidden: false, cancelHidden: false) { (result) in
                               
                               if result == 1 {
                                   // go to payment page
                                   self.payForTripViewController.trip = foundTrip!
                                   self.payForTripViewController.limitExceeded = exceededTripRequest
                                   self.payForTripViewController.containerVC = self
                                   self.swapCurrentViewController(withChildViewController: self.payForTripViewController)
                                   
                               }else{
                                   // cance
                               }
                           }
                       }
                       
                   }
               }
    }
    

    func displayLoginViewController() {
          
    }
      
    func displaySavedLoginViewController() {
          
    }
    
    
    
       // MARK: - Helper Methods
    func swapCurrentViewController(withChildViewController viewController: UIViewController) {
        
        //remove currentViewCOntroller
        removeCurrentViewController()
        
        addChild(viewController)
        view.addSubview(viewController.view)
        
        viewController.view.frame = view.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParent: self)
        currentViewController = viewController
        setNeedsStatusBarAppearanceUpdate()
        
    }
    
    private func removeCurrentViewController(){
        currentViewController?.willMove(toParent: nil)
        // Remove Child View From Superview
        currentViewController?.view.removeFromSuperview()
        // Notify Child View Controller
        currentViewController?.removeFromParent()
    }
    
    
    
    public override var childForStatusBarStyle: UIViewController?{
        return currentViewController
    }
    
//    func collapsedDrawerHeight(bottomSafeArea: CGFloat) -> CGFloat {
//        return getScreenSize().height / 2
//    }
    

    func switchToMainView(){
         let drawerContentVC = BottomViewController()
          
          drawerContentVC.mainViewController = mapView
          mapView?.bottomViewController = drawerContentVC
          pulleyController = PulleyViewController(contentViewController: mainViewController, drawerViewController: drawerContentVC)

         swapCurrentViewController(withChildViewController: pulleyController!)
        
    }
    
    
    func switchToStartPage(){
        swapCurrentViewController(withChildViewController: initialViewController)
    }
    
    func setupView(){
    
        //Check to see if this is the first time using the app
        
//        swapCurrentViewController(withChildViewController: testViewController)
        
//        if !defaults.bool(forKey: USER_PASSED_INITIAL) {
//            swapCurrentViewController(withChildViewController: initialViewController)
//         }else{
//            swapCurrentViewController(withChildViewController: mainViewController)
//        }
        
        
        if Auth.auth().currentUser == nil {
            switchToStartPage()
            return
        }
        let jsonDecoder = JSONDecoder()
        let userData = defaults.data(forKey: Constants.APPLICATION_USER)
        
        if let mUser = userData{
            print("userData => \(String(data: mUser, encoding: .utf8))")
            let returnedUser = try? jsonDecoder.decode(ApplicationUser.self, from: mUser)
            
            Constants.applicationUser = returnedUser
        }
        //print("Constants.applicationUser => \(Constants.applicationUser)")
        if Constants.applicationUser == nil {
            switchToStartPage()
            return
        }
        
        
        switchToMainView()
       
    }
    
    private lazy var tempStarterController: CheckoutTableViewController = {
           // Load Storyboard
           let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
           
           // Instantiate View Controller
           var viewController = storyboard.instantiateViewController(withIdentifier: "CheckoutTableViewController") as! CheckoutTableViewController
           
           return viewController
       }()
}


