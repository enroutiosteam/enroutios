//
//  AppDelegate.swift
//  Enrout
//
//  Created by Daniel Kwakye on 23/06/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import IQKeyboardManagerSwift
import Firebase
import GoogleSignIn
//import FBSDKCoreKit
import FacebookCore
import IQKeyboardManagerSwift
import UserNotifications
import Siren


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
//    let apiKey: String = "AIzaSyBu5WcHSa0TJiuqCnOTMJuCJy1HW-ZeJpY"
    let apiKey = Constants.API_KEY
     let notificationCenter = UNUserNotificationCenter.current()
    

    override init() {
        super.init()
        UIFont.overrideInitialize()
    }
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        IQKeyboardManager.shared.enable = true
        
        GMSPlacesClient.provideAPIKey(apiKey)
        GMSServices.provideAPIKey(apiKey)
        
        FirebaseApp.configure()
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        
        
        // request authorization to send notifications
       let options: UNAuthorizationOptions = [.alert, .sound, .badge]
       notificationCenter.requestAuthorization(options: options) { (didAllow, error) in
           
           if !didAllow {
               print("User has declined notifications")
           }
       }
       
       // check if notificaiton settings is turned off
       notificationCenter.getNotificationSettings { (settings) in
           if settings.authorizationStatus != .authorized {
               // Notifications not allowed
           }
       }
        
        notificationCenter.delegate = self
    
        
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
////

        Thread.sleep(forTimeInterval: 3.0)
        
        let siren = Siren.shared
//         siren.rulesManager = RulesManager(majorUpdateRules: .critical,
//         minorUpdateRules: .annoying,
//         patchUpdateRules: .default,
//         revisionUpdateRules: Rules(promptFrequency: .daily, forAlertType: .option))
//
        let rules = Rules(promptFrequency: .immediately, forAlertType: .option)
        siren.rulesManager = RulesManager(globalRules: rules)
        siren.wail()
        
        return true
    }
    
    func application(_ app: UIApplication,open url: URL,options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return ApplicationDelegate.shared.application(
            app,
            open: url,
            options: options
        )
    }
    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
//        https://learnappmaking.com/local-notifications-scheduling-swift/
//        for notifications
// // for background
        if var user = Constants.applicationUser {
            print("user id => \(user.id!)")
            user.isOnline = false
            user.device = "IOS"
            updateIsOnline(user: user) { _, _ in
            }
        }
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        //AppEvents.activateApp()
        UIApplication.shared.applicationIconBadgeNumber = 0
        if var user = Constants.applicationUser {
            print("user id => \(user.id!)")
            user.isOnline = true
            user.device = "IOS"
            updateIsOnline(user: user) { _, _ in
            }
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    // excerpt from
    ///https://medium.com/quick-code/local-notifications-with-swift-4-b32e7ad93c2
    
    func scheduleNotification(title: String, content body: String, notificationId: String? = nil, trip: Trip? = nil) {
        
        let content = UNMutableNotificationContent() // Содержимое уведомления
        
        content.title = title
        content.body = body
        content.sound = UNNotificationSound.default
        content.badge = 1
        
    //    let date = Date(timeIntervalSinceNow: 3600)
    //    let triggerDate = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute,.second,], from: date)
    //    let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: false)
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        
        let identifier = "Enrout Notification"
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)

        
        notificationCenter.add(request) { (error) in
            if let error = error {
                print("notificationCenter: Error \(error.localizedDescription)")
                return
            }
            
            
            //print("notificationCenter: notification request added")
            
        }
    }


}

