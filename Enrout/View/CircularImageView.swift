//
//  CircularImageView.swift
//  Enrout
//
//  Created by Jude Botchwey on 07/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit

@IBDesignable
class CircularImageView: UIImageView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override func awakeFromNib() {
           setupView()
       }
       
       
       func setupView(){
           self.layer.cornerRadius = self.frame.width / 2
           self.clipsToBounds = true
       }
       
       
       override func prepareForInterfaceBuilder() {
           super.prepareForInterfaceBuilder()
           setupView()
       }

}
