//
//  Vendor.swift
//  Enrout
//
//  Created by Daniel Kwakye on 07/02/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import Foundation

struct Vendor {
    var vendorId: String?
    var vendorBranchId: String?
    var vendorName: String?
    var vendorLocation: PosLatLng?
    var vendorPhoneNumber: String?
    var products: [Product]?
    var vendorDescription: String?
    var vendorLogo: String?
    var vendorBanner: String?
}

