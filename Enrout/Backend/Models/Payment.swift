//
//  Payment.swift
//  Enrout
//
//  Created by Daniel Expresspay on 02/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Foundation

struct Payment {
    var amount: Double?
    var approvedAt: String?
    var failedAt: Date?
    var failedStep: String?
    var mobileAuthToken: String?
    var momoNumber: String?
    var network: String?
    var orderId: String?
    var status = "PENDING"
    var token: String?
    var senderId: String?
    var senderName: String?
    
}
