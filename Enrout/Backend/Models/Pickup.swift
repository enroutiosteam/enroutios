//
//  Pickup.swift
//  Enrout
//
//  Created by Daniel Expresspay on 07/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Foundation
import Firebase

class Pickup {
    
    var index: Int?
    var isPickedup: Bool?
    var isArrived: Bool?
    var pickedupAt: Any?
    var place: String?
    var description: String?
    var requestId: String?
    var itemCategory: String?
    var instruction: String?
    var pickUpName: String?
    var pickUpPersonPhone: String?
    var isPickedupNotified: Bool?
    var isArrivedNotified: Bool?
    
    var pos: Pos?
    var referenceId: String?
    var documentReference: DocumentReference?
    
    init() {
    }
    
    init(dictionary: [String: Any]){
        self.isPickedup = dictionary["isPickedup"] as? Bool
        self.isArrived = dictionary["isArrived"] as? Bool
        self.pickedupAt = (dictionary["pickedupAt"] as? Timestamp)?.dateValue()
        self.place = dictionary["place"] as? String
        self.description = dictionary["description"] as? String
        self.requestId = dictionary["requestId"] as? String
        self.itemCategory = dictionary["itemCategory"] as? String
        self.instruction = dictionary["instruction"] as? String
        self.pickUpName = dictionary["pickUpName"] as? String
        self.pickUpPersonPhone = dictionary["pickUpPersonPhone"] as? String
        self.isPickedupNotified = dictionary["isPickedupNotified"] as? Bool
        self.isArrivedNotified = dictionary["isArrivedNotified"] as? Bool
        
        let posArr = dictionary["pos"] as? [String:Any]
        let geoHash = posArr?["geohash"] as? String
        let posLatLngArr = posArr?["geopoint"] as? [String:Any]
        var posLatLng = PosLatLng()
        posLatLng.latitude = posLatLngArr?["latitude"] as? Double
        posLatLng.longitude = posLatLngArr?["longitude"] as? Double
        
        var pos = Pos()
        pos.geohash = geoHash
        pos.geopoint = posLatLng
        
        self.pos = pos
        self.referenceId = dictionary["referenceId"] as? String
        
    }
    
}

