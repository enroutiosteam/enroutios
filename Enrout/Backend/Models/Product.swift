//
//  Product.swift
//  Enrout
//
//  Created by Daniel Kwakye on 05/02/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import Foundation

struct Product {
    var productId: String?
    var productName: String?
    var productTitle: String?
    var productImage: String?
    var productDescription: String?
    var productPrice: Double?
    var deliveryPrice: Double?
}
