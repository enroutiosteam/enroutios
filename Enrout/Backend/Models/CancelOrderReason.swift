//
//  CancelOrderReason.swift
//  Enrout
//
//  Created by Daniel Expresspay on 09/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Foundation

struct CancelOrderReason {
    var text: String?
    
    static func getReasons() -> [CancelOrderReason]{
        return [
            CancelOrderReason(text: "I found a different transport option"),
            CancelOrderReason(text: ""),
            CancelOrderReason(text: ""),
            CancelOrderReason(text: ""),
            CancelOrderReason(text: ""),
            CancelOrderReason(text: ""),
            CancelOrderReason(text: ""),
            CancelOrderReason(text: ""),
        ]
    }
}

