//
//  Destination.swift
//  Enrout
//
//  Created by Daniel Expresspay on 07/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Foundation
import Firebase

class Destination {
    
    var index: Int?
    var deliveredAt: Any?
    var isDelivered: Bool?
    var description: String?
    var place: String?
    var pos: Pos?
    var recipientName: String?
    var recipientNumber: String?
    var referenceId: String?
    var requestId: String?
    var isArrived: Bool?
    var isArrivedNotified: Bool?
    var isDeliveredNotified: Bool?
    
    var documentReference: DocumentReference?
    
    init() {
    }
    
    init(dictionary: [String: Any]){
        self.deliveredAt = (dictionary["deliveredAt"] as? Timestamp)?.dateValue()
        self.isDelivered = dictionary["isDelivered"] as? Bool
        self.description = dictionary["description"] as? String
        self.place = dictionary["place"] as? String
        
        let posArr = dictionary["pos"] as? [String:Any]
        let geoHash = posArr?["geohash"] as? String
        let posLatLngArr = posArr?["geopoint"] as? [String:Any]
        var posLatLng = PosLatLng()
        posLatLng.latitude = posLatLngArr?["latitude"] as? Double
        posLatLng.longitude = posLatLngArr?["longitude"] as? Double
        
        var pos = Pos()
        pos.geohash = geoHash
        pos.geopoint = posLatLng
        
        self.pos = pos
        self.recipientName = dictionary["recipientName"] as? String
        self.recipientNumber = dictionary["recipientNumber"] as? String
        self.referenceId = dictionary["referenceId"] as? String
        self.requestId = dictionary["requestId"] as? String
        
        self.isArrived = dictionary["isArrived"] as? Bool
        self.isArrivedNotified = dictionary["isArrivedNotified"] as? Bool
        self.isDeliveredNotified = dictionary["isDeliveredNotified"] as? Bool
    }
    
}
