//
//  Pos.swift
//  Enrout
//
//  Created by Daniel Expresspay on 07/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Foundation

struct Pos: Decodable {
    var geopoint: PosLatLng?
    var geohash: String?
}

struct PosLatLng: Decodable, Encodable {
    var longitude: Double?
    var latitude: Double?
    var address: String?
    
    init(latitude: Double, longitude: Double) {
        self.latitude = latitude
        self.longitude = longitude
    }
    
    init() {}
}
