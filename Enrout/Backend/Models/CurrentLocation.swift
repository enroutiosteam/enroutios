//
//  CurrentLocation.swift
//  Enrout
//
//  Created by Daniel Expresspay on 07/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Foundation

struct CurrentLocation: Decodable {
    var latitude: Double?
    var longitude: Double?
}
