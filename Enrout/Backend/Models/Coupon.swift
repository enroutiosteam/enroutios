//
//  Coupon.swift
//  Enrout
//
//  Created by Daniel Expresspay on 07/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Foundation

struct Coupon: Decodable {
    var docId: String?
    var code: String?
    var individualUserLimit: Int?
    var limit: Int?
    var name: String?
    var totalUsed: Int?
    var usersAllowed: Int?
    var value: Double?
    var status: String?
    
    var entriesList = [Entry]()
}

class Entry: Decodable {
    var usage: Int?
    var userId: String?
    
    init(usage: Int?, userId: String?) {
        self.usage = usage
        self.userId = userId
    }
    
    init() {
        
    }
}
