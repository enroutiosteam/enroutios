//
//  Trip.swift
//  Enrout
//
//  Created by Daniel Expresspay on 07/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Foundation
import FirebaseAuth
import Firebase

class Trip {
    
    var actualCost: Double?
    var arrivedAt: Any?
    var estimatedCost: Double?
    var isAccepted: Bool?
    var isArrived: Bool?
    var paymentMethod: String?
    var requestDatetime: Any?
    var requestId: String?
    var riderId: String?
    var riderName: String?
    var riderPhoneNumber: String?
    var riderPlateNumber: String?
    var senderId: String?
    var senderName: String?
    var senderPhoneNumber: String?
    var status: String? = TripStatus.PENDING.rawValue
    
    var pickups: [DocumentReference]?
    var destinations: [DocumentReference]?
    
    var pickupsRef: [String]?
    var destinationsRef: [String]?
    
    var pickupNames: [String]?
    var destinationNames: [String]?
    
    var selectedItemCategories: [String]?
    var selectedItemInstructions: [String]?
    
    var estimatedTimeInSeconds: Int? = 0
    var estimatedDistanceInMeters: Double? = 0
    var estimatedTimeInText: String?
    var estimatedDistanceInText: String?
    var payForMe:Double? = 0.0
    var hasPaid: Bool?
    var typeCode: String? = TripTypeCode.SPSD.rawValue
    
    var couponApplied: Bool? = false
    var couponName: String?
    var couponValueInPercentage: Double?
    var coupon: DocumentReference?
    var couponPreviousPrice: Double?
    
    var riderImage: String?
    var orderNumber: String?
    var userCurrentLng: Double?
    var userCurrentLat: Double?
    
    var pos: Pos?
    
    var currentLocation: CurrentLocation?
    var isPayForMe: Bool?
    var fleetId: String?
    var scheduleId: String?
    var taskCount: Int?
    var taskCompleted: Int?
    var rated: Bool?
    var rating: Double?
    var device: String?
    
    var customerType = "IOS"
    
    var useBox: Bool?
    
    var isPickFromVendor: Bool = false
    
    var pointToBeginSearch: PosLatLng?
    
    var tripStatusNotified: TripStatusNotified?
    
    init() {
        
    }
    
    init(dictionary: [String: Any]) {
        
        self.actualCost = dictionary["actualCost"] as? Double
        self.arrivedAt = dictionary["arrivedAt"] as? Date
        self.estimatedCost = dictionary["estimatedCost"] as? Double
        self.isAccepted  = dictionary["isAccepted"] as? Bool
        self.isArrived = dictionary["isArrived"] as? Bool
        self.paymentMethod = dictionary["paymentMethod"] as? String
        self.requestDatetime = (dictionary["requestDatetime"] as? Timestamp)?.dateValue()
        self.requestId = dictionary["requestId"] as? String
        self.riderId = dictionary["riderId"] as? String
        self.riderName = dictionary["riderName"] as? String
        self.riderPhoneNumber = dictionary["riderPhoneNumber"] as? String
        self.riderPlateNumber = dictionary["riderPlateNumber"] as? String
        self.senderId = dictionary["senderId"] as? String
        self.senderName = dictionary["senderName"] as? String
        self.senderPhoneNumber = dictionary["senderPhoneNumber"] as? String
        self.status = dictionary["status"] as? String
        
        self.pickupsRef = dictionary["pickupsRef"] as? [String]
        self.destinationsRef = dictionary["destinationsRef"] as? [String]
        
        self.pickupNames = dictionary["pickupNames"] as? [String]
        self.destinationNames = dictionary["destinationNames"] as? [String]
        
        self.selectedItemCategories = dictionary["selectedItemCategories"] as? [String]
        self.selectedItemInstructions = dictionary["selectedItemInstructions"] as? [String]
        
        self.estimatedTimeInSeconds = dictionary["estimatedTimeInSeconds"] as? Int
        self.estimatedDistanceInMeters = dictionary["estimatedDistanceInMeters"] as? Double
        self.estimatedTimeInText = dictionary["estimatedTimeInText"] as? String
        self.estimatedDistanceInText = dictionary["estimatedDistanceInText"] as? String
        
        self.payForMe = dictionary["payForMe"] as? Double
        self.hasPaid = dictionary["hasPaid"] as? Bool
        self.typeCode = dictionary["typeCode"] as? String
        
        self.isPickFromVendor = dictionary["isPickFromVendor"] as? Bool ?? false
        
        self.couponApplied = dictionary["couponApplied"] as? Bool
        self.couponName = dictionary["couponName"] as? String
        self.couponValueInPercentage = dictionary["couponValueInPercentage"] as? Double
        self.coupon = dictionary["coupon"] as? DocumentReference
        self.couponPreviousPrice = dictionary["couponPreviousPrice"] as? Double
        
        self.customerType = dictionary["customerType"] as? String ?? "IOS"
        
        self.riderImage = dictionary["riderImage"] as? String
        self.orderNumber = dictionary["orderNumber"] as? String
        self.userCurrentLng = dictionary["userCurrentLng"] as? Double
        self.userCurrentLat = dictionary["userCurrentLat"] as? Double
        
        let posArr = dictionary["pos"] as? [String:Any]
        let geoHash = posArr?["geohash"] as? String
        
        let posLatLngArr = posArr?["geopoint"] as? [String:Any]
        var posLatLng = PosLatLng()
        posLatLng.latitude = posLatLngArr?["latitude"] as? Double
        posLatLng.longitude = posLatLngArr?["logitude"] as? Double
        
        var pos = Pos()
        pos.geohash = geoHash
        pos.geopoint = posLatLng
        
        self.pos = pos
        
        let currentLocArr = dictionary["currentLocation"] as? [String:Any]
        let curLat = currentLocArr?["latitude"] as? Double
        let curLng = currentLocArr?["longitude"] as? Double
        
        var currentLocation = CurrentLocation()
        currentLocation.latitude = curLat
        currentLocation.longitude = curLng
        
        self.currentLocation = currentLocation
        
        
        self.isPayForMe = dictionary["isPayForMe"] as? Bool
        self.fleetId = dictionary["fleetId"] as? String
        self.scheduleId = dictionary["scheduleId"] as? String
        self.taskCount = dictionary["taskCount"] as? Int
        self.taskCompleted = dictionary["taskCompleted"] as? Int
        self.rated = dictionary["rated"] as? Bool
        self.rating = dictionary["rating"] as? Double
        self.device = dictionary["device"] as? String
        
        self.useBox = dictionary["useBox"] as? Bool
        
        let pointToBeginSearchArr = dictionary["pointToBeginSearch"] as? [String:Any]
       var pointToBeginSearch = PosLatLng()
       pointToBeginSearch.latitude = pointToBeginSearchArr?["latitude"] as? Double
       pointToBeginSearch.longitude = pointToBeginSearchArr?["longitude"] as? Double
       self.pointToBeginSearch = pointToBeginSearch
        
        self.tripStatusNotified = TripStatusNotified(dict: dictionary["tripStatusNotified"] as! [String:Bool])
    }
    
    static func createDumyTrip() -> (trip:Trip, pickups: [Pickup], destinations: [Destination]){
        
        let trip = Trip()
        let db = Firestore.firestore()
        let docRef = db.collection(Collections.REQUEST.rawValue).document(String(describing: UUID()))
        
        var pickups = [Pickup]()
        let pickup1 = Pickup()
        pickup1.instruction = "pickup 1 instuction"
        pickup1.isArrived = false
        pickup1.isArrivedNotified = false
        pickup1.isPickedup = false
        pickup1.isPickedupNotified = false
        pickup1.itemCategory = "Book"
        pickup1.pickedupAt = nil
        pickup1.pickUpName = "Yaa Daniels"
        pickup1.pickUpPersonPhone = "233551111121"
        pickup1.place = "Osu"

        var ppPos = Pos()
        ppPos.geohash = Constants.getGeoHash(latitude: 5.556345, longitude: -0.1835434)
        ppPos.geopoint = PosLatLng(latitude: 5.556345, longitude: -0.1835434)
        
        pickup1.pos = ppPos
        pickups.append(pickup1)
        
        // pick up 2
        
        let pickup2 = Pickup()
        pickup2.instruction = "pickup 2 instuction"
        pickup2.isArrived = false
        pickup2.isArrivedNotified = false
        pickup2.isPickedup = false
        pickup2.isPickedupNotified = false
        pickup2.itemCategory = "Book"
        pickup2.pickedupAt = nil
        pickup2.pickUpName = "Kofi Fak ya"
        pickup2.pickUpPersonPhone = "233551111121"
        pickup2.place = "Ashomang Estage"

        var ppPos2 = Pos()
        ppPos2.geohash = Constants.getGeoHash(latitude: 5.7058393, longitude: -0.2407122)
        ppPos2.geopoint = PosLatLng(latitude: 5.7058393, longitude: -0.2407122)
        
        pickup2.pos = ppPos2
        pickups.append(pickup2)
        
        // pick up 3
        
        let pickup3 = Pickup()
        pickup3.instruction = "pickup 3 instuction"
        pickup3.isArrived = false
        pickup3.isArrivedNotified = false
        pickup3.isPickedup = false
        pickup3.isPickedupNotified = false
        pickup3.itemCategory = "Book"
        pickup3.pickedupAt = nil
        pickup3.pickUpName = "Kofi Fak ya"
        pickup3.pickUpPersonPhone = "233551111121"
        pickup3.place = "Lakeside Polistation"

        var ppPos3 = Pos()
        ppPos3.geohash = Constants.getGeoHash(latitude: 5.6974829, longitude: -0.1292818)
        ppPos3.geopoint = PosLatLng(latitude: 5.6974829, longitude: -0.1292818)
        
        pickup3.pos = ppPos3
        pickups.append(pickup3)
        
//        let pickupRequestId = String(describing: UUID())
//        pickup1.requestId = pickupRequestId
   
        
        var destinations = [Destination]()
        let destination = Destination()
        destination.deliveredAt = Date()
        destination.isArrived = false
        destination.isArrivedNotified = false
        destination.isDelivered = false
        destination.isDeliveredNotified = false
        destination.place = "OSU"
        destination.recipientName = "Jude Unknown"
        destination.recipientNumber = "233544433321"
    
        var destPos = Pos()
        destPos.geohash = Constants.getGeoHash(latitude: 5.6376284, longitude: -0.243358)
        destPos.geopoint = PosLatLng(latitude: 5.6376284, longitude: -0.243358)
        destination.pos = destPos
        
        // add point to destinations
        destinations.append(destination)
        
        trip.actualCost = 0.0
        trip.arrivedAt = Date()
        trip.estimatedCost = 2
        trip.isAccepted  = false
        trip.isArrived = false
        trip.paymentMethod = "Cash"
        trip.requestDatetime = Date()
        trip.requestId = docRef.documentID
        trip.riderId = nil
        trip.riderName = nil
        trip.riderPhoneNumber = nil
        trip.riderPlateNumber = nil
        trip.senderId = Auth.auth().currentUser?.uid
        trip.senderName = "Dummy IOS Customer"
        trip.senderPhoneNumber = "233541111111"
        trip.status = TripStatus.PENDING.rawValue
    
        
        // loop tru pickups
   
        var pickupNames = [String?]()
        var pickUpDocRefs = [DocumentReference?]()
        var stringPickupRef = [String?]()
        
        var selectedItemCategories = [String?]()
        var selectedInstructions = [String?]()
        
        var destinationNames = [String?]()
        var stringDestRef = [String?]()
        
        var destDocRefs = [DocumentReference?]()
        
        
        for p in pickups {
            
            let pRef =  db.collection(Collections.PICKUPPOINTS.rawValue).document(String(describing: UUID()))
            pickupNames.append(p.place)
            selectedItemCategories.append(p.itemCategory)
            selectedInstructions.append(p.instruction)
            
            p.documentReference = pRef
            p.referenceId = pRef.documentID
            p.requestId = trip.requestId
    
            pickUpDocRefs.append(pRef)
            stringPickupRef.append(pRef.documentID)
            
            //pickupRefs.append(p)
        }
        
        // loop tru destinations
        for d in destinations {
            let dRef = db.collection(Collections.DESTINATIONS.rawValue).document(String(describing: UUID()))
            destinationNames.append(d.place)
            d.documentReference = dRef
            d.referenceId = dRef.documentID
            d.requestId = trip.requestId
            destDocRefs.append(dRef)
            stringDestRef.append(dRef.documentID)
        }
        
        trip.pickupsRef = stringPickupRef as? [String]
        trip.destinationsRef = stringDestRef as? [String]
//
        trip.pickupNames = pickupNames as? [String]
        trip.destinationNames = destinationNames as? [String]
//
        trip.selectedItemCategories = selectedItemCategories as? [String]
        trip.selectedItemInstructions = selectedInstructions as? [String]
//
        trip.estimatedTimeInSeconds = 600
        trip.estimatedDistanceInMeters = 500
        trip.payForMe = 0.0
        trip.hasPaid = false
        trip.typeCode = TripTypeCode.MP.rawValue

        trip.couponApplied = false
        trip.couponName = nil
        trip.couponValueInPercentage = 0.0

        trip.riderImage = nil
        trip.orderNumber = nil
        trip.userCurrentLng = 5.6122732
        trip.userCurrentLat = -0.1793181
        

        var pos = Pos()
        pos.geohash = Constants.getGeoHash(latitude: trip.userCurrentLat!, longitude: trip.userCurrentLng!)
        pos.geopoint = PosLatLng(latitude: trip.userCurrentLat!, longitude: trip.userCurrentLng!)
        trip.pos = pos
        
        var currentLocation = CurrentLocation()
        currentLocation.latitude = trip.userCurrentLat
        currentLocation.longitude = trip.userCurrentLng
//
        trip.currentLocation = currentLocation
        trip.isPayForMe = false
        trip.fleetId = nil
        trip.scheduleId = nil
        
        trip.taskCount = pickUpDocRefs.count + stringDestRef.count
        trip.taskCompleted = 0
        trip.rated = false
        trip.rating = 0.0
        trip.device = "IOS"
        trip.requestId = "E23528FB-4639-4F49-A7E7-F1062BAC2074"
        
        return (trip, pickups, destinations)
        
    }
    
   static func convertTripToData(trip: Trip) -> [String:Any] {
    
    var dictionary: [String: Any] = [:]
    
    dictionary["actualCost"] = trip.actualCost
    dictionary["arrivedAt"] = trip.arrivedAt
    dictionary["estimatedCost"] = trip.estimatedCost
    dictionary["isAccepted"] = trip.isAccepted
    dictionary["isArrived"] = trip.isArrived
    dictionary["paymentMethod"] = trip.paymentMethod
    dictionary["requestDatetime"] = trip.requestDatetime
    dictionary["requestId"] = trip.requestId
    dictionary["riderId"] = trip.riderId
    dictionary["riderName"] = trip.riderName
    dictionary["riderPhoneNumber"] = trip.riderPhoneNumber
    dictionary["riderPlateNumber"] = trip.riderPlateNumber
    dictionary["senderId"] = trip.senderId
    dictionary["senderName"] = trip.senderName
    dictionary["senderPhoneNumber"] = trip.senderPhoneNumber
    dictionary["status"] = trip.status
    dictionary["pickupsRef"] = trip.pickupsRef
    dictionary["destinationsRef"] = trip.destinationsRef
    dictionary["pickups"] = trip.pickups
    dictionary["destinations"] = trip.destinations
    print("number of detinatons \(trip.destinations?.count)")
    dictionary["pickupNames"] = trip.pickupNames
    dictionary["destinationNames"] = trip.destinationNames
    dictionary["selectedItemCategories"] = trip.selectedItemCategories
    dictionary["selectedItemInstructions"] =  trip.selectedItemInstructions
    dictionary["estimatedTimeInSeconds"] = trip.estimatedTimeInSeconds
    dictionary["estimatedDistanceInMeters"] = trip.estimatedDistanceInMeters
    dictionary["estimatedDistanceInText"] = trip.estimatedDistanceInText
    dictionary["estimatedTimeInText"] = trip.estimatedTimeInText
    dictionary["isPickFromVendor"] = trip.isPickFromVendor
    dictionary["payForMe"] = trip.payForMe
    dictionary["hasPaid"] = trip.hasPaid
    dictionary["typeCode"] = trip.typeCode
    dictionary["couponApplied"] = trip.couponApplied
    dictionary["couponName"] = trip.couponName
    dictionary["couponValueInPercentage"] = trip.couponValueInPercentage
    dictionary["coupon"] = trip.coupon
    dictionary["couponPreviousPrice"] = trip.couponPreviousPrice
    dictionary["riderImage"] = trip.riderImage
    dictionary["orderNumber"] = trip.orderNumber
    dictionary["userCurrentLng"] = trip.userCurrentLng
    dictionary["userCurrentLat"] = trip.userCurrentLat
    dictionary["customerType"] = trip.customerType
    
    dictionary["useBox"] = trip.useBox
    
    dictionary["pos"] = [
        "geohash" : trip.pos!.geohash!,
        "geopoint" : [
            "latitude" : trip.pos?.geopoint?.latitude,
            "longitude" : trip.pos?.geopoint?.longitude
        ]
    ]
    
    dictionary["currentLocation"] = [
        "latitude" : trip.currentLocation!.latitude,
        "longitude" : trip.currentLocation!.longitude
    ]
    
    dictionary["isPayForMe"] = trip.isPayForMe
    dictionary["fleetId"] = trip.fleetId
    dictionary["scheduleId"] = trip.scheduleId
    dictionary["taskCount"] = trip.taskCount
    dictionary["taskCompleted"]  = trip.taskCompleted
    dictionary["rated"] = trip.rated
    dictionary["rating"] = trip.rating
    dictionary["device"] = trip.device
    
    dictionary["tripStatusNotified"] = [
        "PENDING": trip.tripStatusNotified?.PENDING,
        "PROCESSING" : trip.tripStatusNotified?.PROCESSING,
        "ONGOING" : trip.tripStatusNotified?.ONGOING,
        "NOT_FOUND" : trip.tripStatusNotified?.NOT_FOUND,
        "DELAYED" : trip.tripStatusNotified?.DELAYED,
        "CANCELLED" : trip.tripStatusNotified?.CANCELLED,
        "CANCELLED_R" : trip.tripStatusNotified?.CANCELLED_R,
        "COMPLETED" : trip.tripStatusNotified?.COMPLETED
    ]
    
    dictionary["pointToBeginSearch"] = [
        "latitude" : trip.pointToBeginSearch!.latitude,
        "longitude" : trip.pointToBeginSearch!.longitude
    ]
   
       return dictionary
    }
    
    
}
