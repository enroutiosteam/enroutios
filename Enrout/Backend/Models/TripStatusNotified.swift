//
//  TripStatusNotified.swift
//  Enrout
//
//  Created by Daniel Kwakye on 22/01/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import Foundation

struct TripStatusNotified {
    var PROCESSING: Bool = false
    var PENDING:  Bool = false
    var ONGOING : Bool = false
    var NOT_FOUND: Bool = false
    var DELAYED: Bool = false
    var CANCELLED: Bool = false
    var CANCELLED_R: Bool = false
    var COMPLETED: Bool = false
    
    init() {
    }
    
    init(dict: [String:Bool]) {
        self.PROCESSING = dict["PROCESSING"]!
        self.PENDING = dict["PENDING"]!
        self.ONGOING = dict["ONGOING"]!
        self.NOT_FOUND = dict["NOT_FOUND"]!
        self.DELAYED = dict["DELAYED"]!
        self.CANCELLED = dict["CANCELLED"]!
        self.CANCELLED_R = dict["CANCELLED_R"]!
        self.COMPLETED = dict["COMPLETED"]!
    }
    
    func convertToDate() -> [String : Bool] {
        return [
            "PROCESSING" : self.PROCESSING,
            "PENDING" : self.PENDING,
            "ONGOING" : self.ONGOING,
            "NOT_FOUND" : self.NOT_FOUND,
            "DELAYED" : self.DELAYED,
            "CANCELLED" : self.CANCELLED,
            "CANCELLED_R" : self.CANCELLED_R,
            "COMPLETED" : self.COMPLETED
        ]
    
    }
}
