//
//  User.swift
//  NikasemoIOS
//
//  Created by Daniel Kwakye on 09/01/2019.
//  Copyright © 2019 Benard Matic Lomo. All rights reserved.
//

import Foundation

struct ApplicationUser {

    var id: String?
    var fullName: String?  // always available
    var email: String? // available for only users with email and password authentication
    var phoneNumber: String? // only available for users with phone verification
    var authType: String? // phoneAuth, EmailPassword, OnlyEmail, Facebook, Google
  
    var userType: String?
    var address: String?
    var dateAdded: Any?
    var lastLogin: Any?
    var gender: String?
    var photoUrl: String?
    var currentBalance: Double = 0.0
    var outStandingBalance: Double = 0.0
    var isOnline: Bool?
    var device = "IOS"
    var appReviewed = false
    
    enum CodingKeys: String, CodingKey {
        case id
        case fullName
        case email
        case phoneNumber
        case gender
        case photoUrl
        case device
        case appReviewed
    }
    
}

extension ApplicationUser: Encodable{
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(fullName ?? "", forKey: .fullName)
        try container.encode(phoneNumber ?? "", forKey: .phoneNumber)
        try container.encode(gender ?? "", forKey: .gender)
        try container.encode(photoUrl ?? "", forKey: .photoUrl)
        try container.encode(email ?? "", forKey: .email)
        try container.encode(device, forKey: .device)
        try container.encode(appReviewed, forKey: .appReviewed)
    }
}

extension ApplicationUser: Decodable{
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        fullName = try container.decode(String.self, forKey: .fullName)
        phoneNumber = try container.decode(String.self, forKey: .phoneNumber)
        gender = try container.decode(String.self, forKey: .gender)
        email = try container.decode(String.self, forKey: .email)
        device = try container.decode(String.self, forKey: .device)
        appReviewed = try container.decode(Bool.self, forKey: .appReviewed)
        photoUrl = try container.decode(String.self, forKey: .photoUrl)
        
    }
    
}
