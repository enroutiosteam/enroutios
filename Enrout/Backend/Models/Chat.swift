//
//  Chat.swift
//  Enrout
//
//  Created by Daniel Kwakye on 18/01/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth

class Chat {
    var messageText: String?
    var riderId: String?
    var riderName: String?
    var riderPhone: String?
    var customerId: String?
    var createdTimestamp: Date?
    var timeStampAsLong: Int?
    var sentBy: String? = "customer"
    var seen: Bool? = false // prevent the notification, every time chat is opened\
    
    init() {
    }
    
    init(messageText: String, riderId: String, riderName: String, riderPhone: String){
        self.messageText = messageText
        self.riderId = riderId
        self.riderName = riderName
        self.riderPhone = riderPhone
        self.customerId = Auth.auth().currentUser!.uid
        self.createdTimestamp = Date()
        self.sentBy = "customer"
        self.seen = false
    }
    
    init(dictionary: [String: Any]) {
         self.messageText = dictionary["messageText"] as? String
         self.riderId = dictionary["riderId"] as? String
         self.riderName = dictionary["riderName"] as? String
         self.riderPhone = dictionary["riderPhone"] as? String
         self.customerId = dictionary["customerId"] as? String
         self.timeStampAsLong   = dictionary["createdTimestamp"] as! Int
        self.createdTimestamp = intToDate(myInt: self.timeStampAsLong!)
         self.sentBy = dictionary["sentBy"] as? String
         self.seen = dictionary["seen"] as? Bool
    }
    
    func convertToDictionary(chat: Chat) -> [String: Any?]{
        return [
            "messageText": chat.messageText,
            "riderId": chat.riderId,
            "riderName": chat.riderName,
            "riderPhone": chat.riderPhone,
            "customerId": chat.customerId,
            "createdTimestamp": dateToInt(date: Date()),
            "sentBy": chat.sentBy,
            "seen": chat.seen,
        ]
    }
    
    func dateToInt(date: Date) -> Int{
        // using current date and time as an example
        let someDate = Date()

        // convert Date to TimeInterval (typealias for Double)
        let timeInterval = someDate.timeIntervalSince1970

        // convert to Integer
        return Int(timeInterval)
    }
    
    func intToDate(myInt: Int) -> Date{
        // convert Int to Double
        let timeInterval = Double(myInt)

        // create NSDate from Double (NSTimeInterval)
        let nsDate = Date(timeIntervalSince1970: timeInterval)
        
        return nsDate
        
    }
}
