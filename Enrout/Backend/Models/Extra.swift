//
//  Distance.swift
//  Enrout
//
//  Created by Daniel Expresspay on 29/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Foundation

struct Distance {
    var text: String?
    var value: Double?
}

struct Duration {
    var text: String?
    var value: Double?
}
