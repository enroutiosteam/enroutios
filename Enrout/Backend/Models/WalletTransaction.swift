//
//  WalletTransaction.swift
//  Enrout
//
//  Created by Daniel Expresspay on 02/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Foundation
import FirebaseFirestore

struct WalletTransaction {
    
    var amount: Double?
    var createdAt: Any?
    var narration: String?
    var network: String?
    var accountNumber: String?
    var senderId: String?
    
    init(dictionary: [String: Any]) {
        self.amount = dictionary["amount"] as? Double
        self.createdAt = (dictionary["createdAt"] as? Timestamp)?.dateValue()
        self.narration = dictionary["narration"] as? String
        self.network = dictionary["network"] as? String
        self.accountNumber = dictionary["accountNumber"] as? String
        self.senderId = dictionary["senderId"] as? String
    }
    
}
