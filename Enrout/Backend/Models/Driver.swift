//
//  Driver.swift
//  Enrout
//
//  Created by Daniel Expresspay on 08/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

struct Driver {
    var sumOfVotes: Double?
    var totalNoOfVotes: Double?
    var averageRating: Double?
    
    init(dictionary: [String:Any]) {
        self.sumOfVotes = dictionary["sumOfVotes"] as? Double
        self.totalNoOfVotes = dictionary["totalNoOfVotes"] as? Double
        self.averageRating = dictionary["averageRating"] as? Double
    }
}
