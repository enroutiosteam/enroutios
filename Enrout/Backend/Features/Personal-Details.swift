//
//  Personal-Details.swift
//  Enrout
//
//  Created by Daniel Expresspay on 06/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Foundation
import Firebase
import UIKit
import FirebaseStorage


func updateUserDetails(viewController: UIViewController, user: ApplicationUser, imageData: Data?, completed: @escaping (_ status: Bool) -> Void) {
    let db = Firestore.firestore()
    
    var data = [AnyHashable : Any]()
    data["email"] = user.email
    data["fullName"] = user.fullName
    data["phoneNumber"] = user.phoneNumber
    data["gender"] = user.gender
    let uid = Auth.auth().currentUser!.uid
    print("uid => \(uid)")
    let dialog = AnyProgressDialog.newInstance(view: viewController.view).show()
    db.collection(Collections.USERS.rawValue).document(uid)
        .updateData(data, completion: {
            error in
            
            if let err = error {
                dialog.dismis()
                print("err => \(err.localizedDescription)")
                Toast.error(view: viewController.view, message: "check your connection and try again")
                completed(false)
                return
            }
            
            if Constants.applicationUser == nil{
                Constants.applicationUser = ApplicationUser()
            }
            
            Constants.applicationUser?.id = Auth.auth().currentUser?.uid
            Constants.applicationUser?.phoneNumber = user.phoneNumber
            Constants.applicationUser?.fullName = user.fullName
            Constants.applicationUser?.gender = user.gender
            // update name store in userdefault
            Constants.applicationUser?.email = user.email ?? ""
            let encodedUser = try? JSONEncoder().encode(Constants.applicationUser)
            UserDefaults.standard.set(encodedUser, forKey: Constants.APPLICATION_USER)
            
            
            dialog.dismis()
             Toast.success(view: viewController.view, message: "Personal details saved")
            guard let data = imageData else{
                completed(true)
                print("personal details saved")
                return
            }
            
            // update the photoUrl
            uploadPic(viewController: viewController, data: data, completed: completed)
            
        })
}

private func uploadPic(viewController: UIViewController,  data: Data, completed: @escaping (_ status: Bool) -> Void){
    let storage = Storage.storage()
    let storageRef = storage.reference().child("customerImages/\(Auth.auth().currentUser!.uid)")
    

    let dialog = AnyProgressDialog.newInstance(view: viewController.view).show()
    
    // Create file metadata including the content type
    let metadata = StorageMetadata()
    metadata.contentType = "image/jpeg"

    
    // Upload the file to the path "images/rivers.jpg"
    storageRef.putData(data, metadata: metadata) { (metadata, error) in
        if let err = error {
            dialog.dismis()
            print("err => \(err.localizedDescription)")
            Toast.error(view: viewController.view, message: err.localizedDescription)
            completed(false)
            return
        }
        

//      // Metadata contains file metadata such as size, content-type.
//      let size = metadata.size
      // You can also access to download URL after upload.
      storageRef.downloadURL { (url, error) in
        
        if let err = error {
            print("err => \(err.localizedDescription)")
            Toast.error(view: viewController.view, message: err.localizedDescription)
            completed(false)
            return
        }
        
        guard let downloadURL = url else {
          // Uh-oh, an error occurred!
             dialog.dismis()
            completed(false)
          return
        }
        
        let db = Firestore.firestore()
        let data = ["photoUrl" : String(describing: downloadURL)]
        db.collection(Collections.USERS.rawValue).document(Auth.auth().currentUser!.uid).updateData(data, completion:  {
            err in
            dialog.dismis()
            Toast.success(view: viewController.view, message: "Profile picture saved")
            print("download url => \(downloadURL)")
            completed(true)
        })
        

      }
    }


    
}

func getUserDetails(competion: @escaping (_ user: ApplicationUser? ,_ error: Error?) -> Void) {
    
    getUser(userId: Auth.auth().currentUser!.uid) { (status, user) in
        if !status {
            competion(nil, CustomError(localizedDescription: "Check connection and try again"))
            return
        }
        
        competion(user, nil)
        // fetch image and attach to imageview
    }
}

