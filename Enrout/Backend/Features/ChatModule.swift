//
//  File.swift
//  Enrout
//
//  Created by Daniel Kwakye on 16/01/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//
import Firebase
import Foundation

func fetchMessages(riderId: String, completion: @escaping (_ chats: [Chat]?) -> Void) {
    let ref = Database.database().reference(withPath: "messages/"+Auth.auth().currentUser!.uid + riderId)
    
    ref.observeSingleEvent(of: .value) { (snapshots) in
        let dicts = snapshots.value as? [String : [String: Any]] ?? [:]
        let chats = dicts.compactMap { (key, value:  [String: Any]) in
           return Chat(dictionary: value)
        }
        
        completion(chats)
    }
    
    
    
}

func observeMessages(riderId: String, riderName: String, riderPhone: String, completion: @escaping (_ chats: [Chat]?) -> Void){
    
    let ref = Database.database().reference(withPath: "messages/"+Auth.auth().currentUser!.uid + riderId).queryOrdered(byChild: "createdTimestamp")
    
    ref.observe(.value) { (snapshots) in
    
//        let dicts = snapshots.value as? [String : [String: Any]] ?? [:]
        var mChats: [Chat] = []

        var lastChat = Chat()
        lastChat.timeStampAsLong = 0
        var lastChidNode: DatabaseReference? = nil
        
        
        for (index, postsnapshot) in (snapshots.children.allObjects as! [DataSnapshot]).enumerated() {
            
            let value = postsnapshot.value as! [String: Any]
            
            let chat = Chat(dictionary: value)
            mChats.append(chat)
            print("inital index \(index) => message => \(chat.messageText!)")
            
            if chat.timeStampAsLong! > lastChat.timeStampAsLong! {
                lastChidNode = postsnapshot.ref
                lastChat = chat
            }
             
        }
        let lastSeen: Bool = lastChat.seen == nil || !lastChat.seen!
        if "rider" == lastChat.sentBy! &&  lastSeen{
            // showNotification
            scheduleNotification(title: riderName, content: lastChat.messageText!)
            
            if lastChidNode != nil {
                lastChat.seen = true
                lastChat.riderName = riderName
                lastChat.riderPhone = riderPhone
                lastChidNode!.setValue(lastChat.convertToDictionary(chat: lastChat))
            }
            
            print("message from rider notified")
        }
        
//        for value in dicts.values {
//
//        }
        
        
        
        completion(mChats)
    }
}


func sendMessage(chat: Chat, completion: @escaping (_ error: Error?) -> Void){
    let ref = Database.database().reference(withPath: "messages/"+Auth.auth().currentUser!.uid + chat.riderId!)
    
    let data = chat.convertToDictionary(chat: chat)
    ref.childByAutoId().setValue(data) {
        (error:Error?, ref:DatabaseReference) in
        completion(error)
    }
}
