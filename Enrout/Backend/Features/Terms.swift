//
//  Terms.swift
//  Enrout
//
//  Created by Daniel Kwakye on 29/01/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//


import Foundation
import UIKit
import Firebase

func openTerms(viewcontroller: UIViewController){
   let dialog = AnyProgressDialog.newInstance(view: viewcontroller.view).show()
    let db = Firestore.firestore()
    
    db.collection(Collections.SETTINGS.rawValue).getDocuments { (query, error) in
        if let err = error{
            dialog.dismis()
            Toast.error(view: viewcontroller.view, message: err.localizedDescription)
            return
        }
        
        let data = query!.documents[0].data()
        
        let faq = data["terms_and_conditions"] as! String
        guard let url = URL(string: faq) else {
            dialog.dismis()
            Toast.error(view: viewcontroller.view, message: "Please try again")
            return
        }
        dialog.dismis()
        UIApplication.shared.open(url)

    }
    
}
