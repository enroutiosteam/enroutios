//
//  TripRequest.swift
//  Enrout
//
//  Created by Daniel Expresspay on 06/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit
import Firebase
import Alamofire
import SwiftyJSON
import GooglePlaces
import CoreLocation

// search google places on text changed
func searchGooglePlaces(query:String, completion: @escaping (_ placeCompleteList: [PlaceComplete]?)-> Void){
     let placesClient = GMSPlacesClient.shared()
    

    let filter = GMSAutocompleteFilter()
    filter.country = "GH"
    
    
    placesClient.autocompleteQuery(query, bounds: nil, boundsMode: .bias, filter: filter) { (predictions, error) in
        
        if let err = error {
            print(err.localizedDescription)
            completion(nil)
            return
        }
        
        guard let predictions = predictions else{
            completion(nil)
            return
        }
        
        var placeCompletes = [PlaceComplete]()
        
        for prediction in predictions {
            let pc = PlaceComplete()
            pc.name = prediction.attributedPrimaryText.string
            pc.description = prediction.attributedSecondaryText?.string
            pc.placeId = prediction.placeID
        
            placeCompletes.append(pc)
            print("\(prediction.attributedPrimaryText.string) , \(String(describing: prediction.attributedSecondaryText?.string))")
        }
        
        completion(placeCompletes)
    }
}

func getLatLngFromPlaceId(viewController:UIViewController, place: PlaceComplete, completion: @escaping (_ placecomplete: PlaceComplete) -> Void){
    
    print("place id => \(String(describing: place.placeId))")
    let placesClient = GMSPlacesClient.shared()
    
    // A hotel in Saigon with an attribution.
    //let placeID = "ChIJV4k8_9UodTERU5KXbkYpSYs"

    // Specify the place data types to return.
    let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
        UInt(GMSPlaceField.placeID.rawValue) | GMSPlaceField.coordinate.rawValue)!

    let dialog = AnyProgressDialog.newInstance(view: viewController.view).show()
    
    placesClient.fetchPlace(fromPlaceID: place.placeId!, placeFields: fields, sessionToken: nil, callback: {
      (foundPlace: GMSPlace?, error: Error?) in
            dialog.dismis()
        
      if let error = error {
        Toast.error(view: viewController.view, message: "Unable to find location")
        print("An error occurred: \(error.localizedDescription)")
        return
      }
      if let foundPlace = foundPlace {
        
        place.latitude = foundPlace.coordinate.latitude
        place.longitude = foundPlace.coordinate.longitude
        
        completion(place)
        print("The selected place is: \(String(describing: foundPlace.name))")
        print("latitude => \(String(describing: place.latitude))")
        print("longitude => \(String(describing: place.longitude))")
      }
    })
    
}

// check if enrout is operating at the selected place - TESTED
func validatePlace(viewController: UIViewController, placeComplete: PlaceComplete, pointType: PointType, completion: @escaping (_ status: Bool,_ message: String) -> Void){
//    let res = Trip.createDumyTrip()
//    let pickups:[Pickup] = res.pickups
//    let pickup = pickups[0]
    
//    print("latitude => \(String(describing: pickup.pos!.geopoint!.latitude!)) & longitude => \(String(describing: pickup.pos!.geopoint!.longitude!)) && place => \(String(describing: pickup.place!))")
    
    let url = Constants.BASE_URL + "/universal/calculate-distances/check-location"
    let p = [
        "place": placeComplete.name!,
        "latitude": placeComplete.latitude!,
        "longitude": placeComplete.longitude!,
        "pointType": pointType == .PICKUP ? pointType.rawValue : "DELIVERY",
        ] as [String : Any]
    
    let dialog = AnyProgressDialog.newInstance(view: viewController.view!).show()
    AF.request(url, method: .post, parameters: p).useBasicAuth().response { (response) in
        if let err = response.error {
            dialog.dismis()
            //Toast.error(view: self.view, message: err.localizedDescription)
            print("Error  \(err.localizedDescription)")
            return
        }
        
        guard let data = response.data else {
              //Toast.error(view: self.view, message: "Please try again later")
              return
          }
           //
          let json = try? JSON(data: data)
          let dict = json?.dictionary
          print("json returned => \(String(describing: dict))")
          guard let jsonDict = dict else {
              return
          }
           
           dialog.dismis()
           print(jsonDict)
        let status = jsonDict["status"]?.bool
        let message = jsonDict["message"]?.string
        
        completion(status!, message!)
    }
}


// get the estimated cost
func estimateCost(viewController: UIViewController, completion: @escaping (_ message: String?,_ fare: Double?, _ isSurgeCharge: Bool?, _ status: Bool) -> Void){
    //let res = UIConstants.activeTrip
    let pickups:[Pickup] = UIConstants.activePickUps
    let destinations:[Destination] = UIConstants.activeDestinations
    
    var pickupsForServer = [Any]()
    var destinationsForServer = [Any]()
    
    for pp in pickups {
        
        var ppData = [String:Any]()
        ppData["place"] = pp.place
        ppData["longitude"] = pp.pos!.geopoint?.longitude
        ppData["latitude"] = pp.pos!.geopoint?.latitude

        pickupsForServer.append(ppData)
    }
    
    for dest in destinations {
        
        var ddData = [String:Any]()
        ddData["place"] = dest.place
        ddData["longitude"] = dest.pos?.geopoint?.longitude
        ddData["latitude"] = dest.pos?.geopoint?.latitude

        destinationsForServer.append(ddData)
    }
    
    getDirectionBtnPoints { (status, message, distance, time, routes) in
        if !status {
            completion(nil, nil, nil, false)
            return
        }
        
         let p = [
                "pickups": stringify(from: pickupsForServer)!,
                "destinations": stringify(from: destinationsForServer)!,
                "type": TripTypeCode.SPSD.rawValue,
                "totalDistance": distance!.value! / 1000,
                "totalTime" : time!.value! / 60
                ] as [String : Any]
            let url = Constants.BASE_URL + "/universal/calculate-distances"
            let dialog = AnyProgressDialog.newInstance(view: viewController.view!)
            
        AF.request(url, method: .post, parameters: p).useBasicAuth().response { (response) in
                
                if let err = response.error {
                       dialog.dismis()
                       //Toast.error(view: self.view, message: err.localizedDescription)
                       print("Error  \(err.localizedDescription)")
                        completion(nil, nil, nil, false)
                       return
                   }
                   
                   guard let data = response.data else {
                       //Toast.error(view: self.view, message: "Please try again later")
                     completion(nil, nil, nil, false)
                       return
                   }
            //
                   let json = try? JSON(data: data)
                   let dict = json?.dictionary
                   print("json returned => \(String(describing: dict))")
                   guard let jsonDict = dict else {
                       return
                   }
                
                dialog.dismis()
                print(jsonDict)
                
                let status = jsonDict["status"]?.bool
                let message = jsonDict["message"]?.string
                let fare = jsonDict["fare"]?.double
                let isSurgeCharge = jsonDict["isSurgeCharge"]?.bool
                
                completion(message!, fare!, isSurgeCharge!, status!)

            }
        
    }
    
}

func getRoute(origin: PosLatLng, destination: PosLatLng, completion: @escaping(_ status: Bool?,_ route: [String:Any]?) -> Void){
    let mode = "driving"
    let url = "https://maps.googleapis.com/maps/api/directions/json?key=\(Constants.SERVER_KEY)&mode=\(mode)&origin=\(String(describing: origin.latitude!)),\(origin.longitude!)&destination=\(String(describing: destination.latitude!)),\(destination.longitude!)"
    
    print("getRoute url => \(url)")
    
    AF.request(url, method: .get).response { (response) in
        if let err = response.error {
              //Toast.error(view: self.view, message: err.localizedDescription)
              print("Error  \(err.localizedDescription)")
               completion(false, nil)
              return
          }
        
        guard let data = response.data else {
                   //Toast.error(view: self.view, message: "Please try again later")
                 completion(false, nil)
                   return
               }
        //
               let json = try? JSON(data: data)
               let dict = json?.dictionary
               guard let jsonDict = dict else {
                   return
               }
            
        
        if jsonDict["status"]?.string != "OK"{
            let error_message = jsonDict["error_message"]?.string
            print("Error message => \(String(describing: error_message))")
            completion(false, nil)
            return
        }
        
        let routes = jsonDict["routes"]?.array
        let route = routes?[0]
        let polylines = route?["overview_polyline"].dictionaryObject
        
        completion(nil, polylines)
    }
}

func getDirectionBtnPoints(pickups: [Pickup]? = nil, destinations: [Destination]? = nil ,completion: @escaping (_ status: Bool,_ error_message: String?,_ distance: Distance?,_ time: Duration?, _ routes: [String:Any]? ) -> Void){
    
    var places = [PlaceComplete]()
    for pickup in (pickups == nil ? UIConstants.activePickUps : pickups)! {
        let place = PlaceComplete()
        place.latitude = pickup.pos?.geopoint?.latitude
        place.longitude = pickup.pos?.geopoint?.longitude
        
        places.append(place)
    }
    
    for destination in (destinations == nil ? UIConstants.activeDestinations : destinations)! {
        let place = PlaceComplete()
        place.latitude = destination.pos?.geopoint?.latitude
        place.longitude = destination.pos?.geopoint?.longitude
        
        places.append(place)
    }
    
    if places.count < 2 {
        completion(false, "Invalid number of places", nil, nil, nil)
        return
    }
    let lastIndex = places.count - 1
    let origin = (latitude: places[0].latitude,longitude: places[0].longitude)
    let destination = (latitude: places[lastIndex].latitude,longitude: places[lastIndex].longitude)
    let mode = "driving"
    
    var url = "https://maps.googleapis.com/maps/api/directions/json?key=\(Constants.SERVER_KEY)&mode=\(mode)&origin=\(origin.latitude!),\(origin.longitude!)&destination=\(destination.latitude!),\(destination.longitude!)"
    
    print("number of places was => \(places.count)")
    if places.count > 2 {
        url = "\(url)&waypoints="
        for (index,place) in places.enumerated() {
            if index != 0 && index != lastIndex {
                url = "\(url)via:\(place.latitude!),\(place.longitude!)|"
                
            }
        }
        url.remove(at: url.index(before: url.endIndex))
    }
    url = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
    print("url => \(url)")
    
    AF.request(url, method: .get).response { (response) in
        
                if let err = response.error {
                   //Toast.error(view: self.view, message: err.localizedDescription)
                   print("Error  \(err.localizedDescription)")
                    completion(false, err.localizedDescription, nil, nil, nil)
                   return
               }
               
               guard let data = response.data else {
                   //Toast.error(view: self.view, message: "Please try again later")
                 completion(false, "Unable to parse data", nil, nil, nil)
                   return
               }
        //
               let json = try? JSON(data: data)
               let dict = json?.dictionary
               guard let jsonDict = dict else {
                   return
               }
            
        
        if jsonDict["status"]?.string != "OK"{
            let error_message = jsonDict["error_message"]?.string
            print("Error message => \(String(describing: error_message))")
            completion(false, error_message, nil, nil, nil)
            return
        }
        
            let routes = jsonDict["routes"]?.array
            let route = routes?[0]
            let legs = route?["legs"].array
            let leg = legs?[0].dictionary
            let distance = leg?["distance"]?.dictionary
            let duration = leg?["duration"]?.dictionary
            let polylines = route?["overview_polyline"].dictionaryObject
            
            
            //print("leg => \(String(describing: leg))")
            let distanceText = distance?["text"]?.string
            let distanceValue = distance?["value"]?.double
            
            let durationText = duration?["text"]?.string
            let durationValue = duration?["value"]?.double            
            
            completion(true, nil, Distance(text: distanceText, value: distanceValue), Duration(text: durationText, value: durationValue), polylines)
            
    }
    
    
}

private func stringify(from object:Any) -> String? {
    guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
        return nil
    }
    return String(data: data, encoding: String.Encoding.utf8)
}


// request for a trip
func dispatchRequest(viewController: UIViewController, userCurrLatLng: PosLatLng, completion: @escaping (_ status: Bool, _ error: String?) -> Void){
//
    let trip = UIConstants.activeTrip
    let pickups = UIConstants.activePickUps
    let destinations = UIConstants.activeDestinations
    
//    let dummyTrip = Trip.createDumyTrip()
//    let trip = dummyTrip.trip
//    let pickups = dummyTrip.pickups
//    let destinations = dummyTrip.destinations
    
    trip.senderId = Auth.auth().currentUser?.uid
    trip.senderName = Constants.applicationUser?.fullName
    trip.senderPhoneNumber = Constants.applicationUser?.phoneNumber
    
    
    trip.isAccepted  = false
    trip.isArrived = false
    trip.requestDatetime = FieldValue.serverTimestamp()
    trip.riderId = nil
    trip.riderName = nil
    trip.riderPhoneNumber = nil
    trip.riderPlateNumber = nil
    trip.status = TripStatus.PENDING.rawValue
    trip.hasPaid = false
    trip.riderImage = nil
    trip.orderNumber = nil
    trip.userCurrentLng = userCurrLatLng.longitude
    trip.userCurrentLat = userCurrLatLng.latitude
    
    var currentLocation = CurrentLocation()
    currentLocation.latitude = trip.userCurrentLat
    currentLocation.longitude = trip.userCurrentLng
//
    trip.currentLocation = currentLocation
    trip.isPayForMe = false
    trip.fleetId = nil
    trip.scheduleId = nil
    
    trip.taskCount = pickups.count + destinations.count
    trip.taskCompleted = 0
    trip.rated = false
    trip.rating = 0.0
    trip.device = "IOS"
    trip.actualCost = 0.0
    trip.arrivedAt = FieldValue.serverTimestamp()
    
    let dialog = AnyProgressDialog.newInstance(view: viewController.view).show()
    let db = Firestore.firestore()
    
    let docRef = db.collection(Collections.REQUEST.rawValue).document(String(describing: UUID()))
    trip.requestId = docRef.documentID
    
    print("trip id: " + trip.requestId!)
    
    var pickupNames = [String?]()
    var pickUpDocRefs = [DocumentReference?]()
    var stringPickupRef = [String?]()
    
    var selectedItemCategories = [String?]()
    var selectedInstructions = [String?]()
    
    let ppGroup = DispatchGroup()
    let ddGroup = DispatchGroup()

    
    // set pickups
    for pp in pickups {
         ppGroup.enter()
        let pRef =  db.collection(Collections.PICKUPPOINTS.rawValue).document(String(describing: UUID()))
        pp.documentReference = pRef
        pp.referenceId = pRef.documentID
        pp.requestId = trip.requestId
        
        pp.isArrived = false
        pp.isPickedup = false
        pp.isArrivedNotified = false
        pp.isPickedupNotified = false

        pickupNames.append(pp.place)
        pickUpDocRefs.append(pRef)
        stringPickupRef.append(pRef.documentID)
        
        selectedItemCategories.append(pp.itemCategory)
        selectedInstructions.append(pp.instruction)
        
        let data: [String: Any?] = [
        "instruction" : pp.instruction,
        "isArrived" : pp.isArrived,
        "isArrivedNotified" : pp.isArrivedNotified,
        "isPickedup" : pp.isPickedup,
        "isPickedupNotified" : pp.isPickedupNotified,
        "itemCategory" : pp.itemCategory,
        "pickedupAt" : pp.pickedupAt,
        "pickUpName" : pp.pickUpName,
        "pickUpPersonPhone" : pp.pickUpPersonPhone,
        "place" : pp.place,
        "pos" : [
            "geohash" : pp.pos!.geohash!,
            "geopoint" : [
                "latitude" : pp.pos!.geopoint!.latitude,
                "longitude" : pp.pos!.geopoint!.longitude
            ]
        ],
        "referenceId" : pp.referenceId,
        "requestId" : pp.requestId
      ]
    db.collection(Collections.PICKUPPOINTS.rawValue).document(pp.referenceId!).setData(data as [String : Any]){
        error in
          
            
            if let err = error {
                completion(false, err.localizedDescription)
                return
            }
            
            ppGroup.leave()
        }
    }
    
    var destinationNames = [String?]()
    var stringDestRef = [String?]()
    var destDocRefs = [DocumentReference?]()
    
    ppGroup.notify(queue: .main) {
        
        //
        // set destinations
        for dest in destinations {
            ddGroup.enter()
            let dRef = db.collection(Collections.DESTINATIONS.rawValue).document(String(describing: UUID()))
           
            dest.documentReference = dRef
            dest.referenceId = dRef.documentID
            dest.requestId = trip.requestId
            
            dest.isArrived = false
            dest.isDelivered = false
            dest.isArrivedNotified = false
            dest.isDeliveredNotified = false
            
            destinationNames.append(dest.place)
            destDocRefs.append(dRef)
            stringDestRef.append(dRef.documentID)
            
            let data: [String:Any?] = [
                "deliveredAt" : dest.deliveredAt,
                "isArrived" : dest.isArrived,
                "isArrivedNotified" : dest.isArrivedNotified,
                "isDelivered" : dest.isDelivered,
                "isDeliveredNotified" : dest.isDeliveredNotified,
                "place" : dest.place,
                "pos" : [
                    "geohash" : dest.pos!.geohash!,
                    "geopoint" : [
                        "latitude" : dest.pos!.geopoint!.latitude,
                        "longitude" : dest.pos!.geopoint!.longitude
                    ]
                ],
                "recipientName" : dest.recipientName,
                "recipientNumber" : dest.recipientNumber,
                "referenceId" : dest.referenceId,
                "requestId" : dest.requestId
            
            ]
            
            db.collection(Collections.DESTINATIONS.rawValue).document(dest.referenceId!).setData(data as [String : Any], completion: {
                error in
                
                if let err = error {
                    completion(false, err.localizedDescription)
                    return
                }
                
                ddGroup.leave()
                
            })
        }
        
        ddGroup.notify(queue: .main) {
            
               trip.pickupsRef = stringPickupRef as? [String]
                trip.destinationsRef = stringDestRef as? [String]
            //
            trip.pickups = pickUpDocRefs as? [DocumentReference]
            trip.destinations = destDocRefs as? [DocumentReference]
          
                trip.pickupNames = pickupNames as? [String]
                trip.destinationNames = destinationNames as? [String]
            //
                trip.selectedItemCategories = selectedItemCategories as? [String]
                trip.selectedItemInstructions = selectedInstructions as? [String]
            
                trip.typeCode = getTripType().rawValue

                var pos = Pos()
                pos.geohash = Constants.getGeoHash(latitude: trip.userCurrentLat!, longitude: trip.userCurrentLng!)
                pos.geopoint = PosLatLng(latitude: trip.userCurrentLat!, longitude: trip.userCurrentLng!)
                trip.pos = pos
            
            trip.couponPreviousPrice = UIConstants.activeTrip.estimatedCost
            
                if let c = Constants.TEMP_COUPON {
                    trip.couponApplied = true;
                    trip.couponName = c.name;
                    trip.couponValueInPercentage = c.value;
                    trip.coupon = db.collection(Collections.COUPONS.rawValue).document(c.docId!)
                    trip.estimatedCost = trip.estimatedCost! - (trip.estimatedCost! * c.value!)
                
                    applyCoupon(code: c.code!) { (err, coupon) in
                        
                        // Then EMIT TRIP TO SERVER
                      
                        
                    }
                }
            
//                if let c = Constants.TEMP_COUPON {
//                    UIConstants.activeTrip.couponApplied = true;
//                    trip.couponName = c.name;
//                    UIConstants.activeTrip.couponValueInPercentage = c.value;
//                    trip.couponApplied = true
//                    trip.couponValueInPercentage = c.value
//                }
            
              trip.pointToBeginSearch = pickups[0].pos?.geopoint
            
                let tripNotfiedStatus = TripStatusNotified()
               trip.tripStatusNotified = tripNotfiedStatus
                
                let tripData : [String : Any] = Trip.convertTripToData(trip: trip)
               
            print("tripData dests: \(String(describing: tripData["destinations"]))")
            //
            db.collection(Collections.REQUEST.rawValue).document(trip.requestId!).setData(tripData) { (error) in
                    
                        if let err = error {
                            dialog.dismis()
                            completion(false, err.localizedDescription)
                            print("Error occured when creating trip => \(err.localizedDescription)")
                            return
                        }
                    
                    // AFTER FIREBASE IS SUCCESSFUL
                
                
                emitTripToServer(requestId: trip.requestId!, completion: {
                      error in
                      dialog.dismis()
                      if let err = error {
                          completion(false, err.localizedDescription)
                          return
                      }
                      
                      completion(true, nil)
                  })
                    
                        
                    
                }
        }
        
    }

    
    
}

private func emitTripToServer(requestId: String, completion: @escaping (_ error: Error?) -> Void){
    
    let p = [
        "requestId": requestId
    ]
    let url = Constants.BASE_URL + "/universal/request-matrix/search-request/v2"
    print("url => \(url)")
    AF.request(url, method: .post, parameters: p).useBasicAuth().response { (response) in
        
        if let err = response.error {
           //Toast.error(view: self.view, message: err.localizedDescription)
           print("emitTripToServer Error  \(err.localizedDescription)")
            completion(CustomError(localizedDescription: err.localizedDescription))
           return
       }
       
       guard let data = response.data else {
           //Toast.error(view: self.view, message: "Please try again later")
           return
       }
//
       let json = try? JSON(data: data)
       let dict = json?.dictionary
       print("json returned => \(String(describing: dict))")
       guard let jsonDict = dict else {
        completion(CustomError(localizedDescription: "unable to decode json data"))
           return
       }
        
//
       let status = jsonDict["status"]?.bool
       let message = jsonDict["message"]?.string
        
        print("search-request/v2 -----------------")
        print("status => \(String(describing: status)) , message => \(String(describing: message))")
        if !status!{
            completion(CustomError(localizedDescription: message!))
            return
        }
        
        completion(nil)
 
    }
    
}

// cancel trip
func cancelTrip(viewController:UIViewController, trip: Trip?, reasons: String, completion: @escaping () -> Void){
    
    let data = [
        "requestId": trip?.requestId,
        "customerId": Auth.auth().currentUser?.uid,
        "reasons": reasons,
        "customerName": Constants.applicationUser?.fullName,
        "customerPhoneNumber": Constants.applicationUser?.phoneNumber,
        "isAccepted": trip != nil && "ONGOING" == trip?.status
        ] as [String : Any?]
    
    
    let dialog = AnyProgressDialog.newInstance(view: viewController.view).show()
      let connection = SockIOConnection.getSocketInstance()
    
    connection.socket.on("request::cancelled::done") { (data, ark) in
        dialog.dismis()
        completion()
    }
    
    connection.socket.emit("request::cancelled", data)
    
}


// listen to pick up points status changes
func listenToPickupChanges(trip: Trip?, completion: @escaping (_ pickups: [Pickup]) -> Void ){
    let db = Firestore.firestore()
    db.collection(Collections.PICKUPPOINTS.rawValue).whereField("requestId", isEqualTo:  trip!.requestId!).addSnapshotListener { (snapshots, error) in
               if let err = error {
                   print("\(err.localizedDescription)")
                   return
               }
               
           let pickups = (snapshots?.documents.compactMap({ (snapshot) in
               snapshot.data().flatMap { (data) in
                   return Pickup(dictionary: data)
               }
           }))!
         
        completion(pickups)
               
       }
}

// listen to destination points status changes
func listenToDestChanges(trip: Trip?, completion: @escaping (_ destinations: [Destination]) -> Void ) {
    let db = Firestore.firestore()
    db.collection(Collections.DESTINATIONS.rawValue).whereField("requestId", isEqualTo:  trip!.requestId!).addSnapshotListener { (snapshots, error) in
            if let err = error {
               print("\(err.localizedDescription)")
               return
            }
        
    let destinations = (snapshots?.documents.compactMap({ (snapshot) in
            snapshot.data().flatMap { (data) in
                return Destination(dictionary: data)
            }
        }))!
        
   completion(destinations)
                                
    
    }
}

// listen to changes on a trip
func listenToStatusChange(tripId: String, completion: @escaping (_ trip: Trip?, _ status: TripStatus?, _ error: Error?) -> Void){
    let db = Firestore.firestore()
    db.collection(Collections.REQUEST.rawValue).document(tripId).addSnapshotListener { (snapShot, error) in
        if let err = error {
            completion(nil,nil,err)
            return
        }
        
        let mTrip = snapShot?.data().flatMap({ (data) in
            return Trip(dictionary: data)
        })
    
        if mTrip != nil {
        completion(mTrip,TripStatus(rawValue: mTrip!.status!), nil)
        }
        
    }
}

// fetch previous trip so that user can request trip again
func requestTripAgain(tripId: String, completion: @escaping (_ trip: Trip?,_ pickups:[Pickup]?,_ destinations: [Destination]?,_ error: Error?) -> Void)
{
    let db = Firestore.firestore()
    
 db.collection(Collections.REQUEST.rawValue).document(tripId).getDocument { (snapshot, error) in
        if let err = error {
            print("Error => \(err.localizedDescription)")
            completion(nil, nil, nil, err)
            return
        }
        
        let data = snapshot!.data()
        let trip = data.flatMap { (data) in
            return Trip(dictionary: data)
        }
        
        // get pickups
    db.collection(Collections.PICKUPPOINTS.rawValue).whereField("requestId", isEqualTo: tripId).getDocuments { (ppSnapshots, error) in
            
            if let err = error {
                print("Error => \(err.localizedDescription)")
                completion(nil, nil, nil, err)
                return
            }
        
            let pickups = (ppSnapshots?.documents.compactMap({ (snapshot) in
                snapshot.data().flatMap { (data) in
                    return Pickup(dictionary: data)
                }
            }))!
        
        db.collection(Collections.DESTINATIONS.rawValue).whereField("requestId", isEqualTo: tripId).getDocuments { (ddSnapshots, error) in
                if let err = error {
                    print("Error => \(err.localizedDescription)")
                    completion(nil, nil, nil, err)
                    return
                }
            
                let destinations = (ddSnapshots?.documents.compactMap({ (snapshot) in
                    snapshot.data().flatMap { (data) in
                        return Destination(dictionary: data)
                    }
                }))!
            
                completion(trip, pickups, destinations, nil)
            }
        }
        // get destinations
        
    }

    
}

// fetch outstanding owing trip


// pay for owing trip



// for getting the rider image
extension UIImageView {
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleToFill) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
        }.resume()
    }
    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleToFill) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}

func checkOutstandingPayment(completion: @escaping (_ foundPendingTrip: Bool, _ trip: Trip?, _ hasExceededLimit: Bool) -> Void){
    let db = Firestore.firestore()
    if Auth.auth().currentUser == nil {
         completion(false, nil, false)
        return
    }
    db.collection(Collections.REQUEST.rawValue)
        .whereField("senderId", isEqualTo: Auth.auth().currentUser!.uid)
    .whereField("hasPaid", isEqualTo: false)
        .whereField("status", isEqualTo: "COMPLETED")
        .addSnapshotListener { (snapshots, error) in
            // check if there are pending trips
            guard let snapshots = snapshots else {
                completion(false, nil, false)
                return
            }
            
            if snapshots.isEmpty {
                completion(false, nil, false)
                return
            }
            
            var nonCashPayTrip: Trip?
            var countNonCash = 0
            for item in snapshots.documents {
                let data = item.data()
                let mTrip = Trip(dictionary: data)
                if mTrip.paymentMethod! != "Cash" && mTrip.paymentMethod! != "CASH" {
                    nonCashPayTrip = mTrip
                    countNonCash = countNonCash + 1
                }
            }
            
            if nonCashPayTrip == nil {
                completion(false, nil, false)
                return
            }
            
            if countNonCash < 2 {
                // user can request for another trip
                completion(true, nonCashPayTrip, false)
            }else {
                // don't allow for another trip request
                completion(true, nonCashPayTrip, true)
            }
            
    }
}


func markTripStatusAsNotified(trip: Trip){
//    tripStatusNotified
    let db = Firestore.firestore()
    db.collection(Collections.REQUEST.rawValue).document(trip.requestId!).updateData(["tripStatusNotified": trip.tripStatusNotified!.convertToDate()])
}


func markIsPickupArrivedAsNotified(pickup: Pickup){
    let db = Firestore.firestore()
       pickup.isArrivedNotified = true
   db.collection(Collections.PICKUPPOINTS.rawValue).document(pickup.referenceId!).updateData(["isArrivedNotified" : pickup.isArrivedNotified!])
}


func markIsPickedupAsNotified(pickup: Pickup){
    let db = Firestore.firestore()
       pickup.isPickedupNotified = true
   db.collection(Collections.PICKUPPOINTS.rawValue).document(pickup.referenceId!).updateData(["isPickedupNotified" : pickup.isPickedupNotified!])
}

func markIsDestinationArrivedAsNotified(destination: Destination){
    let db = Firestore.firestore()
    destination.isArrivedNotified = true
db.collection(Collections.DESTINATIONS.rawValue).document(destination.referenceId!).updateData(["isArrivedNotified" : destination.isArrivedNotified!])
}

func markIsDestinationDeliveredAsNotified(destination: Destination){
    let db = Firestore.firestore()
        destination.isDeliveredNotified = true
    db.collection(Collections.DESTINATIONS.rawValue).document(destination.referenceId!).updateData(["isDeliveredNotified" : destination.isDeliveredNotified!])
}

//func fetchMostRecentTrip(completion: @escaping (_ error: Error?, _ trips: [Trip]?)){
//
//    let db = Firestore.firestore()
//
//}
