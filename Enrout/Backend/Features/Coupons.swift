//
//  Coupons.swift
//  Enrout
//
//  Created by Daniel Expresspay on 06/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Foundation
import Firebase


func checkIfCouponCanBeApplied(viewController: UIViewController, code: String, completion: @escaping (_ error:Error?, _ coupon: Coupon?) -> Void ){
    
    let db = Firestore.firestore()
    let dialog = AnyProgressDialog.newInstance(view: viewController.view).show()
    db.collection(Collections.COUPONS.rawValue).whereField("code", isEqualTo: code).getDocuments { (snapshots, error) in
        
            if let err = error {
                dialog.dismis()
                print("Error => \(err.localizedDescription)")
               completion(err, nil)
                return
            }
            
            guard let documents = snapshots?.documents else {
                return
            }
        
            if documents.isEmpty {
                dialog.dismis()
                Toast.error(view: viewController.view, message: "Coupon does not exist")
                print("Coupon does not exist")
                completion(CustomError(localizedDescription: "Coupon does not exist"), nil)
                return
            }
            
            let data = documents[0].data()
            var coupon = Coupon()
            coupon.code = data["code"] as? String
            coupon.docId = data["docId"] as? String
            coupon.individualUserLimit = data["individualUserLimit"] as? Int
            coupon.limit = data["limit"] as? Int
            coupon.name = data["name"] as? String
            coupon.totalUsed = data["totalUsed"] as? Int
            coupon.usersAllowed = data["usersAllowed"] as? Int
            coupon.value = data["value"] as? Double
            coupon.status = data["status"] as? String
        
        if coupon.status == nil || coupon.status == "INACTIVE" {
            dialog.dismis()
            Toast.error(view: viewController.view, message: "The coupon has expired")
            print("Coupon does not exist")
            completion(CustomError(localizedDescription: "The coupon has expired"), nil)
            return
        }
            
            let anyEntrySet = data["entries"] as! [[String:Any]]
            var mEntrys = [Entry]()
            for x in anyEntrySet
            {
                let newEntry = Entry(usage: x["usage"] as? Int, userId: x["userId"] as? String)
                mEntrys.append(newEntry)
            }
            coupon.entriesList = mEntrys
            
            dialog.dismis()
        
        // Toast.success(view: viewController.view, message: "Coupon exists")
               print("coupon => \(String(describing: coupon))")
               print("any entry set => \(String(describing: anyEntrySet))")
               
               if coupon.totalUsed! >= coupon.limit!{
                   completion(CustomError(localizedDescription: "This coupon has been exhausted"), nil)
                   return;
               }
        
            for (index, entry) in coupon.entriesList.enumerated() {
                
                if Auth.auth().currentUser!.uid == entry.userId! {
                    // check if user has exceeded his limit
                    if coupon.individualUserLimit! <= entry.usage! {
                        completion(CustomError(localizedDescription: "Sorry, you can't use this coupon again"), nil)
                        return
                    }
                }
            }
        
            completion(nil, coupon)
            
            // end here
            
       
        }
    
}

func applyCoupon(code: String, completion: @escaping (_ error:Error?, _ coupon: Coupon?) -> Void){
    let db = Firestore.firestore()

    db.collection(Collections.COUPONS.rawValue).whereField("status", isEqualTo: "ACTIVE").whereField("code", isEqualTo: code).getDocuments { (snapshots, error) in
        
        if let err = error {
            print("Error => \(err.localizedDescription)")
           completion(err, nil)
            return
        }
        
        guard let documents = snapshots?.documents else {
            return
        }
        
        if documents.isEmpty {
            print("Coupon does not exist")
            completion(CustomError(localizedDescription: "Coupon does not exist"), nil)
            return
        }
        
        let data = documents[0].data()
        var coupon = Coupon()
        coupon.code = data["code"] as? String
        coupon.docId = data["docId"] as? String
        coupon.individualUserLimit = data["individualUserLimit"] as? Int
        coupon.limit = data["limit"] as? Int
        coupon.name = data["name"] as? String
        coupon.totalUsed = data["totalUsed"] as? Int
        coupon.usersAllowed = data["usersAllowed"] as? Int
        coupon.value = data["value"] as? Double
        
        let anyEntrySet = data["entries"] as! [[String:Any]]
        var mEntrys = [Entry]()
        for x in anyEntrySet
        {
            let newEntry = Entry(usage: x["usage"] as? Int, userId: x["userId"] as? String)
            mEntrys.append(newEntry)
        }
        coupon.entriesList = mEntrys
        
        
       // Toast.success(view: viewController.view, message: "Coupon exists")
        print("coupon => \(String(describing: coupon))")
        print("any entry set => \(String(describing: anyEntrySet))")
        
        if coupon.totalUsed! >= coupon.limit!{
            completion(CustomError(localizedDescription: "This coupon has been exhausted"), nil)
            return;
        }
        
          // check if user has used the coupon before
        var userHasUsedCouponBefore = false
        for (index, entry) in coupon.entriesList.enumerated() {
            
            if Auth.auth().currentUser!.uid == entry.userId! {
                userHasUsedCouponBefore = true
                // user has used this code before

                // check if user has exceeded his limit
                if coupon.individualUserLimit! <= entry.usage! {
                    completion(CustomError(localizedDescription: "Sorry, you can't use this coupon again"), nil)
                    break
                }
                
                // remove the current user's entry from the coupon entries
                let paramRemove = [
                    "entries": FieldValue.arrayRemove([[
                        "userId" : entry.userId!,
                        "usage" : entry.usage!
                        ]])
                ]
                
            
                db.collection(Collections.COUPONS.rawValue).document(coupon.docId!).updateData(paramRemove) { (error) in
                    if let err = error {
//                        dialog.dismis()
                        print(err.localizedDescription)
                        return
                    }
                    
                     entry.usage = entry.usage! + 1
                     coupon.entriesList[index] = entry
                    
                        // add the current user with updated usage to the list of ppl who have used the coupon
                        let paramAdd = [
                            "entries": FieldValue.arrayUnion([[
                                "userId" : entry.userId!,
                                "usage" : entry.usage!
                            ]]),
                            "totalUsed": coupon.totalUsed! + 1
                            ] as [String : Any]
                        
                    db.collection(Collections.COUPONS.rawValue).document(coupon.docId!).updateData(paramAdd) { (error) in
                            if let err = error {
//                                dialog.dismis()
                                print(err.localizedDescription)
                                return
                            }
                        
           
                        print("coupon applied")
                        completion(nil, coupon)
                
                    
                        }
                }
            
            } // end of checking if user has used the coupon before
            
        }
        
        if !userHasUsedCouponBefore {
            // todo
            let mEntry = Entry()
            mEntry.usage = 1
            mEntry.userId = Auth.auth().currentUser!.uid
            coupon.entriesList.append(mEntry)
            
            let paramAdd = [
                "entries": FieldValue.arrayUnion([[
                "userId" : mEntry.userId!,
                "usage" : mEntry.usage!
                ]]),
                "totalUsed": coupon.totalUsed! + 1
                ] as [String : Any]
          db.collection(Collections.COUPONS.rawValue).document(coupon.docId!).updateData(paramAdd) { (error) in
//                    dialog.dismis()
                       if let err = error {
                           print(err.localizedDescription)
                           return
                       }
               print("coupon applied")
               completion(nil, coupon)
            }
            
        }
        
        
                           // user's first time of using the code
    }
    

        
        //var entry = Entry()
//data["entriesList"][""] as?
        
}

