//
//  ContactUs.swift
//  Enrout
//
//  Created by Daniel Expresspay on 06/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Foundation
import UIKit

func contactUs(viewcontroller: UIViewController){
    let email = "info@enroutdelivery.com"
    if let url = URL(string: "mailto:\(email)") {
      if #available(iOS 10.0, *) {
        UIApplication.shared.open(url)
      } else {
        UIApplication.shared.openURL(url)
      }
    }
}
