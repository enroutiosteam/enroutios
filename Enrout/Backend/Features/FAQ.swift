//
//  FAQ.swift
//  Enrout
//
//  Created by Daniel Expresspay on 06/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Foundation
import UIKit
import Firebase

func openFaq(viewcontroller: UIViewController){
   let dialog = AnyProgressDialog.newInstance(view: viewcontroller.view).show()
    let db = Firestore.firestore()
    
    db.collection(Collections.SETTINGS.rawValue).getDocuments { (query, error) in
        if let err = error{
            dialog.dismis()
            Toast.error(view: viewcontroller.view, message: err.localizedDescription)
            return
        }
        
        let data = query!.documents[0].data()
        
        let faq = data["faq"] as! String
        guard let url = URL(string: faq) else {
            dialog.dismis()
            Toast.error(view: viewcontroller.view, message: "Please try again")
            return
        }
        dialog.dismis()
        UIApplication.shared.open(url)

    }
    
}
