//
//  Wallet.swift
//  Enrout
//
//  Created by Daniel Expresspay on 02/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth
import Alamofire
import SwiftyJSON

struct Wallet {
    let db = Firestore.firestore()
    let firebaseUser = Auth.auth().currentUser
    var view: UIView!
    var viewController: UIViewController?
    
    static func getInstance(view: UIView, viewController: UIViewController) -> Wallet {
        return Wallet(view, viewController: viewController)
    }
    
    private init(_ view: UIView, viewController: UIViewController) {
        self.view = view
        self.viewController = viewController
    }
    
    func getTransactions(onComplete: @escaping (_ walletTransaction: [WalletTransaction]?, _ error: Error?) ->  Void){
        guard let uid = Auth.auth().currentUser?.uid else {
            onComplete(nil, CustomError(localizedDescription: "user not found"))
            return
        }
        
        db.collection(Collections.TRANSACTIONLEDGER.rawValue)
        .whereField("senderId", isEqualTo: uid)
        .whereField("type", isEqualTo: "DEPOSIT")
        .whereField("transType", isEqualTo: "CREDIT")
        .order(by: "createdAt", descending: true)
            .getDocuments { (querySnapShot, error) in
                
                if let error = error {
                    print(error.localizedDescription)
                    onComplete(nil, error)
                    return
                }
                
                print("Number of transactions => \(String(describing: querySnapShot?.count))")
                
                if let walletTransactions = querySnapShot?.documents.compactMap({
                  $0.data().flatMap({ (data) in
                    return WalletTransaction(dictionary: data)
                  })
                }) {
                    print("Wallet Transactions: \(walletTransactions)")
                    onComplete(walletTransactions, nil)
                } else {
                    print("unable to decode transactions")
                    onComplete(nil, CustomError(localizedDescription: "unable to decode transactions"))
                }
                
        }
    }
    
    func getWallet(completion: @escaping (_ error: Error?) -> Void){
    db.collection(Collections.USERS.rawValue).document(Auth.auth().currentUser!.uid).getDocument { (snapshot, error) in
            if let err = error {
                print(err.localizedDescription)
                completion(error)
                return
            }
            let data = snapshot!.data()
        print("application user => \(Constants.applicationUser)")
        Constants.applicationUser?.currentBalance = data!["currentBalance"] as! Double
        Constants.applicationUser?.outStandingBalance = data!["outStandingBalance"] as! Double
        
            completion(nil)
        }
    }
    
    func payForTripWithMomo(trip: Trip, momoNumber: String, network: MomoNetwork, amount: Double, vodafoneAuthToken: String?, completion: @escaping (_ paymentStatus: Bool, _ error: Error?) -> Void){
        
        let paymentRefId = getUniqueString()
        
        var payment = Payment()
        payment.amount = amount
        if let vodafoneAuthToken = vodafoneAuthToken{
            payment.mobileAuthToken = vodafoneAuthToken
        }
        payment.momoNumber = momoNumber
        payment.network = network.rawValue
        payment.orderId = paymentRefId
        payment.senderId = firebaseUser!.uid
        payment.senderName = Constants.applicationUser!.fullName
        
        var param = [String:Any]()
        param["amount"] = payment.amount
        param["mobileAuthToken"] = payment.mobileAuthToken
        param["momoNumber"] = payment.momoNumber
        param["network"] = payment.network
        param["orderId"] = payment.orderId
        param["senderId"] = payment.senderId
        param["senderName"] = payment.senderName
        param["status"] = "PENDING"
        
        print("trip id => \(trip.requestId)")
        
        let dialog = AnyProgressDialog.newInstance(view: view).show()
        db.collection(Collections.PAYMENTS.rawValue).document(paymentRefId).setData(param) {  error in
            dialog.dismis()
            
            if let error = error {
                completion(false, error)
           
                print("Server Error - \(error)")
                return
            }
            
            // pass the parameter to the api
            
            var apiNetwork: String?
            
            switch network {
            case MomoNetwork.MTN:
                apiNetwork = "MTN_MM"
                break
            case MomoNetwork.AIRTEL:
                 apiNetwork = "TIGO_CASH"
                break
            case MomoNetwork.TIGO:
                 apiNetwork = "AIRTEL_MM"
                break
            case MomoNetwork.VODAFONE:
                 apiNetwork = "VODAFONE_CASH"
                break
            }
            

            let url = Constants.BASE_URL + "/transactions/pay/pay-for-trip-momo"
            
            var p = [String: Any]()
            p["amount"] = amount
            p["orderId"] = paymentRefId
            p["momoNumber"] = momoNumber
            p["network"] = apiNetwork!
            p["mobileAuthToken"] = vodafoneAuthToken
            p["paymentType"] = "TRIP_PAYMENT"
            p["senderName"] = trip.senderName!
            p["transType"] = "DEBIT"
            p["senderId"] = trip.senderId!
            p["currency"] = "GHS"
            p["coupon"] = String(describing: trip.actualCost! * trip.couponValueInPercentage!/100)
            p["requestId"] = trip.requestId!
            p["payForMe"] = String(describing: trip.payForMe!)
            p["riderId"] = trip.riderId!
            p["fleetId"] = trip.fleetId!
            p["type"] = paymentRefId
            p["senderPhoneNumber"] = trip.senderPhoneNumber!
            p["riderPlateNumber"] = trip.riderPlateNumber!
            p["riderPhoneNumber"] = trip.riderPhoneNumber!
            
            let dialog2 = AnyProgressDialog.newInstance(view: self.view).show()
            
            AF.request(url, method: .post, parameters: p).useBasicAuth().response {
                response in
                dialog2.dismis()
                
                self.afterPayment(paymentRefId: paymentRefId, response: response, onCompletion: completion)
                
            }
            
        }
        
        
    }
    
    func payForTripWithWallet(trip: Trip, completion: @escaping (_ error: CustomError?, _ insufficientFunds: Bool) -> Void){
        
        let dialog = AnyProgressDialog.newInstance(view: self.viewController!.view).show()
        
        getWallet { (error) in
            dialog.dismis()
            if let error = error {
                completion(CustomError(localizedDescription: error.localizedDescription), false)
                return
            }
            
            let mCurrentBal = Constants.applicationUser!.currentBalance
            let tripCost = trip.actualCost!
            if  mCurrentBal < tripCost {
                completion(CustomError(localizedDescription: "Your wallet balance is below cost."), true)
                return
            }
            
            let url = Constants.BASE_URL + "/transactions/pay/pay-for-trip-wallet"
            var param = [String:Any]()
            param["amount"] = trip.actualCost!
            param["coupon"] = trip.actualCost! * trip.couponValueInPercentage!/100
            param["requestId"] = trip.requestId!
            param["payForMe"] = trip.payForMe!
            param["riderId"] = trip.riderId!
            param["senderId"] = trip.senderId!
            param["fleetId"] = trip.fleetId!
            param["type"] = "TRIP_PAYMENT"
            param["orderId"] = trip.orderNumber!
            param["senderPhoneNumber"] = trip.senderPhoneNumber!
            param["riderPlateNumber"] = trip.riderPlateNumber!
            param["riderPhoneNumber"] = trip.riderPhoneNumber!
            
            let dialog2 = AnyProgressDialog.newInstance(view: self.view).show()
            AF.request(url, method: .post, parameters: param).useBasicAuth().response { (response) in
                dialog2.dismis()
                           if let err = response.error {
                               
                               Toast.error(view: self.view, message: err.localizedDescription)
                               print("Error  \(err.localizedDescription)")
                               return
                           }
                           
                           guard let data = response.data else {
                               Toast.error(view: self.view, message: "Please try again later")
                               return
                           }
                           
                           let json = try? JSON(data: data)
                           let dict = json?.dictionary
                           print("json returned => \(String(describing: dict))")
                           guard let jsonDict = dict else {
                              Toast.error(view: self.view, message: "System maintenance, please try later.")
                               return
                           }
                           
                           let status = jsonDict["status"]?.bool
                let message = jsonDict["message"]?.string
                    
                if !status! {
                    let err = CustomError(localizedDescription: message!)
                    completion(err, true)
                    return
                }
                
                           completion (nil, false)
            }
            
        }
    }
    
    private func afterPayment(paymentRefId: String,response: AFDataResponse<Data?>,onCompletion: @escaping (_ paymentStatus: Bool, _ error: Error?) -> Void) {
        
        if let err = response.error {
             
             Toast.error(view: self.view, message: err.localizedDescription)
             print("Error  \(err.localizedDescription)")
             return
         }
         
         guard let data = response.data else {
             Toast.error(view: self.view, message: "Please try again later")
             return
         }
         
         let json = try? JSON(data: data)
         let dict = json?.dictionary
         print("json returned => \(String(describing: dict))")
         guard let jsonDict = dict else {
            Toast.error(view: self.view, message: "System maintenance, please try later.")
             return
         }
         
         let status = jsonDict["status"]?.bool
        let message = jsonDict["message"]?.string
        
        if !status! {
            print("sever error => \(message!)")
            let swtVC = presentSweetAlert(presentingController: self.viewController!, message: message!)
            onCompletion(false, CustomError(localizedDescription: message!))
            return
        }
         
    
         Toast.success(view: self.view, message: "You'll receive a prompt soon")
         
         let swtVC = presentSweetAlert(presentingController: self.viewController!, message: "You'll receive a prompt soon")
        
         if status! {
             self.db.collection(Collections.PAYMENTS.rawValue).document(paymentRefId).addSnapshotListener { (document, error) in
                 
                 if let error = error{
                     onCompletion(false, error)
                     return
                 }
                 
                 let doc = document!.data()
                 
                 print("payment status changed => \(String(describing: doc!["status"]))")
                 
                 if doc!["status"] as! String == "APPROVED" {
                     if swtVC!.isViewLoaded && (swtVC!.view!.window != nil) {
                         swtVC!.dismiss(animated: true, completion: nil)
                     }
                     onCompletion(true, nil)
                     return
                 }
                 
                 else if doc!["status"] as! String == "FAILED" {
                     onCompletion(false, CustomError(localizedDescription: "FAILED"))
                     return
                 }
                 
                 else if doc!["status"] as! String == "DECLINED" {
                     onCompletion(false, CustomError(localizedDescription: "DECLINED"))
                 }
                 
                // onCompletion(true, nil)
                 
             }
         }
        
    }
    
    func addFunds(momoNumber: String, network: MomoNetwork, amount: Double, vodafoneAuthToken: String?,
                  onCompletion: @escaping (_ paymentStatus: Bool, _ error: Error?) -> Void) {
        let paymentRefId = getUniqueString()
        
        var payment = Payment()
        payment.amount = amount
        if let vodafoneAuthToken = vodafoneAuthToken{
            payment.mobileAuthToken = vodafoneAuthToken
        }
        payment.momoNumber = momoNumber
        payment.network = network.rawValue
        payment.orderId = paymentRefId
        payment.senderId = firebaseUser!.uid
        payment.senderName = Constants.applicationUser!.fullName
        var param = [String:Any]()
        param["amount"] = payment.amount
        param["mobileAuthToken"] = payment.mobileAuthToken
        param["momoNumber"] = payment.momoNumber
        param["network"] = payment.network
        param["orderId"] = payment.orderId
        param["senderId"] = payment.senderId
        param["senderName"] = payment.senderName
        param["status"] = "PENDING"
        
        let dialog = AnyProgressDialog.newInstance(view: view).show()
        db.collection(Collections.PAYMENTS.rawValue).document(paymentRefId).setData(param) {  error in
            if let error = error {
                onCompletion(false,error)
                dialog.dismis()
                print("Server Error - \(error)")
                return
            }
            
            // pass the parameter to the api
            
            var apiNetwork: String?
            
            switch network {
            case MomoNetwork.MTN:
                apiNetwork = "MTN_MM"
                break
            case MomoNetwork.AIRTEL:
                 apiNetwork = "TIGO_CASH"
                break
            case MomoNetwork.TIGO:
                 apiNetwork = "AIRTEL_MM"
                break
            case MomoNetwork.VODAFONE:
                 apiNetwork = "VODAFONE_CASH"
                break
            }
            
            let url = Constants.BASE_URL + "/deposits/customer-deposit";
            
            var p = [String: String]()
            let amountToPay = String(describing: amount)
            p["amount"] = amountToPay
            p["orderId"] = paymentRefId
            p["momoNumber"] = momoNumber
            p["network"] = apiNetwork
            p["mobileAuthToken"] = payment.mobileAuthToken ?? ""
            p["senderName"] = Constants.applicationUser?.fullName ?? ""
            p["transType"] = "CREDIT"
            p["paymentType"] = "DEPOSIT"
            p["currency"] = "GHS"
            let senderId = Auth.auth().currentUser?.uid ?? ""
            p["senderId"] = senderId
            print("url => \(url)")
            
            AF.request(url, method: .post, parameters: p).useBasicAuth() .response { (response) in
                
                dialog.dismis()
                
                self.afterPayment(paymentRefId: paymentRefId, response: response, onCompletion: onCompletion)
                
            }
        
        }
        
    }
}

// pay for trip with wallet

