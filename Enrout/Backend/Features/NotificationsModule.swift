//
//  NotificationsModule.swift
//  Enrout
//
//  Created by Daniel Kwakye on 19/01/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import Foundation
import UserNotifications

func scheduleNotification(title: String, content: String, trip: Trip? = nil) {
    
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    appDelegate?.scheduleNotification(title: title, content: content, trip: trip)
//    if let trip = trip {
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificationTrip"), object: nil, userInfo: ["trip": Trip.convertTripToData(trip: trip)])
//    }
}


extension AppDelegate: UNUserNotificationCenterDelegate {
    
    // remember to call notificationCenter.delegate = self in the appDelegate itself
    
    // this method present nofication even if its in the foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.alert, .sound])
    
    }
    
    // to perform an action when the user taps on the notification icon
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        if response.notification.request.identifier == "Local Notification" {
            print("Handling notifications with the Local Notification Identifier")
        }
        
        completionHandler()
        
    }
    
}

