//
//  Deliveries.swift
//  
//
//  Created by Daniel Expresspay on 06/12/2019.
//

import Firebase
import FirebaseAuth

func  getTrips(by status: TripHistoryTypes,onlyToday: Bool = false, limit: Int = 10, completion: @escaping (_ trips: [Trip]?,_ error: Error?) -> Void) {
    let db = Firestore.firestore()
    var ref = db.collection(Collections.REQUEST.rawValue).whereField("senderId", isEqualTo: Auth.auth().currentUser!.uid).order(by: "requestDatetime", descending: true)
    
        if onlyToday {
          ref = ref.whereField("requestDatetime", isDateInToday: Date())
        }else{
            ref = ref.limit(to: limit)
        }
    
    
        ref.getDocuments { (snapshots, error) in
            
            if let err = error {
                completion(nil, err)
                return
            }
            
           let trips = snapshots?.documents.compactMap({ (snapshot) in
                snapshot.data().flatMap { (data) in
                    return Trip(dictionary: data)
                }
            })
            
            var selectTrips = [Trip]()
            
            if let trips = trips {
                switch status {
                case .ONGOING:
                    for trip in trips {
                        if trip.status == TripStatus.PENDING.rawValue
                            || trip.status == TripStatus.NOT_FOUND.rawValue
                         || trip.status == TripStatus.ONGOING.rawValue
                         || trip.status == TripStatus.PROCESSING.rawValue
                            || trip.status == TripStatus.DELAYED.rawValue
                        {

                            selectTrips.append(trip)
                        }
                    }
                    
                     completion(selectTrips, nil)
                    break
                case .CANCELLED:
                    for trip in trips {
                           if trip.status == TripStatus.CANCELLED.rawValue
                               || trip.status == TripStatus.CANCELLED_R.rawValue
                           {
                               selectTrips.append(trip)
                           }
                       }
                       
                        completion(selectTrips, nil)
                    break
                case .DELIVERED:
                    for trip in trips {
                           if trip.status == TripStatus.COMPLETED.rawValue
                           {
                               selectTrips.append(trip)
                           }
                       }
                       
                        completion(selectTrips, nil)
                    break
                    
                }
                
                
                return
            }
            
            completion(nil, CustomError(localizedDescription: "Please try again"))
            

    }
}

func savePreviousTrips(){
    getTrips(by: .DELIVERED, limit: 5) { (trips, error) in
        if error != nil {
            return
        }
        
        if let trips = trips {
            
        }
    }
}

