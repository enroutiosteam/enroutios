//
//  ReviewAppModule.swift
//  Enrout
//
//  Created by Daniel Kwakye on 22/02/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth

func shouldShowReviewDialog(completion: @escaping (_ show:Bool) -> Void){
    getUser(userId: Constants.applicationUser!.id!) { (status, user) in
        if !status{
            completion(false)
            return
        }
        
        guard let user = user else {
            completion(false)
            return
        }
        
        if user.appReviewed {
            completion(false)
            return
        }
        
        
        // check if user has completed at least 2 trips
        let db = Firestore.firestore()
        db.collection(Collections.REQUEST.rawValue).whereField("senderId", isEqualTo: Auth.auth().currentUser!.uid)
        .whereField("status", isEqualTo: "COMPLETED").getDocuments { (snapshot, error) in
            if error != nil {
                completion(false)
                return
            }
            
            guard let docs = snapshot?.documents else{
                completion(false)
                return
            }
            
            if docs.isEmpty{
                completion(false)
                return
            }
            
            var hasRequestedToday = false
            
            for doc in docs {
                let data = doc.data()
                let trip  = Trip.init(dictionary: data)
                let requestDatetime =  trip.requestDatetime as! Date
                

                let today = Calendar.current.isDateInToday(requestDatetime)
                
                if today {
                    hasRequestedToday = true
                    break
                }
            }
            
            
            if hasRequestedToday && docs.count > 2 {
                completion(true)
            }else{
                completion(false)
            }
            
            
            
        }
        
    }
}

func updateAppReviewStatusToTrue(completion: @escaping(_ updated: Bool,_ user: ApplicationUser?) -> Void){
    Constants.applicationUser!.appReviewed = true
    Constants.applicationUser!.device = "IOS"
    updateUser(user: Constants.applicationUser!) { (status, user) in
        if !status {
            completion(false, nil)
            return
        }
        
        guard let user = user else {
            completion(false, nil)
            return
        }
        
        completion(false, user)
        
    }
}
