//
//  Ratings.swift
//  Enrout
//
//  Created by Daniel Expresspay on 06/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Firebase
import UIKit

func rateRider(viewController: UIViewController,riderId: String, tripId: String?, mark: Double, message: String?, completion: @escaping (_ error: Error?) -> Void) {

    let db = Firestore.firestore()
    let dialog = AnyProgressDialog.newInstance(view: viewController.view).show()
    db.collection(Collections.DRIVERS.rawValue).document(riderId).getDocument { (snapshot, error) in
        if let err = error {
            dialog.dismis()
            completion(err)
            print("error => \(err.localizedDescription)")
            return
        }
        
        dialog.dismis()
        let driver = snapshot.flatMap { (doc) in
            return Driver(dictionary: doc.data()!)
        }
        
        if let driver = driver {
            if mark != 0.0 {
                let newSumOfVotes: Double = driver.sumOfVotes! + mark
                let newTotalVotes: Double = driver.totalNoOfVotes! + 1
                
                let newAverateRating: Double = newSumOfVotes / newTotalVotes
                
                let params = [
                    "sumOfVotes" : newSumOfVotes,
                    "totalNoOfVotes" : newTotalVotes,
                    "averageRating" : newAverateRating
                ]
                
                 /// insert into back into the rider rating
                db.collection(Collections.DRIVERS.rawValue).document(riderId).updateData(params)
                
                // insert into trip
                if let trip = tripId {
                    
                    let param2 = [
                        "rated": true,
                        "rating": mark
                        ] as [String : Any]
                    
                    db.collection(Collections.REQUEST.rawValue).document(trip).updateData(param2)
                }
                
                /// write into comments
                if let msg = message {
                    let commentsParams = [
                        "userId" : Auth.auth().currentUser!.uid,
                        "driverId": riderId,
                        "comment" : msg,
                        "dateCreated": Date()
                        ] as [String : Any]
                    db.collection(Collections.DRIVER_COMMENTS.rawValue).addDocument(data: commentsParams)
                }
                
                completion(nil)
                
            }
            return
        }
    }
}

func getRiderRating(riderId: String, completion: @escaping (_ rating: Double?) -> Void){
    let db = Firestore.firestore()
    db.collection(Collections.DRIVERS.rawValue).document(riderId).getDocument { (snapshot, error) in
        if let err = error {
            print(err.localizedDescription)
            completion(nil)
            return
        }
        
        let driver = snapshot.flatMap { (doc) in
            return Driver(dictionary: doc.data()!)
        }
        
        completion(driver?.averageRating)
        
        
    }
}

func checkIfTheresUnratedTrip(completion: @escaping(_ trip: Trip?) -> Void){
     let db = Firestore.firestore()
    print("sender id => \(Auth.auth().currentUser!.uid)")
    db.collection(Collections.REQUEST.rawValue)
        .whereField("senderId", isEqualTo: Auth.auth().currentUser!.uid)
       .whereField("rated", isEqualTo: false)
        .whereField("status", isEqualTo: "COMPLETED")
//    .order(by: "requestDateTime", descending: true)
        .getDocuments {
            (snapshots, error) in
            
            if let err = error {
                print(err.localizedDescription)
                completion(nil)
                return
            }
            
            guard let snapshots = snapshots else {
                completion(nil)
                return
            }
            
            if snapshots.documents.count < 1 {
                completion(nil)
                return
            }
            
            let firstTripData = snapshots.documents[0].data()
            let firstTrip = Trip(dictionary: firstTripData)
            
            completion(firstTrip)
    }
}

