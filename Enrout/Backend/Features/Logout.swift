//
//  Logout.swift
//  Enrout
//
//  Created by Daniel Expresspay on 06/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Foundation
import UIKit
import FirebaseAuth

func logoutUser(viewController: UIViewController, completion: @escaping () -> Void) {
   let dialog = AnyProgressDialog.newInstance(view: viewController.view).show()
   if var user = Constants.applicationUser {
       print("user id => \(user.id!)")
       user.isOnline = false
       user.device = "IOS"
       updateUser(user: user) { _, _ in
           try? Auth.auth().signOut()
        dialog.dismis()
            completion()
       }
   }
}
