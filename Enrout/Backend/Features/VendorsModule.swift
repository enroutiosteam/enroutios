//
//  VendorsModule.swift
//  Enrout
//
//  Created by Daniel Kwakye on 07/02/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

func fetchVendors(completion: @escaping (_ vendors: [Vendor]?) -> Void ){
     let url = Constants.BASE_URL + "/vendors/ads/get-all"
//    let tempUrl = "https://www.clockalt.com/api.php"
    AF.request(url, method: .get).useBasicAuth().response { (response) in
        if let err = response.error {
            completion(nil)
            print("Error  \(err.localizedDescription)")
            return
        }
        
        guard let data = response.data else {
            completion(nil)
            return
        }
        
        let json = try? JSON(data: data)
        let dict = json?.dictionary
        
        guard let mDict = dict else {
            completion(nil)
              return
        }
        
        let status = mDict["status"]?.bool
        if !status!{
            completion(nil)
            return
        }
        
        
        let array = mDict["adverts"]?.array
        
        ///print("json returned => \(String(describing: dict))")
        guard let jsonArray = array else {
          completion(nil)
            return
        }
        
       // let status = jsonDict["status"]?.bool
        var mVendors: [Vendor] = []
        for jsonDict in jsonArray {
            
            var vendor = Vendor()
        
            vendor.vendorId = jsonDict["vendorId"].string
            vendor.vendorBanner = jsonDict["vendorBanner"].string
            vendor.vendorBranchId = jsonDict["vendorBranchId"].string
            vendor.vendorName = jsonDict["vendorName"].string
            vendor.vendorLogo = jsonDict["vendorLogo"].string
            vendor.vendorDescription = jsonDict["vendorDescription"].string
            vendor.vendorPhoneNumber = jsonDict["vendorPhoneNumber"].string
            
            let location = jsonDict["vendorLocation"].dictionary
            let lat = location?["latitude"]?.double
            let lng = location?["longitude"]?.double
            let address = location?["place"]?.string
            
            var posLatLng = PosLatLng(latitude: lat!, longitude: lng!)
            posLatLng.address = address
            vendor.vendorLocation = posLatLng
            
            let prodArray = jsonDict["products"].array
            var products: [Product] = []
            
            for prodDict in prodArray! {
                var product = Product()
                product.deliveryPrice = prodDict["deliveryPrice"].double
                product.productDescription = prodDict["productDescription"].string
                product.productId = prodDict["productId"].string
                product.productImage = prodDict["productImage"].string
                product.productName = prodDict["productName"].string
                product.productPrice = prodDict["productPrice"].double
                product.productTitle = prodDict["productTitle"].string
              
                products.append(product)
            }
            
            vendor.products = products
            
            mVendors.append(vendor)
            
        }
        
         completion(mVendors)
    }
}
