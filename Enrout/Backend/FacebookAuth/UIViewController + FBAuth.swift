//
//  UIViewcontroller + Auth.swift
//  NikasemoIOS
//
//  Created by Daniel Kwakye on 06/01/2019.
//  Copyright © 2019 Benard Matic Lomo. All rights reserved.
//


import UIKit
import FBSDKCoreKit
import FacebookLogin
import FirebaseAuth
import Firebase

extension UIViewController{
    
    func registerWithFacebook(username: String, phoneNumber: String) {
       let preferences = UserDefaults.standard
       preferences.set(phoneNumber, forKey: Constants.PHONE_AUTH_PHONE_NUMBER)
       preferences.set(username, forKey: Constants.USER_DISPLAY_NAME)
       preferences.set(LoginRegisterAuthType.FACEBOOK_REGISTRATION.rawValue, forKey: Constants.FACEBOOK_AUTH_TYPE)
      preferences.set(AuthType.FACEBOOK.rawValue, forKey: Constants.AUTH_TYPE)

        authFB()
        

   }
    
    func signInWithFacebook(){
        let preferences = UserDefaults.standard
        preferences.set(LoginRegisterAuthType.FACEBOOK_LOGIN.rawValue, forKey: Constants.FACEBOOK_AUTH_TYPE)
        preferences.set(AuthType.FACEBOOK.rawValue, forKey: Constants.AUTH_TYPE)
        
         authFB()
    }
    
    private func authFB() {
        
             let loginManager = LoginManager()
        //
                let dialog = AnyProgressDialog.newInstance(view: self.view).show()
                loginManager.logIn(permissions: ["public_profile","email"], from: self) { (result, error) in
                      if let error = error {
                            dialog.dismis()
                            Toast.error(view: self.view, message: "Authentication cancelled")
                            print("Failed to login: \(error.localizedDescription)")
                            return
                        }

                    guard let accessToken = AccessToken.current else {
                            dialog.dismis()
                            Toast.error(view: self.view, message: "Authentication cancelled")
                            print("Failed to get access token")
                            return
                        }

                    print("accessToken is - : \(accessToken)")
                    
                    let credential = FacebookAuthProvider.credential(withAccessToken: AccessToken.current!.tokenString)
                    
                    dialog.dismis()
                    let preferences = UserDefaults.standard
                    let authType = preferences.string(forKey: Constants.FACEBOOK_AUTH_TYPE)
                    let type = LoginRegisterAuthType(rawValue: authType!)!
                    
                    switch type {
                    case .FACEBOOK_LOGIN:
                        finalizeLoginWithCredentials(viewController: self, credential: credential)
                        break
                    case .FACEBOOK_REGISTRATION:
                        finalizeRegistrationWithCredentials(viewController: self, credential: credential)
                        break
                    default:
                        Toast.error(view: self.view, message: "Please try again")
                    }
    
             }
        
    }
    
    
}
