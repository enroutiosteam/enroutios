//
//  UIViewController + PhoneAuth.swift
//  Enrout
//
//  Created by Daniel Expresspay on 28/11/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

extension UIViewController {
    
    func loginWithPhoneNumber(phoneNumber: String){
        let preferences = UserDefaults.standard
        preferences.set(phoneNumber, forKey: Constants.PHONE_AUTH_PHONE_NUMBER)
        preferences.set(LoginRegisterAuthType.PHONE_LOGIN.rawValue, forKey: Constants.PHONE_AUTH_TYPE)
         preferences.set(AuthType.PHONE.rawValue, forKey: Constants.AUTH_TYPE)
        processPhone(phoneNumber)
    }
//
    func registerWithPhoneNumber(username: String, phoneNumber: String){
        let preferences = UserDefaults.standard
        preferences.set(phoneNumber, forKey: Constants.PHONE_AUTH_PHONE_NUMBER)
        preferences.set(username, forKey: Constants.USER_DISPLAY_NAME)
        preferences.set(LoginRegisterAuthType.PHONE_REGISTRATION.rawValue, forKey: Constants.PHONE_AUTH_TYPE)
        preferences.set(AuthType.PHONE.rawValue, forKey: Constants.AUTH_TYPE)

        processPhone(phoneNumber)
    }
    
    func processPhone(_ phoneNumber: String){
        let dialog = AnyProgressDialog.newInstance(view: self.view).show()
               PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
                     if let error = error {
                       dialog.dismis()
                       Toast.error(view: self.view, message: "Authentication cancelled")
                       print(error.localizedDescription)
                       return
                     }
                   
                   guard let verificationID = verificationID else{
                       dialog.dismis()
                       Toast.error(view: self.view, message: "Please try again")
                       return
                   }
                   
                   dialog.dismis()
                   Toast.success(view: self.view, message: "A verfication code will be sent to you via sms")
                
                print("verification ID => \(String(describing: verificationID))")
                UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
                 
                presentModalController(presentingController: self, imageName: "modal_image", headerTitle: "Verification Code Sent", subHeaderTitle: "A verfication code will be sent to you via sms", doneHidden: false, cancelHidden: true) { _ in
                    
                }
                   

               }
    }
    
    func verifyCode(verificationCode: String){
        let vID = UserDefaults.standard.string(forKey: "authVerificationID")
    
        guard let verificationID = vID else {
            Toast.error(view: self.view, message: "Authentication failed, check credentials and try again")
            return
        }
        
        let credential = PhoneAuthProvider.provider().credential(
        withVerificationID: verificationID,
        verificationCode: verificationCode)
        
        // sign in here
        let preferences = UserDefaults.standard
        let authType = preferences.string(forKey: Constants.PHONE_AUTH_TYPE)
        let type = LoginRegisterAuthType(rawValue: authType!)!
        
        switch type {
        case .PHONE_LOGIN:
            finalizeLoginWithCredentials(viewController: self, credential: credential)
            break
        case .PHONE_REGISTRATION:
            finalizeRegistrationWithCredentials(viewController: self, credential: credential)
            break
        default:
            Toast.error(view: self.view, message: "Technial Error, Please try later")
        }

        
    }
    
    
}
