//
//  ViewController + Auth.swift
//  NikasemoIOS
//
//  Created by Daniel Kwakye on 06/01/2019.
//  Copyright © 2019 Benard Matic Lomo. All rights reserved.
//


import UIKit
import GoogleSignIn
import Firebase
import FirebaseAuth


extension UIViewController : GIDSignInDelegate{
    
    public func registerWithGoogle(username: String, phoneNumber: String){
        let preferences = UserDefaults.standard
        preferences.set(phoneNumber, forKey: Constants.PHONE_AUTH_PHONE_NUMBER)
        preferences.set(username, forKey: Constants.USER_DISPLAY_NAME)
        preferences.set(LoginRegisterAuthType.GOOGLE_REGISTRATION.rawValue, forKey: Constants.GOOGLE_AUTH_TYPE)
        preferences.set(AuthType.GOOGLE.rawValue, forKey: Constants.AUTH_TYPE)
        
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    
    func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any])
      -> Bool {
        return GIDSignIn.sharedInstance().handle(url)
    }

    
    
    //  google sign in completed
    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        print("sign in")
        
        if let error = error {
            Toast.error(view: self.view, message: "Authentication cancelled")
            print("\(error.localizedDescription)")
            return
        }
        
        // Perform any operations on signed in user here.

        //let idToken = user.authentication.idToken // Safe to send to the server
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
        accessToken: authentication.accessToken)
        
        let preferences = UserDefaults.standard
        let authType = preferences.string(forKey: Constants.GOOGLE_AUTH_TYPE)
        let type = LoginRegisterAuthType(rawValue: authType!)!
        
        switch type {
        case .GOOGLE_LOGIN:
            finalizeLoginWithCredentials(viewController: self, credential: credential)
            break
        case .GOOGLE_REGISTRATION:
            finalizeRegistrationWithCredentials(viewController: self, credential: credential)
            break
        default:
            Toast.error(view: self.view, message: "Technical Error, Please try later")
        }

   
    }
    
    public func signInWithGoogle(){
        let preferences = UserDefaults.standard
        preferences.set(LoginRegisterAuthType.GOOGLE_LOGIN.rawValue, forKey: Constants.GOOGLE_AUTH_TYPE)
         preferences.set(AuthType.GOOGLE.rawValue, forKey: Constants.AUTH_TYPE)
        
        // sign in with oAuth
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance()?.signIn()
        
        
    }

    
}
