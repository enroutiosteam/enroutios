//
//  AnyProgressDialog.swift
//  Attenda
//
//  Created by Daniel Kwakye on 31/08/2018.
//  Copyright © 2018 Teksol. All rights reserved.
//

import Foundation
import UIKit
import JGProgressHUD

struct AnyProgressDialog {
    
    private let dialog: JGProgressHUD
    private let view: UIView
    
    private init(dialog: JGProgressHUD, view: UIView) {
        self.dialog = dialog
        self.view = view
    }
    
    static func newInstance(view: UIView, text: String = "Please wait") -> AnyProgressDialog{
        let progress = JGProgressHUD(style: .dark)
        progress.textLabel.text = text
        let anyProgress = AnyProgressDialog.init(dialog: progress, view: view)
        return anyProgress
        //progress.show(in: self.view, animated: true)
    }
    
    func setText(text: String){
        dialog.textLabel.text = text
    }
    
    func show() -> AnyProgressDialog{
        let dialog = self.dialog
        dialog.show(in: self.view, animated: true)
        return self
    }
    
    func dismis() {
        let dialog = self.dialog
        dialog.dismiss(animated: true)
    }
}
