//
//  CustomError.swift
//  Enrout
//
//  Created by Daniel Expresspay on 02/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Foundation

struct CustomError: LocalizedError {
    var localizedDescription: String
    var errorDescription: String? { return localizedDescription }
}
