//
//  SockIOConnection.swift
//  Enrout
//
//  Created by Daniel Expresspay on 02/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Foundation
import SocketIO
import FirebaseAuth



struct SockIOConnection {
    
    static var socketConnection: SockIOConnection?
    
    let manager = SocketManager(socketURL: URL(string: Constants.BASE_URL)!, config: [.log(true), .compress, .connectParams(
        ["customerId": Auth.auth().currentUser!.uid, "type": "CUSTOMER", "customerName": "IOS customer"]
        ), .forcePolling(true), .forceWebsockets(true)
    ])
    
    //
    let socket: SocketIOClient
    
    static func getSocketInstance() -> SockIOConnection{
        if socketConnection == nil {
            socketConnection = SockIOConnection()
            socketConnection?.socket.connect()
        }
        
        return socketConnection!
        
    }
    private init() {
        self.socket  = manager.defaultSocket
        socket.on(clientEvent: .connect) {data, ack in
            print("socket connected")
        }
        
        socket.on(clientEvent: .disconnect) { (data, ack) in
            print("socket disconnected")
        }
        
    }
    
    
//    func connect() {
//        print("connecting ....")
//        self.socket.connect()
//    }
//    
//    func disconect() {
//        print("disconnecting ...")
//        self.socket.disconnect()
//    }
    
    
    
}
