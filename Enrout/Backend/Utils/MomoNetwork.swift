//
//  MomoNetwork.swift
//  Enrout
//
//  Created by Daniel Expresspay on 02/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Foundation

enum MomoNetwork: String{
    case MTN, TIGO, AIRTEL, VODAFONE
}
