//
//  Toast.swift
//  Attenda
//
//  Created by Daniel Kwakye on 31/08/2018.
//  Copyright © 2018 Teksol. All rights reserved.
//

import Foundation
import UIKit
import JGProgressHUD

struct Toast {
    
    static func success(view: UIView,message: String) {
        let toast = JGProgressHUD(style: .dark)
        toast.textLabel.text = message
        toast.indicatorView = JGProgressHUDSuccessIndicatorView(contentView: view)
        toast.position = JGProgressHUDPosition.bottomCenter
        toast.show(in: view, animated: true)
        toast.dismiss(afterDelay: 3, animated: true)
    }
    
    static func error(view: UIView,message: String) {
        let toast = JGProgressHUD(style: .dark)
        toast.textLabel.text = message
        toast.indicatorView = JGProgressHUDErrorIndicatorView(contentView: view)
        toast.position = JGProgressHUDPosition.bottomCenter
        toast.show(in: view, animated: true)
        toast.dismiss(afterDelay: 3, animated: true)
    }
}
