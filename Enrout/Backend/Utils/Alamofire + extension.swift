//
//  Alamofire + extension.swift
//  Enrout
//
//  Created by Daniel Kwakye on 15/01/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import Foundation
import Alamofire

extension DataRequest {
    
    func useBasicAuth() -> DataRequest{
        authenticate(username: Constants.API_USERNAME, password: Constants.API_PASSWORD)
        return self
    }
}
