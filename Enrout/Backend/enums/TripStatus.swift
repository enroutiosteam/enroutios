//
//  TripStatus.swift
//  Enrout
//
//  Created by Daniel Expresspay on 07/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Foundation

enum TripStatus: String {
    case PENDING
    case CANCELLED
    case CANCELLED_R
    case ONGOING
    case COMPLETED
    case NOT_FOUND
    case PROCESSING
    case DELAYED
}
