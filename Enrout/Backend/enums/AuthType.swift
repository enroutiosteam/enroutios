//
//  AuthType.swift
//  Enrout
//
//  Created by Daniel Expresspay on 28/11/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Foundation

enum AuthType: String {
    case GOOGLE, FACEBOOK, PHONE
}
