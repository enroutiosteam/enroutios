//
//  File.swift
//  Enrout
//
//  Created by Daniel Expresspay on 25/11/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Foundation

enum LoginRegisterAuthType: String{
    case GOOGLE_LOGIN, GOOGLE_REGISTRATION
    case FACEBOOK_LOGIN, FACEBOOK_REGISTRATION
    case PHONE_LOGIN, PHONE_REGISTRATION
}
