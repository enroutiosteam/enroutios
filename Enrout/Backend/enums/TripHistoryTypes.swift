//
//  File.swift
//  Enrout
//
//  Created by Daniel Expresspay on 08/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

enum TripHistoryTypes {
    case ONGOING, CANCELLED, DELIVERED
}
