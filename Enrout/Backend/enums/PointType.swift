//
//  PointType.swift
//  Enrout
//
//  Created by Daniel Expresspay on 08/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Foundation

enum PointType : String{
    case PICKUP, DESTINATION
}
