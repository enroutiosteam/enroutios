//
//  Collections.swift
//  Enrout
//
//  Created by Daniel Expresspay on 25/11/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Foundation

enum Collections : String {
    case USERS, PAYMENTS, TRANSACTIONLEDGER, SETTINGS, COUPONS, REQUEST
    case PICKUPPOINTS, DESTINATIONS, DRIVERS, DRIVER_COMMENTS
}
