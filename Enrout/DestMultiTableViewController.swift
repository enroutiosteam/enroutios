//
//  DestMultiTableViewController.swift
//  Enrout
//
//  Created by Daniel Kwakye on 09/01/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import UIKit
import MGSwipeTableCell

class DestMultiTableViewController: UITableViewController {

    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var addAnotherBtn: UIButton!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var pageTitle: UILabel!
    
    @IBOutlet var placePlaceholders: [UILabel]!
    @IBOutlet var moreBtns: [UIButton]!
    
    var bottomViewController: BottomViewController?
    
    @IBOutlet var descriptions: [UILabel]!
    
    private lazy var destAdditionalDetailsViewController: DestAdditionalDetailsViewController? = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(identifier: "DestAdditionalDetailsViewController") as? DestAdditionalDetailsViewController
        return viewController
        
    }()
    
    func refreshControler(){
        print("current act dest => \(UIConstants.activeDestinations.count)")
        tableView.reloadData()
        
        for (index,destination) in UIConstants.activeDestinations.enumerated() {
           let placehoder = placePlaceholders[index]
            placehoder.text = destination.place
            
            let desc = descriptions[index]
            var description = ""
            if destination.recipientName != nil {
                description = destination.recipientName!
            }
            desc.text = description
            moreBtns[index].tag = index
            moreBtns[index].addTarget(self, action: #selector(moreBtnTapped(_:)), for: .touchUpInside)
            
            let cell =  tableView.cellForRow(at: IndexPath(row: index, section: 1)) as! MGSwipeTableCell
            cell.delegate = self
            
        }
        
        if getTripType() == .MP {
            addAnotherBtn.isHidden = true
        }else{
            addAnotherBtn.isHidden = false
        }
        
        tableView.reloadData()
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        doneBtn.curveCorners()
        addAnotherBtn.addTarget(self, action: #selector(addBtnTapped), for: .touchUpInside)
        closeBtn.addTarget(self, action: #selector(closeBtnTapped), for: .touchUpInside)
        
        doneBtn.addTarget(self, action: #selector(doneBtnTapped), for: .touchUpInside)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        refreshControler()
    }

  // MARK: - Table view data source

  override func numberOfSections(in tableView: UITableView) -> Int {
      // #warning Incomplete implementation, return the number of sections
      return 4
  }

  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      // #warning Incomplete implementation, return the number of rows
      if section == 1 {
          return UIConstants.activeDestinations.count // this should depend on the number of destinations
      }else{
          return 1
      }
  }
  
  override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
      
      return UIView()
  }
  
  override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
      
      return 1
      
  }
  
  override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
      return 0
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      tableView.deselectRow(at: indexPath, animated: false)
    
    if indexPath.section == 0 {
        closeBtnTapped(nil)
    }else if indexPath.section == 1 {
        let btn = UIButton()
        btn.tag = indexPath.row
        moreBtnTapped(btn)
    }
  }
    
    @IBAction func moreBtnTapped(_ sender: UIButton){
        let index = sender.tag
        let destination  = UIConstants.activeDestinations[index]
        destination.index = index
        destAdditionalDetailsViewController?.destination = destination
        destAdditionalDetailsViewController?.source = self
        present(destAdditionalDetailsViewController!, animated: true, completion: nil)
    }
    
     @IBAction func closeBtnTapped(_ sender: Any?) {
    //          UIConstants.activePickUps = []
    //          placeCompleteController.rowIsSelectable = true
    //          placeCompleteController.rowIsSwipable = false
            bottomViewController?.onbackPressed(currentPanel: .destinationpointdetailsmultiple,  additionalData: ["type":"BACKPRESSED"])
          }
          
      @IBAction func addBtnTapped(){
         //placeCompleteController.rowIsSelectable = true
        bottomViewController?.onbackPressed(currentPanel: .destinationpointdetailsmultiple, additionalData: ["type":"ADD_DESTINATION"])
      }
        
    @IBAction func doneBtnTapped(){
            bottomViewController?.switchdrawer(currentPanel: .destinationpointdetailsmultiple)
    }
    
    func additionalDetailsAdded(destination: Destination){
        UIConstants.activeDestinations[destination.index!] = destination
        refreshControler()
    }
    
  

}

extension DestMultiTableViewController: MGSwipeTableCellDelegate{
    func swipeTableCell(_ cell: MGSwipeTableCell, swipeButtonsFor direction: MGSwipeDirection, swipeSettings: MGSwipeSettings, expansionSettings: MGSwipeExpansionSettings) -> [UIView]? {
           
           if direction == .leftToRight {
               return nil
           }
           
           swipeSettings.transition = MGSwipeTransition.border;
           expansionSettings.buttonIndex = 0;
           
           expansionSettings.fillOnTrigger = true;
           expansionSettings.threshold = 1.1;
           let padding = 15;
           let color1 = UIColor.init(red:1.0, green:59/255.0, blue:50/255.0, alpha:1.0);
           
           let trash = MGSwipeButton(title: "Delete", icon: UIImage(systemName: "trash")?.withTintColor(UIColor.white), backgroundColor: color1, padding: padding, callback: {
               cell in
               cell.rightExpansion.fillOnTrigger = false
                           //print("swipe offset => \(String(describing: ce))")
               let indexPath = (self.tableView.indexPath(for: cell)?.row)!
               print("row swiped at \(indexPath)")
               UIConstants.activeDestinations.remove(at: indexPath)
               self.refreshControler()
               self.bottomViewController?.mainViewController?.reRenderIconsOnMap()
               
               if UIConstants.activeDestinations.count < 2{
                   self.bottomViewController?.switchdrawer(currentPanel: .searchdestinationpoint)
               }
               

               return false
           });
           
           
           return [trash]
    }
    
    func swipeTableCell(_ cell: MGSwipeTableCell, canSwipe direction: MGSwipeDirection, from point: CGPoint) -> Bool {
           return true
    }
       
}
