//
//  PickupAdditionalDetailsViewController.swift
//  Enrout
//
//  Created by Daniel Kwakye on 09/01/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import UIKit
import ContactsUI

class PickupAdditionalDetailsViewController: UIViewController {

    @IBOutlet weak var pageTitle: UILabel!
    @IBOutlet weak var itemCategoryTF: UITextField!
    @IBOutlet weak var itemInstructionsTF: UITextField!
    @IBOutlet weak var addContactDetailsBtn: UIButton!
    
    @IBOutlet weak var pickupPersonNameTF: UITextField!
    @IBOutlet weak var pickupPersonPhoneTF: UITextField!
    
    @IBOutlet weak var doneBtn: UIButton!
    var source: PickupMultiTableViewController?
    
    var pickup: Pickup?
    
    var contactPicker: CNContactPickerViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        doneBtn.addTarget(self, action: #selector(doneBtnTapped(_:)), for: .touchUpInside)
        addContactDetailsBtn.addTarget(self, action: #selector(addContactBtnTapped), for: .touchUpInside)
        
        itemCategoryTF.delegate = self
        
       contactPicker  = CNContactPickerViewController()
              contactPicker.delegate = self
              contactPicker.displayedPropertyKeys =
                  [CNContactGivenNameKey
                      , CNContactPhoneNumbersKey]
        doneBtn.curveCorners()
        
        let image = UIImage(named: "add")
        let resizedImage = image?.resizeImage(targetSize: CGSize(width: 30, height: 30))
        addContactDetailsBtn.setImage(resizedImage, for: .normal)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setVaules()
        
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        // let newImage = image.resizeImage(targetSize: CGSize(width: 20.0, height: 20.0))
         btn.setImage(UIImage(named: "menu_icon"), for: .normal)
         btn.addTarget(self, action:  #selector(self.menuItemCategoryTapped(_:)), for: .touchUpInside)
        
         let rv = UIView(frame: CGRect(x: 0, y: 0, width: btn.frame.width + 10, height: btn.frame.height))
         itemCategoryTF.rightViewMode = .always
         rv.addSubview(btn)
         itemCategoryTF.rightView = rv
        
       
        itemInstructionsTF.addRightIcon(image: UIImage(named: "edit_icon")!)
        
        pickupPersonNameTF.addRightIcon(image: UIImage(named: "edit_icon")!)
        pickupPersonPhoneTF.addRightIcon(image: UIImage(named: "edit_icon")!)
        
        let image = UIImage(named: "add_contact")
        let resizedImage = image?.resizeImage(targetSize: CGSize(width: 30, height: 30))
        addContactDetailsBtn.setImage(resizedImage, for: .normal)
    }
    
    @IBAction func menuItemCategoryTapped(_ sender: UIButton?){
        presentItemCategories(presentingController: self)
    }
    
    func setVaules(){
        pageTitle.text = pickup?.place
        itemCategoryTF.text = pickup?.itemCategory
        itemInstructionsTF.text = pickup?.instruction
        pickupPersonNameTF.text = pickup?.pickUpName
        pickupPersonPhoneTF.text = pickup?.pickUpPersonPhone
    }
    
    @IBAction func addContactBtnTapped(){
       
        self.present(contactPicker, animated: true, completion: nil)
    }
    
    @IBAction func closeBtnTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneBtnTapped(_ sender: Any){
        pickup?.pickUpName = pickupPersonNameTF.text
        pickup?.pickUpPersonPhone = pickupPersonPhoneTF.text
        pickup?.instruction = itemInstructionsTF.text
        pickup?.itemCategory = itemCategoryTF.text
        
        source?.additionalDetailsAdded(pickup: pickup!)
        self.dismiss(animated: true, completion: nil)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PickupAdditionalDetailsViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == itemCategoryTF {
            itemCategoryTF.endEditing(true)
            // show categories to choose from
            print("show item categories")
            presentItemCategories(presentingController: self)
            
        }
    }
}

extension PickupAdditionalDetailsViewController: ItemCategoryDelegate {
    func onSelect(item: ItemCategory) {
        itemCategoryTF.text = item.label
    }
}

extension PickupAdditionalDetailsViewController: CNContactPickerDelegate{
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        // You can fetch selected name and number in the following way

        // user name
        let userName:String = contact.givenName

        // user phone number
        let userPhoneNumbers:[CNLabeledValue<CNPhoneNumber>] = contact.phoneNumbers
        let firstPhoneNumber:CNPhoneNumber = userPhoneNumbers[0].value


        // user phone number string
        let primaryPhoneNumberStr:String = firstPhoneNumber.stringValue

        pickupPersonNameTF.text = userName
        pickupPersonPhoneTF.text = primaryPhoneNumberStr
        print(primaryPhoneNumberStr)

    }
}
