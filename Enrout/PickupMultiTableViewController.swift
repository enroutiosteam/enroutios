//
//  PickupMultiTableViewController.swift
//  Enrout
//
//  Created by Daniel Kwakye on 09/01/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import UIKit
import MGSwipeTableCell

class PickupMultiTableViewController: UITableViewController {
    
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var pageTitle: UILabel!
    @IBOutlet weak var addAnotherBtn: UIButton!
    @IBOutlet weak var doneBtn: UIButton!
    
    @IBOutlet var placePlaceholder: [UILabel]!
    @IBOutlet var moreBtn: [UIButton]!
    
    @IBOutlet var descriptions: [UILabel]!
    
    
    var bottomViewController: BottomViewController?
    
    private lazy var pickupAdditionalDetailsViewController: PickupAdditionalDetailsViewController? = {
           let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
           let viewController = storyboard.instantiateViewController(identifier: "PickupAdditionalDetailsViewController") as? PickupAdditionalDetailsViewController
           return viewController
           
       }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        doneBtn.curveCorners()
        addAnotherBtn.addTarget(self, action: #selector(addBtnTapped), for: .touchUpInside)
        closeBtn.addTarget(self, action: #selector(closeBtnTapped), for: .touchUpInside)
        
        doneBtn.addTarget(self, action: #selector(doneBtnTapped), for: .touchUpInside)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        refreshControler()
    }
    
    func refreshControler(){
        tableView.reloadData()
        
        for (index,pickup) in UIConstants.activePickUps.enumerated() {
            
           let placehoder = placePlaceholder[index]
            placehoder.text = pickup.place
            
            let desc = descriptions[index]
            var description = ""
            if pickup.pickUpName != nil {
                description = pickup.pickUpName!
            }
            desc.text = description
            moreBtn[index].tag = index
            moreBtn[index].addTarget(self, action: #selector(moreBtnTapped(_:)), for: .touchUpInside)
            
          let cell =  tableView.cellForRow(at: IndexPath(row: index, section: 1)) as! MGSwipeTableCell
            cell.delegate = self
            
        }
        
        if getTripType() == .MD {
            addAnotherBtn.isEnabled = false
        }else{
            addAnotherBtn.isEnabled = true
        }
        
        tableView.reloadData()
    }
    
    @IBAction func moreBtnTapped(_ sender: UIButton){
        let index = sender.tag
        let pickup  = UIConstants.activePickUps[index]
        pickup.index = index
        pickupAdditionalDetailsViewController?.pickup = pickup
        pickupAdditionalDetailsViewController?.source = self
        present(pickupAdditionalDetailsViewController!, animated: true, completion: nil)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 4
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 1 {
            return UIConstants.activePickUps.count // this should depend on the number of pickups
        }else{
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        return UIView()
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 1
        
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if indexPath.section == 0 {
            closeBtnTapped(nil)
        }else if indexPath.section == 1 {
            let btn = UIButton()
            btn.tag = indexPath.row
            moreBtnTapped(btn)
        }
    }
    
    @IBAction func closeBtnTapped(_ sender: Any?) {
//          UIConstants.activePickUps = []
//          placeCompleteController.rowIsSelectable = true
//          placeCompleteController.rowIsSwipable = false
          bottomViewController?.onbackPressed(currentPanel: .pickpointdetailmultiple)
      }
      
      @IBAction func addBtnTapped(){
         //placeCompleteController.rowIsSelectable = true
          bottomViewController?.onbackPressed(currentPanel: .pickpointdetailmultiple)
      }
    
    @IBAction func doneBtnTapped(){
        bottomViewController?.switchdrawer(currentPanel: .pickpointdetailmultiple)
    }
    
    func additionalDetailsAdded(pickup: Pickup){
        UIConstants.activePickUps[pickup.index!] = pickup
        refreshControler()
    }

}

extension PickupMultiTableViewController: MGSwipeTableCellDelegate{
    
    func swipeTableCell(_ cell: MGSwipeTableCell, swipeButtonsFor direction: MGSwipeDirection, swipeSettings: MGSwipeSettings, expansionSettings: MGSwipeExpansionSettings) -> [UIView]? {
        
        if direction == .leftToRight {
            return nil
        }
        
        swipeSettings.transition = MGSwipeTransition.border;
        expansionSettings.buttonIndex = 0;
        
        expansionSettings.fillOnTrigger = true;
        expansionSettings.threshold = 1.1;
        let padding = 15;
        let color1 = UIColor.init(red:1.0, green:59/255.0, blue:50/255.0, alpha:1.0);
        
        let trash = MGSwipeButton(title: "Delete", icon: UIImage(systemName: "trash")?.withTintColor(UIColor.white), backgroundColor: color1, padding: padding, callback: {
            cell in
            cell.rightExpansion.fillOnTrigger = false
                        //print("swipe offset => \(String(describing: ce))")
            let indexPath = (self.tableView.indexPath(for: cell)?.row)!
            print("row swiped at \(indexPath)")
            UIConstants.activePickUps.remove(at: indexPath)
            self.refreshControler()
            self.bottomViewController?.mainViewController?.reRenderIconsOnMap()
            
            if UIConstants.activePickUps.count < 2{
                self.bottomViewController?.switchdrawer(currentPanel: .searchpickpoint)
            }
            

            return false
        });
        
        
        return [trash]
        
    }
    
    
    func swipeTableCell(_ cell: MGSwipeTableCell, canSwipe direction: MGSwipeDirection, from point: CGPoint) -> Bool {
        return true
    }
    
}

