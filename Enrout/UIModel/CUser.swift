//
//  CUser.swift
//  Enrout
//
//  Created by Daniel Kwakye on 16/01/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import Foundation

struct CUser: MSGUser {
    var displayName: String
    var avatar: UIImage?
    var avatarUrl: URL?
    var isSender: Bool
    
}
