//
//  PlaceComplete.swift
//  Enrout
//
//  Created by Daniel Expresspay on 26/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Foundation

class PlaceComplete {
    var index: Int?
    var icon: UIImage?
    var name: String?
    var description: String?
    var latitude: Double?
    var longitude: Double?
    var placeId: String?
    
    enum CodingKeys: String, CodingKey {
        case index
        case name
        case description
        case latitude
        case longitude
    }
}

extension PlaceComplete: Encodable {
  func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    try container.encode(index, forKey: .index)
    try container.encode(name, forKey: .name)
    
  }
}
