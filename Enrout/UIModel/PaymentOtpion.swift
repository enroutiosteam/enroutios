//
//  File.swift
//  Enrout
//
//  Created by Daniel Kwakye on 10/01/2020.
//  Copyright © 2020 Enrout. All rights reserved.
//

import Foundation

struct PaymentOption {
    var id: Int?
    var name: String?
    var image: UIImage?
}
