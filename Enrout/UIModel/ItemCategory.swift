//
//  ItemCategory.swift
//  Enrout
//
//  Created by Daniel Expresspay on 28/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Foundation

class ItemCategory {
    var index: Int?
    var icon: String?
    var label: String?
    
    init(icon: String?, label: String?) {
        self.icon = icon
        self.label = label
    }
}
