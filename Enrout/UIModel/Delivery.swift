//
//  Delivery.swift
//  Enrout
//
//  Created by Daniel Expresspay on 13/12/2019.
//  Copyright © 2019 Enrout. All rights reserved.
//

import Foundation

struct Delivery {
    var leftIcon: String?
    var title: String?
    var subTitle: String?
    var rightIcon: String?
    var rightText: String?
}
